#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
using namespace cv;

struct TouchPoint {
    int id;
    Point2f position;
    bool active;
};

double calculateDistance(Point2f p1, Point2f p2) {
    return sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2));
}

int main()
{
    Mat video;
    VideoCapture cap("D:/Uni/Semester_7/MTMM/Ressources/mt_camera_raw.AVI");

    if (!cap.isOpened()) {
        cout << "Error: Cannot open video" << endl;
        return -1;
    }

    // Lade das Video für den Vergleich
    video = imread("D:/Uni/Semester_7/MTMM/Ressources/MT_Screenshots-20240312/Video.jpg", IMREAD_GRAYSCALE);

    Mat frame, gray, blurred, diff, diff2, thresh;

    // Liste für die verfolgten Finger-Blobs
    vector<TouchPoint> touchPoints;

    // Original framerate des Videos
    double fps = cap.get(CAP_PROP_FPS);

    while (1) {
        // Capture frame-by-frame
        cap >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        cvtColor(frame, gray, COLOR_BGR2GRAY);

        absdiff(gray, video, diff);
        blur(diff, blurred, Size(15, 15), Point(-1, -1), BORDER_DEFAULT);
        diff2 = diff - blurred;
        threshold(diff2, thresh, 6, 255, THRESH_BINARY);

        vector<vector<Point>> contours;
        vector<Vec4i> hierarchy;
        findContours(thresh, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);

        Mat output = frame.clone(); // Clone frame for drawing

        // Loop durch alle Konturen
        for (size_t i = 0; i < contours.size(); i++) {
            // Überprüfen Sie die Konturgröße (Anzahl der Punkte) und Fläche ("Blob" -Größe)
            if (contourArea(contours[i]) > 30 && contours[i].size() > 4) {
                // Ellipse auf das Ausgabebild zeichnen (in blauer Farbe)
                RotatedRect rect = fitEllipse(contours[i]);
                ellipse(output, rect, Scalar(255, 0, 0), 1, 8); // Blaue Farbe (BGR-Format)

                // Position und Größe des Bounding-Box abrufen
                Point2f center = rect.center;
                Size2f size = rect.size;

                // Neue Finger-Blobs erkennen und ID zuweisen
                bool newTouch = true;
                int assignedID = -1; // Speichert die zugewiesene ID für die Berücksichtigung von Mehrfachzuweisungen

                for (TouchPoint& touch : touchPoints) {
                    double dist = calculateDistance(touch.position, center);
                    if (dist < 20) { // Schwellenwert für die Nähe
                        // Finger-Blob aktualisieren
                        touch.position = center;
                        touch.active = true;
                        newTouch = false;
                        assignedID = touch.id; // ID für Mehrfachzuweisungen speichern
                        break;
                    }
                }

                // Neue Finger-Blobs hinzufügen
                if (newTouch) {
                    TouchPoint newTouchPoint;
                    newTouchPoint.id = touchPoints.size();
                    newTouchPoint.position = center;
                    newTouchPoint.active = true;
                    touchPoints.push_back(newTouchPoint);
                    assignedID = newTouchPoint.id; // ID für Mehrfachzuweisungen speichern
                }

                // ID neben der Ellipse anzeigen
                putText(output, "ID:" + to_string(assignedID), Point(center.x - 5, center.y - 10), FONT_HERSHEY_SIMPLEX, 0.3, Scalar(255, 255, 255), 1);
            }
        }

        // Inaktive Finger-Blobs entfernen
        touchPoints.erase(remove_if(touchPoints.begin(), touchPoints.end(), [](const TouchPoint& touch) { return !touch.active; }), touchPoints.end());
        // Anzeige
        imshow("Original", frame);
        imshow("Blob Detection", output);

        // Warten entsprechend der Original-Framerate des Videos
        int delay = static_cast<int>(1000 / fps);
        char c = (char)waitKey(delay);
        if (c == 27)
            break;
    }

    cap.release();
    destroyAllWindows();

    //TODO: Minimal benötigte Daten: ID, x-Position, y-Position
    //Optionale Daten: Pfad, Alter, Größe, …

    return 0;
}
