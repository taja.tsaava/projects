#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
using namespace std;
using namespace cv;

#ifndef P1_IMAGEFILTER_H
#define P1_IMAGEFILTER_H

class ImageFilter {       
public:
    void filter();
};

#endif //P1_IMAGEFILTER_H
