
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Uni/Semester_7/MTMM/P1/ImageFilter.cpp" "D:/Uni/Semester_7/MTMM/P1/cmake-build-debug/CMakeFiles/P1.dir/ImageFilter.cpp.obj"
  "D:/Uni/Semester_7/MTMM/P1/main.cpp" "D:/Uni/Semester_7/MTMM/P1/cmake-build-debug/CMakeFiles/P1.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/MinGWOpenCV/opencv-4.5.5/build"
  "C:/MinGWOpenCV/opencv-4.5.5/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/core/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/flann/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/imgproc/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/ml/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/photo/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/dnn/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/features2d/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/imgcodecs/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/videoio/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/calib3d/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/highgui/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/objdetect/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/stitching/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/ts/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/video/include"
  "C:/MinGWOpenCV/opencv-4.5.5/modules/gapi/include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
