#include "ImageFilter.h"

void filter(){
    Mat first = imread("D:/Uni/Semester_7/MTMM/P1/1.jpg", cv::IMREAD_GRAYSCALE);
    Mat second = imread("D:/Uni/Semester_7/MTMM/P1/2.jpg", cv::IMREAD_GRAYSCALE);

    Mat diff;
    absdiff(first, second, diff);

    Mat blurred;
    blur(diff, blurred, Size(15, 15), Point(-1, -1), BORDER_DEFAULT);

    Mat diff2 = diff - blurred;

    Mat thresh;
    threshold(diff2, thresh, 5, 255, THRESH_BINARY);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(thresh, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE); // Use 'thresh' instead of 'binary'

    Mat output = second.clone(); // Clone 'second' image for drawing
    // Iterate through all the contours
    for (size_t i = 0; i < contours.size(); i++) {
        // Check contour size (number of points) and area ("blob" size)
        if (contourArea(contours[i]) > 30 && contours[i].size() > 4) {
            // Draw ellipse on the output image (in blue color)
            RotatedRect rect = fitEllipse(contours[i]);
            ellipse(output, rect, Scalar(255, 0, 0), 1, 8); // Blue color (BGR format)
            // Get position and size of the bounding box
            Point2f center = rect.center;
            Size2f size = rect.size;
            // Print position, size, and ID information
            cout << "Object " << i << " - Center: (" << center.x << ", " << center.y << "), Size: (" << size.width << ", " << size.height << "), ID: " << i << endl;
            // Display ID next to the ellipse
            putText(output, "ID:" + to_string(i), Point(center.x - 5, center.y - 10), FONT_HERSHEY_SIMPLEX, 0.3, Scalar(255, 255, 255), 1);
        }
    }

    /*cvtColor(thresh, out, COLOR_GRAY2BGR);
    for(vector<Point> cont: contours)
    {
        Rect box = boundingRect(cont);
        rectangle(out, box, Scalar(255, 0, 0));
    }*/

    /*
    cv::imshow("1", first);
    cv::imshow("2", second);
    */
    imshow("Difference", diff);
    imshow("Blur", blurred);
    imshow("Difference with Blur", diff2);
    imshow("Threshold", thresh);
    imshow("Output", output);
}
