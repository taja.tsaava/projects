#include <iostream>
#include <rplidar.h>
#include <cmath>
#include <fstream>

#define LIST_SIZE 8192
#define mapSize 100
#define sizeOfCell 0.025
#define angleTolerance 1
#define distanceTolerance 0.02



using namespace rp;
using namespace sl;
using namespace std;

struct AD {
    float angle;
    float dist;
};

struct gridMap{
    float x;
    float y;
    bool isWall = false;
    AD angDis;
};

size_t nodeCount;

gridMap dataMap[mapSize][mapSize];
sl_lidar_response_measurement_node_hq_t nodes[LIST_SIZE];
sl_lidar_response_measurement_node_hq_t* nodeBuffer = nodes;
AD adList[LIST_SIZE];
AD* adBuffer = adList;


pthread_t dataT;
pthread_mutex_t mutex;
pthread_cond_t condition;
bool lock = false;

//Dummy
class Car{
    void drive(){};
    void stopAndDoStuffAfterDetectionComplete(){};
};

float m_sqrt(const float &n){
    static union{int i; float f;} u;
    u.i = 0x5F375A86 - (*(int*)&n >> 1);
    return (int(3) - n * u.f * u.f) * n * u.f * 0.5f;
}

float getDistForCell(const float& x, const float& y){
    float back = m_sqrt(((x*x)+(y*y)));
    return back;
}

float calculateAngleBetweenVectors(const double& x1, const double& x2, const double& y1, const double& y2){
    double dot;
    double det;
    double angle;

    dot = x1*x2 + y1*y2;
    det = x1*y2 - y1*x2;
    angle = atan2(det, dot);
    angle = angle * (180.0/3.141592653589793238463);

    if(angle<0) angle = angle + 360;
    return angle;
}

const float x = 0.0; //Vektor zum Betrachten für den Winkel zwischen Zellpunkt
const float y = 100;
void quadrantOR(){

    float off = 0.05;
    float offY = 0.00;

    for(u_int i = (mapSize/2)-1; i>0; i--){
        for(u_int j = (mapSize/2)-1; j<mapSize; j++){
            if(i != 0 && i != mapSize-1 && j != 0 && j != mapSize-1){
                if(j == (mapSize/2)-1){
                    dataMap[i][j].x = 0;
                }else{
                    dataMap[i][j].x = dataMap[i][j - 1].x + off;
                }
                dataMap[i][j].y = offY;
                dataMap[i][j].angDis.angle = calculateAngleBetweenVectors(dataMap[i][j].x, x, dataMap[i][j].y, y);
                dataMap[i][j].angDis.dist = getDistForCell(dataMap[i][j].x, dataMap[i][j].y);
            }
        }
        offY += 0.05;
    }
}

void quadrantUR() {

    float offX = 0.0;
    float offY = 0.0;
    for (u_int i = (mapSize / 2) - 1; i < mapSize; i++) {
        for (u_int j = (mapSize / 2) - 1; j < mapSize; j++) {
            if (i != 0 && i != mapSize - 1 && j != 0 && j != mapSize - 1) {
                if(i == (mapSize / 2) - 1 && j == (mapSize / 2) - 1) {/*offX += 0.05;*/ continue;}
                dataMap[i][j].x = dataMap[i][j].x + offX;
                dataMap[i][j].y = offY;
                dataMap[i][j].angDis.angle = calculateAngleBetweenVectors(dataMap[i][j].x, x, dataMap[i][j].y, y);
                dataMap[i][j].angDis.dist = getDistForCell(dataMap[i][j].x, dataMap[i][j].y);
                offX += 0.05;
            }
        }
        offX = 0.0;
        offY -= 0.05;
    }
}

void quadrantUL(){

    float offX = 0.0;
    float offY = 0.0;

    for (u_int i = (mapSize / 2) - 1; i < mapSize; i++) {
        for (u_int j = (mapSize / 2) - 1; j > 0; j--) {
            if (i != 0 && i != mapSize - 1 && j != 0 && j != mapSize - 1){
                dataMap[i][j].x = offX;
                dataMap[i][j].y = offY;
                dataMap[i][j].angDis.angle = calculateAngleBetweenVectors(dataMap[i][j].x, x, dataMap[i][j].y, y);
                dataMap[i][j].angDis.dist = getDistForCell(dataMap[i][j].x, dataMap[i][j].y);
                offX -= 0.05;
            }
        }
        offX = 0.0;
        offY -= 0.05;
    }
}

void quadrantOL(){

    float offX = 0.0;
    float offY = 0.0;
    for(u_int i = (mapSize/2)-1; i>0; i--){
        for(u_int j = (mapSize/2)-1; j>0; j--){
            if(i != 0 && i != mapSize-1 && j != 0 && j != mapSize-1){
                dataMap[i][j].x = offX;
                dataMap[i][j].y = offY;
                dataMap[i][j].angDis.angle = calculateAngleBetweenVectors(dataMap[i][j].x, x, dataMap[i][j].y, y);
                dataMap[i][j].angDis.dist = getDistForCell(dataMap[i][j].x, dataMap[i][j].y);
                offX -= 0.05;
            }
        }
        offX = 0.0;
        offY += 0.05;
    }
}
void markObstacleInMap(const AD& ad){

    float quandrantDecider = ad.angle;

    if(quandrantDecider >= 0.0 && quandrantDecider <= 90.00){
        for(u_int i = (mapSize/2)-1; i>0; i--){
            for(u_int j = (mapSize/2)-1; j<mapSize; j++){
                if(i != 0 && i != mapSize-1 && j != 0 && j != mapSize-1) {
                    if(dataMap[i][j].angDis.angle-angleTolerance<ad.angle && dataMap[i][j].angDis.angle+angleTolerance>ad.angle){
                        if(dataMap[i][j].angDis.dist-distanceTolerance<ad.dist && dataMap[i][j].angDis.dist+distanceTolerance>ad.dist){
                            dataMap[i][j].isWall = true;
                        }
                    }
                }
            }
        }
    }
    if(quandrantDecider >= 90.0 && quandrantDecider <= 180.00){
        for (u_int i = (mapSize / 2) - 1; i < mapSize; i++) {
            for (u_int j = (mapSize / 2) - 1; j < mapSize; j++) {
                if (i != 0 && i != mapSize - 1 && j != 0 && j != mapSize - 1){
                    if(dataMap[i][j].angDis.angle-angleTolerance<ad.angle && dataMap[i][j].angDis.angle+angleTolerance>ad.angle){
                        if(dataMap[i][j].angDis.dist-distanceTolerance<ad.dist && dataMap[i][j].angDis.dist+distanceTolerance>ad.dist){
                            dataMap[i][j].isWall = true;
                        }
                    }
                }
            }
        }
    }

    if(quandrantDecider >= 180.0 && quandrantDecider <= 270.00){
        for (u_int i = (mapSize / 2) - 1; i < mapSize; i++) {
            for (u_int j = (mapSize / 2) - 1; j > 0; j--) {
                if (i != 0 && i != mapSize - 1 && j != 0 && j != mapSize - 1) {
                    if(dataMap[i][j].angDis.angle-angleTolerance<ad.angle && dataMap[i][j].angDis.angle+angleTolerance>ad.angle){
                        if(dataMap[i][j].angDis.dist-distanceTolerance<ad.dist && dataMap[i][j].angDis.dist+distanceTolerance>ad.dist){
                            dataMap[i][j].isWall = true;
                        }
                    }
                }
            }
        }
    }

    if(quandrantDecider >= 270.0 && quandrantDecider <= 360.00){
        for(u_int i = (mapSize/2)-1; i>0; i--){
            for(u_int j = (mapSize/2)-1; j>0; j--) {
                if (i != 0 && i != mapSize - 1 && j != 0 && j != mapSize - 1) {
                    if(dataMap[i][j].angDis.angle-angleTolerance<ad.angle && dataMap[i][j].angDis.angle+angleTolerance>ad.angle){
                        if(dataMap[i][j].angDis.dist-distanceTolerance<ad.dist && dataMap[i][j].angDis.dist+distanceTolerance>ad.dist){
                            dataMap[i][j].isWall = true;
                        }
                    }
                }
            }
        }
    }

}


void* processData(void* ad){

    pthread_mutex_lock(&mutex);
    AD* angle_distance = (AD*) ad;

    while(!lock) {
        lock = true;
        std::ofstream fd;
        fd.open("max_isn_netter.txt");
        for(unsigned int i = 0; i <  nodeCount; i++)
            fd << angle_distance[i].angle << " " << angle_distance[i].dist;


        fd.close();
    }

    lock = false;
    pthread_mutex_unlock(&mutex);
}


int main() {

    for(int i = 0; i<mapSize; i++){
        for(int j = 0; j<mapSize; j++){
            dataMap[i][j].x = 0.0;
            dataMap[i][j].y = 0.0;
            dataMap[i][j].angDis.angle = 0;
            dataMap[i][j].angDis.dist = 0;
        }
    }

    quadrantOR();
    quadrantUR();
    quadrantUL();
    quadrantOL();


    Result<IChannel *> channel = createSerialPortChannel("/dev/ttyUSB0", 115200);

    ILidarDriver *lidar = *createLidarDriver();
    auto res = (*lidar).connect(*channel);
    if (SL_IS_OK(res)) {
        sl_lidar_response_device_info_t deviceInfo;
        res = (*lidar).getDeviceInfo(deviceInfo);
        if (SL_IS_OK(res)) {
            printf("Model: %d, Firmware Version: %d.%d, Hardware Version: %d\n",
                   deviceInfo.model,
                   deviceInfo.firmware_version >> 8, deviceInfo.firmware_version & 0xffu,
                   deviceInfo.hardware_version);
        } else {
            fprintf(stderr, "Failed to get device information from LIDAR %08x\r\n", res);
        }
    } else {
        fprintf(stderr, "Failed to connect to LIDAR %08x\r\n", res);
    }
    // TODO
    u_int8_t failGetDataCount = 0;
    bool scanIsOk = true;
    LidarScanMode scanMode;
    lidar->startScan(false, true, 0, &scanMode);
    while (scanIsOk) {

        lidar->grabScanDataHq(nodeBuffer,nodeCount);
        lidar->ascendScanData(nodeBuffer, nodeCount);

        float distance_32 = 0;
        float  angle_16 = 0;
        AD ad{angle_16,distance_32};

        for (int i = 0; i < nodeCount; i++) {
            ad.angle = nodes[i].angle_z_q14 * 90.f / (1 << 14);
            ad.dist = nodes[i].dist_mm_q2 / 1000.f / (1 << 2);
            adList[i] = ad;
        }

        //if(pthread_cond_init(&condition,nullptr) == 0){std::cerr << "condition error" << std::endl; break;}

        pthread_create(&dataT, NULL, processData, (void *) adBuffer); //pthread_join(*dataT, NULL); Falls wir auf den Thread warten wollen irgendwo

        pthread_join(dataT, NULL);
        break;
        if (SL_IS_FAIL(res)) {
            // failed to get scan data
            failGetDataCount += 1;
            if(failGetDataCount == 100) {
                scanIsOk = false;
                std::cerr << "Scan failed" << std::endl;
                break;
            }
        }
    }

    delete lidar;
    delete* channel;

    return 1;
}
