import math
from binary_search_tree import BinarySearchTree

import cv2 as cv

# base vector for grid initialization
base_vector = (0, 100)

# grid parameter
grid_size = 150
cell_size = 0.02
grid_center = int(grid_size / 2 - 1)
grid_map = [[0 for i in range(grid_size)] for j in range(grid_size)]

tree = BinarySearchTree()


def main():
    setup_grid_map()

    # for i in range(grid_size):
    #    for j in range(grid_size):
    #        print("[" + str(grid_map[i][j].x) + "," + str(grid_map[i][j].y) + "," + str(grid_map[i][j].angle) + "," + str(grid_map[i][j].dist) + "] ", end="")
    #    print("\n")
    initialize_search_tree()
    test_data = get_test_data_list()
    for data in test_data:
        detect_obstacle(float(data['angle']), float(data['dist']))

    for i in range(grid_size):
        for j in range(grid_size):
            if j == grid_center and i == grid_center:
                print("O", end="")
                continue
            if grid_map[i][j].is_obstacle:
                print("*", end="")
            else:
                print("_", end="")
        print()


def get_test_data_list():
    test_data_file = open('./res/test_data.txt', 'r')
    data_list = []
    lines = test_data_file.readlines()
    for line in lines:
        temp = line.split(' ')
        angle_dist = {'angle': temp[0], 'dist': temp[1]}
        data_list.append(angle_dist)
    return data_list


def initialize_search_tree():
    tree.root.angle = grid_map[grid_center + 1][grid_center].angle
    tree.root.dist = grid_map[grid_center + 1][grid_center].dist
    tree.root.i, tree.root.j = grid_center + 1, grid_center
    for i in range(grid_size):
        for j in range(grid_size):
            if i == grid_center - 1 and j == grid_center:
                continue
            tree.insert_node(grid_map[i][j].angle, grid_map[i][j].dist, i=i, j=j)
    print_tree(tree.root)


def print_tree(node, level=0):
    if node is not None:
        print_tree(node.left, level + 1)
        print(' ' * 4 * level + '-> ' + '[' + str(node.angle) + ', ' + str(node.dist) + ']')
        print_tree(node.right, level + 1)


class Cell:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.angle = 0
        self.dist = 0
        self.is_obstacle = 0


def get_dist_for_cell(x, y):
    # returns the length of the given vector
    return math.sqrt((x ** 2) + (y ** 2))


def get_angle_for_cell(x, y):
    # returns the angle of the given vector
    dot = base_vector[0] * x + base_vector[1] * y
    det = base_vector[0] * y - base_vector[1] * x
    angle = math.atan2(dot, det)
    angle = angle * (180 / math.pi) - 90
    if angle < 0:
        angle += 360
    return angle


def setup_grid_map():
    for i in range(grid_size):
        for j in range(grid_size):
            grid_map[i][j] = Cell()
    initialize_upper_right_quadrant()
    initialize_upper_left_quadrant()
    initialize_lower_right_quadrant()
    initialize_lower_left_quadrant()


def initialize_upper_right_quadrant():
    y_offset = 0
    for i in range(grid_center, 0, -1):
        for j in range(grid_center, grid_size):
            if i == grid_center and j == grid_center:
                continue
            if i == 0 or i == grid_size - 1 or j == 0 or j == grid_size - 1:
                continue
            else:
                if j == grid_center:
                    grid_map[i][j].x = 0.0
                else:
                    grid_map[i][j].x = grid_map[i][j - 1].x + cell_size
                grid_map[i][j].y = y_offset
                grid_map[i][j].dist = get_dist_for_cell(grid_map[i][j].x, grid_map[i][j].y)
                grid_map[i][j].angle = get_angle_for_cell(grid_map[i][j].x, grid_map[i][j].y)
        y_offset += cell_size


def initialize_upper_left_quadrant():
    y_offset = 0
    for i in range(grid_center, 0, -1):
        for j in range(grid_center, 0, -1):
            if i == grid_center and j == grid_center:
                continue
            if i == 0 or i == grid_size or j == 0 or j == grid_size:
                continue
            else:
                if j == grid_center:
                    grid_map[i][j].x = 0.0
                else:
                    grid_map[i][j].x = grid_map[i][j + 1].x - cell_size
            grid_map[i][j].y = y_offset
            grid_map[i][j].dist = get_dist_for_cell(grid_map[i][j].x, grid_map[i][j].y)
            grid_map[i][j].angle = get_angle_for_cell(grid_map[i][j].x, grid_map[i][j].y)
        y_offset += cell_size


def initialize_lower_right_quadrant():
    y_offset = 0
    for i in range(grid_center, grid_size):
        for j in range(grid_center, grid_size):
            if i == grid_center and j == grid_center:
                continue
            if i == 0 or i == grid_size - 1 or j == 0 or j == grid_size - 1:
                continue
            else:
                if j == grid_center:
                    grid_map[i][j].x = 0.0
                else:
                    grid_map[i][j].x = grid_map[i][j - 1].x + cell_size
            grid_map[i][j].y = y_offset
            grid_map[i][j].dist = get_dist_for_cell(grid_map[i][j].x, grid_map[i][j].y)
            grid_map[i][j].angle = get_angle_for_cell(grid_map[i][j].x, grid_map[i][j].y)
        y_offset -= cell_size


def initialize_lower_left_quadrant():
    y_offset = 0
    for i in range(grid_center, grid_size):
        for j in range(grid_center, 0, -1):
            if i == grid_center and j == grid_center:
                continue
            if i == 0 or i == grid_size - 1 or j == 0 or j == grid_size - 1:
                continue
            else:
                if j == grid_center:
                    grid_map[i][j].x = 0.0
                else:
                    grid_map[i][j].x = grid_map[i][j + 1].x - cell_size
            grid_map[i][j].y = y_offset
            grid_map[i][j].dist = get_dist_for_cell(grid_map[i][j].x, grid_map[i][j].y)
            grid_map[i][j].angle = get_angle_for_cell(grid_map[i][j].x, grid_map[i][j].y)
        y_offset -= cell_size


def detect_obstacle(angle, dist):
    curr = tree.root
    prev = Cell()
    dist_diff = 10
    prev_dist_diff = 0
    while round(curr.angle) != round(angle):
        prev = curr
        if angle < curr.angle:
            curr = curr.left
        elif angle > curr.angle:
            curr = curr.right

    prev_dist_diff = dist_diff
    dist_diff = abs(curr.dist - dist)

    while prev_dist_diff > dist_diff:
        prev = curr
        prev_dist_diff = dist_diff

        #if prev.dist < curr.dist:
        #    curr = curr.left
        #elif prev.dist > curr.dist:
        #    curr = curr.right

        if curr.right is not None:
            curr = curr.right
        else:
            curr = curr.left

        if curr is None:
            break
        dist_diff = abs(curr.dist - dist)

    if prev is None:
        prev = curr
    grid_map[prev.i][prev.j].is_obstacle = True


if __name__ == "__main__":
    main()
