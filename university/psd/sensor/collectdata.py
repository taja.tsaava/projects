import sys
sys.path.insert(1, '/home/monkeyderiven/PycharmProjects/autonomous-parking/autonomous-parking-python')
import parking_spot_detection as psd
#import sensor_value_formatter
from rplidar import RPLidar
lidar = None
scan_data_path = "res/scan_data.txt"


def main():
    connect()
    #formatter = Formatter()
    for i, scan in enumerate(lidar.iter_scans(scan_type='express')):
        rotated_scan = []
        for data in scan:
            new_data = [data[0], data[1], data[2]/1000]
            rotated_scan.append(new_data)
        final_parking_corner = psd.find_parking_spot(rotated_scan)
    #    for data in scan:
    #    formatter.add_entry(data[1], data[2])
    #    final_json = f.export_json_string()
    #    networking.set_sensor_values(final_json)
        if len(final_parking_corner) == 0:
            print()
        else:
            print("parking spot found!")
        break
    disconnect()


def connect():
    global lidar
    lidar = RPLidar('/dev/ttyUSB0')
    info = lidar.get_info()
    print(info)
    health = lidar.get_health()
    print(health)
    return lidar


def write_scan_to_file(path):
    clear_file(path)
    file = open(path, 'a')
    for i, scan in enumerate(lidar.iter_scans(scan_type='express')):
        if i == 0:
            continue
        for data in scan:
            line = str(data[1]) + " " + str(data[2]) + "\n"
            file.write(line)
        break
    file.close


def clear_file(path):
    file = open(path, 'w')
    file.close


def disconnect():
    lidar.stop()
    lidar.stop_motor()
    lidar.disconnect()


if __name__ == "__main__":
    main()