
class Node:
    def __init__(self, angle=0, dist=0, i=0, j=0):
        self.angle = angle
        self.dist = dist
        self.left = None
        self.right = None
        self.i = i
        self.j = j


class BinarySearchTree:
    def __init__(self):
        self.root = Node()

    def set_root(self, angle, dist):
        self.root.angle = angle
        self.root.dist = dist

    def insert_node(self, angle, dist, i, j):
        curr = self.root
        prev = None
        left = None
        while curr is not None:
            prev = curr
            if int(angle) < int(curr.angle):
                curr = curr.left
                left = True
            elif int(angle) > int(curr.angle):
                curr = curr.right
                left = False
            else:
                if dist <= curr.dist:
                    curr = curr.left
                    left = True
                else:
                    curr = curr.right
                    left = False
        if left:
            prev.left = Node(angle=angle, dist=dist, i=i, j=j)
        else:
            prev.right = Node(angle=angle, dist=dist, i=i, j=j)




