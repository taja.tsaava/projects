
import math
import cython
from libc.math cimport cos, sin, pi
from libc.math cimport sqrt

cdef double my_array[][2]

cdef int degrees = 360 / 135
cdef double radians = deg_to_rad(degrees)
cdef double cos_rad = cos(radians)
cdef double sin_rad = sin(radians)
cdef double x, y, offset_x, offset_y, adjusted_x, adjusted_y, qx, qy
cdef list obstacle_points_list = []
cdef list straightened_list = []
cdef get_line_endpoint(tuple start_point, points_list):
    cdef double max_dist = -1
    cdef tuple end_point = None
    for point in points_list:
        if get_distance(start_point, point) > max_dist:
            max_dist = get_distance(start_point, point)
            end_point = point
    return end_point


cdef remove_line_points_from_obstacle_list(line):
    cdef int i
    for i in range(len(obstacle_points_list)):
        if i == len(line)-1:
            obstacle_points_list.remove(obstacle_points_list[0])
            return
        obstacle_points_list.remove(obstacle_points_list[0])

def get_distance(tuple[double] point1, tuple[double] point2):
    cdef double dx = point2[0] - point1[0]
    cdef double dy = point2[1] - point1[1]
    cdef double dis = sqrt(dx * dx + dy * dy)
    return dis

def get_line_points(tuple[double] start_point, tuple[double] end_point):
    # list of points that are on the line
    cdef list points = [start_point]
    # amount of points that should be on the line
    cdef int points_count = int(get_distance(start_point, end_point) * 50)
    cdef double x_diff, y_diff
    try:
        x_diff = (start_point[0] - end_point[0]) / points_count * -1
    except ZeroDivisionError:
        x_diff = 0
    try:
        y_diff = (start_point[1] - end_point[1]) / points_count * -1
    except ZeroDivisionError:
        y_diff = 0
    cdef int i
    for i in range(points_count - 2):
        points.append([points[i][0] + x_diff, points[i][1] + y_diff])
    points.append(end_point)
    return points

cdef inline double dist(tuple[double] a, tuple[double] b):
    cdef double dx, dy
    dx = a[0] - b[0]
    dy = a[1] - b[1]
    return sqrt(dx * dx + dy * dy)

def is_point_on_artificial_line(list artificial_line_points, tuple[double] point, double inaccuracy_tolerance):
    cdef int i = 0
    cdef double dist = 0.0
    cdef double min_dist = 99999
    for i in range(len(artificial_line_points)):
        dist = math.dist(artificial_line_points[i], point)
        if dist <= min_dist:
            min_dist = dist

    if inaccuracy_tolerance >= min_dist:
        return True

    return False

cdef inline double deg_to_rad(double degrees):
    return degrees * (pi / 180)
def rotate(point, origin):

    x, y = point
    offset_x, offset_y = origin
    adjusted_x = (x - offset_x)
    adjusted_y = (y - offset_y)

    qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
    qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y
    return qx, qy


# Cython version of the get_best_line_params function
@cython.boundscheck(False)  # disable bounds checking for performance
@cython.wraparound(False)  # disable negative indexing for performance
cpdef get_best_line_params(list[tuple[double]] artificial_line, list[tuple[double]] obstacle_points_list):
    cdef int line_rotations = 135
    cdef double line_inaccuracy_offset = 0.01
    cdef list[int] best_line_params = [0, 0, []]
    cdef int i, hits, miss
    cdef list[tuple[double]] points_on_line
    cdef int j
    cdef tuple[double] point
    cdef list[tuple[double]] artificial_line_points

    for i in range(line_rotations):
        hits = 0
        miss = 0
        points_on_line = []
        # counts points which are on the artificial line
        j = 0
        for j in range(len(obstacle_points_list)):
            point = obstacle_points_list[j]
            artificial_line_points = get_line_points(artificial_line[0], artificial_line[1])
            if is_point_on_artificial_line(artificial_line_points, point, line_inaccuracy_offset):
                hits += 1
                miss = 0
                # obstacle_points_list.remove(point)
                points_on_line.append(point)
            else:
                miss += 1
                if miss == 2:
                    break
        if hits > best_line_params[0]:
            best_line_params = [hits, artificial_line, points_on_line]
        artificial_line = [artificial_line[0], rotate(artificial_line[1], artificial_line[0])]
    return best_line_params

cdef straighten_lines():
    #artifical line to detect hits on this line [start_point, end_point]
    cdef list line = []
    cdef int line_rotations = 135
    cdef int line_rotation_offset = 360 / line_rotations
    cdef float line_inaccuracy_offset = 0.010
    cdef float line_length = .4
    cdef list[tuple[double]] artificial_line = []
    cdef list[tuple[double]] straightened_list = []
    cdef best_line_params = []
    cdef end_point = []
    cdef line_points = []
    straightened_list.append(obstacle_points_list[0])
    obstacle_points_list.remove(obstacle_points_list[0])
    cdef int debugCounter = 0
    cdef int blubCounter = 0
    while len(obstacle_points_list) > 0:
        artificial_line = [straightened_list[len(straightened_list) - 1],
                           [straightened_list[len(straightened_list) - 1][0],
                            straightened_list[len(straightened_list) - 1][1] + line_length]]

        best_line_params = get_best_line_params(artificial_line, obstacle_points_list)
        #append the current point to straightened_list if it does not belong to any line
        if len(best_line_params[2]) == 0:
            straightened_list.append(obstacle_points_list[0])
            obstacle_points_list.remove(obstacle_points_list[0])
            blubCounter += 1
            continue
        #removes the points belonging to the line from the obstacle list
        remove_line_points_from_obstacle_list(best_line_params[2])
        end_point = best_line_params[2][len(best_line_params[2]) - 1]
        #has to be removed later
        debugCounter += 1
        if best_line_params[2][0] == end_point:
            continue
        #gets the straightened line points
        line_points = get_line_points(artificial_line[0], end_point)
        #puts the line into the straigtened list
        if len(line_points) > 0:
            straightened_list.extend(line_points)
    return straightened_list