# Author:       Mirco Janisch
# Description:

import json
import logging
import socket
from threading import Thread

import networking

RECEIVE_PORT = 8080
BUFFER_SIZE = 1024  # max size in bytes of message, TODO: determine actual length of packages

latest_datagram = None
running = True


# Create socket and bind
def create_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', RECEIVE_PORT))
    logging.info(f"Bound UDP socket to port {RECEIVE_PORT}")
    return sock


# Loop, that waits for a new real time control datagram and sets it to networking.latest_data,
# so networking.process_steering_values can process it. Done this way, so process_steering_values
# and this method can work async from each other
def receive_messages_thread(sock):
    while running:
        data, addr = sock.recvfrom(BUFFER_SIZE)  # blocking
        data = data.decode('utf-8')  # decode to string

        received_json = json.loads(data)
        payload_type = received_json["type"]

        if payload_type == networking.PayloadTypes.data.value:
            global latest_datagram
            latest_datagram = (data, addr)

        # activate sending
        networking.set_sending(True)
        networking.set_dest_ip(addr[0])

        logging.debug(f"Received message ({data}) from address {addr}")


class Server:

    # Set up a new UDP server for receiving data
    def __init__(self):
        self.sock = create_socket()
        Thread(target=receive_messages_thread, args=(self.sock,)).start()
        logging.info("Started thread for receiving messages")


if __name__ == '__main__':
    Server()
