# Author:       Mirco Janisch
# Description:  Module to provide automatic discovery for the car

import json
import logging
import socket
from threading import Thread

import networking

DISCOVER_SEND_PORT = 8083
DISCOVER_RECEIVE_PORT = 8082

BUFFER_SIZE = 1024  # max size in bytes of message, TODO: determine actual length of packages


# Create socket and bind
def create_receive_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', DISCOVER_RECEIVE_PORT))
    logging.info(f"Bound UDP socket to port {DISCOVER_RECEIVE_PORT}")
    return sock


# Create socket for sending
def create_send_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    logging.info(f"Created UDP socket for sending")
    return sock


# Functions that gets executed as a thread. Waits for incoming discovery requests and answers them
def receive_messages_thread(recv_sock):
    while True:
        data, addr = recv_sock.recvfrom(BUFFER_SIZE)  # blocking

        received_json = json.loads(data.decode('utf-8'))
        payload_type = received_json["type"]

        if payload_type == networking.PayloadTypes.discovery.value:
            ip = addr[0]
            networking.set_dest_ip(ip)  # setup own destination

            send_sock = create_send_socket()
            send_sock.sendto(data, (ip, DISCOVER_SEND_PORT))

            logging.debug(
                f"Received and returned message ({data.decode('utf-8')}) from address {addr} to{(ip, DISCOVER_SEND_PORT)}")
        else:
            logging.warning(f"Received unknown message ({data.decode('utf-8')}) from address {addr}")


class DiscoveryResponder:

    def __init__(self):
        self.sock = create_receive_socket()

        Thread(target=receive_messages_thread, args=(self.sock,)).start()
        logging.info(f"Started thread for discovery responder")


if __name__ == '__main__':
    DiscoveryResponder()
