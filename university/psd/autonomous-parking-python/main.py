# Author:       Mirco Janisch
# Description:  Main entry for program

import logging

import networking

DEBUG_LEVEL = logging.DEBUG


def main():
    # set log level
    logging.basicConfig()
    logging.getLogger().setLevel(DEBUG_LEVEL)

    # start all servers, clients and discovery services needed
    networking.prepare_connection()


if __name__ == '__main__':
    main()
