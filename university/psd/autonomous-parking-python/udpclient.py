# Author:       Mirco Janisch
# Description:  Client for sending real time data OR sensor values to the car

import logging
import socket
import threading

import networking

SEND_PORT = 8081
SENSOR_VALUE_PORT = 8085


# Create UDP socket
def create_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    logging.info(f"Created UDP socket for sending")
    return sock


# Function that keeps scheduling itself with a fixed interval to send the latest available data to the app
def send_messages_timer(sock, delay_between, is_sensor_values):
    threading.Timer(delay_between, send_messages_timer,
                    args=(sock, delay_between, is_sensor_values)).start()  # "recursive" call as thread

    # obtain IP address, form JSON message from data and send to address
    ip = networking.get_dest_ip()
    is_send = networking.get_sending()

    if ip is not None and is_send:
        if is_sensor_values:
            message = networking.get_sensor_message_data()
        else:
            message = networking.get_message_data()

        port = SENSOR_VALUE_PORT if is_sensor_values else SEND_PORT
        sock.sendto(str(message).encode(encoding='utf-8'), (ip, port))
        logging.debug(f"Send message ({message}) to address {ip}:{port}")


class Client:

    # set up the UDP client and starts it as thread
    def __init__(self, delay_between, is_sensor_values):
        self.sock = create_socket()
        send_messages_timer(self.sock, delay_between, is_sensor_values)
        type_message = "sensor values" if is_sensor_values else "status data"
        logging.info(f"Started thread for sending {type_message}")


if __name__ == '__main__':
    Client(networking.DELAY_BETWEEN_SENDING, False)
