# Author:       Mirco Janisch
# Description:  Main module for networking stuff. Also holds latest

import copy
import json
import logging
import threading
import time
from enum import Enum

import discovery_service
import drive_car
import safe_command
import udpclient
import udpserver

DELAY_BETWEEN_SENDING = 0.040  # seconds
DELAY_BETWEEN_SENDING_SENSOR_VALUES = 1  # seconds

dest_ip = None
sending = False
remote_control = True

speed = 0  # arbitrary unit
sensor_values = {}


# possible payload types
class PayloadTypes(Enum):
    data = "data"
    sensor = "sensor"
    safe = "safe"
    discovery = "discovery"
    error = "error"


# function, that fetches latest datagram to process it and starts a new thread with delay for next fetch
def process_steering_values():
    threading.Timer(DELAY_BETWEEN_SENDING, process_steering_values).start()  # "recursive" call

    if remote_control:
        dg = udpserver.latest_datagram
        if dg is not None:

            # get extract data from latest datagram
            data = json.loads(dg[0])
            payload_type = data["type"]
            if payload_type == PayloadTypes.data.value:
                r_angle = int(data["angle"])
                r_speed = int(data["speed"])

                logging.debug(f"Got (speed:{r_speed}) and (angle:{r_angle})")
                drive_car.drive_manuel(r_speed, r_angle)


# setup all the required connections to communicate with app
def prepare_connection():
    # HTTP server for safe commands
    threading.Thread(target=safe_command.start_app).start()

    # Discovery service
    discovery_service.DiscoveryResponder()

    # UDP server for receiving real time data
    udpserver.Server()
    process_steering_values()

    # UDP client for sending real time data
    udpclient.Client(DELAY_BETWEEN_SENDING, False)
    udpclient.Client(DELAY_BETWEEN_SENDING_SENSOR_VALUES, True)


# build JSON array, that can be sent
def get_message_data():
    data = {
        "time": int(time.time()),
        "speed": copy.deepcopy(speed),
    }
    return json.dumps(data)


# create a deep copy of the latest sensor value dictonary
def get_sensor_message_data():
    return copy.deepcopy(sensor_values)


# returns the IP address of the car, if known
def get_dest_ip():
    return dest_ip


# TODO Hardware Team: call this function with actual value
def set_speed(speed_value):
    global speed
    speed = speed_value


# TODO Sensor Team: call this function, giving exported JSON string from Formatter
def set_sensor_values(values):
    global sensor_values
    sensor_values = values


def set_sending(send):
    global sending
    sending = send


def set_dest_ip(ip):
    global dest_ip
    dest_ip = ip


def get_sending():
    return sending


if __name__ == '__main__':
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)

    prepare_connection()
