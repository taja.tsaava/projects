#authored by Burak Palaz
import logging
import networking
import os
import time
from auto_process import drive
from rplidar import RPLidar
SERVO_BLASTER_DEVICE_FILE = "/dev/servoblaster"

init_pwm = 150

max_pwm_sp = 200
min_pwm_sp = 100

min_angle = -26
max_angle = 26
min_pwm_ag = 110
max_pwm_ag = 295
pwm_angle_compensation = 6

break_pwm_dif = 11
min_pwm_dif = 7

last_dir = "forward"
is_driving = False

try:
    device = os.open(SERVO_BLASTER_DEVICE_FILE, os.O_RDWR | os.O_SYNC)
except:
    logging.warning(f"File not found ({SERVO_BLASTER_DEVICE_FILE})")


def drive_manuel(speed, angle):
    change_pwm_speed(speed)
    change_pwm_angle(angle)
    #networking.set_speed(calc_speed())



def drive_slow(direction, angle):
    global is_driving
    print('drive_slow')
    if not direction == last_dir:
        change_dir()
    if direction == "forward":
        if not is_driving:
            start_drive(direction)
        change_pwm_speed(init_pwm + min_pwm_dif)
        # drive_glide()  # just testing

    if direction == "backward":
        if not is_driving:
            start_drive(direction)
        change_pwm_speed(init_pwm + min_pwm_dif)

    if direction == "stop":
        if is_driving:
            change_pwm_speed(min_pwm_sp)
            is_driving = False;
        else:
            change_pwm_speed(init_pwm)

    change_pwm_angle(map_angle_on_pwm(angle))


def start_drive(direction):
    if direction == "backward":
        mult = -1
    else:
        mult = 1

    change_pwm_speed(init_pwm + mult * break_pwm_dif)
    time.sleep(0.5)
    change_pwm_speed(init_pwm + mult * min_pwm_dif)


def change_dir():
    global is_driving
    global last_dir
    if last_dir == "backward":
        change_pwm_speed(init_pwm - 1);
        change_pwm_speed(init_pwm)
        last_dir = "forward"
        is_driving = False
    else:
        change_pwm_speed(init_pwm + 1);
        change_pwm_speed(init_pwm)
        last_dir = "backward"
        is_driving = False


def change_pwm_speed(pwm):
    message: str = f"1={str(pwm)}%"
    logging.debug(f"Message for acceleration is {message}")
    write_to_device_file(message)


def change_pwm_angle(pwm):
    message: str = f"0={str(pwm)}%"
    logging.debug(f"Message for steering is {message}")
    write_to_device_file(message)


def map_angle_on_pwm(angle):
    angle_range = max_angle - min_angle
    if angle > 0:
        angle_pwm_dif = max_pwm_ag - 150 + pwm_angle_compensation
        return (angle / angle_range) * angle_pwm_dif + 150
    else:
        if angle < 0:
            angle_pwm_dif = 150 + pwm_angle_compensation - min_pwm_ag
            return (angle / angle_range) * angle_pwm_dif + 150
        else:
            return 150 + pwm_angle_compensation


def calc_speed():
    global counter
    global last_counter_reset
    speed = ((counter * 0.7)/ (last_counter_reset - time.time))
    counter = 0
    last_counter_reset = time.time
    return speed


counter = 0
last_counter_reset = time.time


def write_to_device_file(message):
    try:
        os.write(device, str.encode(message + '\n'))
        logging.debug(f"Wrote ({message}) to ({SERVO_BLASTER_DEVICE_FILE})")
    except:
        logging.warning(f"Can't write to {SERVO_BLASTER_DEVICE_FILE}")


#demo to test car movement with a fixed path
def drive_demo():
    # steer left
    drive_manuel(50, 70)

    # steer right
    time.sleep(2)
    drive_manuel(50, 30)

    # forward
    time.sleep(2)
    drive_manuel(55, 50)

    # backward
    time.sleep(1)
    drive_manuel(45, 50)
    time.sleep(1)
    drive_manuel(45, 50)

    # back to neutral
    time.sleep(2)
    drive_manuel(50, 50)

#old method not used in final algorithm
#detects corners
def Corner180():
    cnt = 0
    var = []
    for i in range(0, len(inFloats), 2):
        if int(inFloats[i]) == 180:
            cnt += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            var.append(inFloats[i])
            var.append(inFloats[i + 1])

    if cnt == 3:
        for i in range(1, len(var) - 1, 2):
            if var[i] > var[i + 1]:
                print("Corner detected")
                sim.pauseSimulation()


#compares specific angles used to switch to another state
def compareAnglesDictionary():
    global d
    avg180 = 0
    avg90 = 0
    # Check if dictionary has values stored for either angle
    #if 90 in d and 180 in d:
    if 90 in d and 0 in d:
        # Only compare angles when there are 3 of each
        #itr = len(d[180])
        itr = len(d[0])
        #for a in d[180]:
        for a in d[0]:
            avg180 += a
        avg180 /= itr

        #eigentlich 90 nicht 270
        itr = len(d[90])
        for a in d[90]:
            avg90 += a
        avg90 /= itr
        print("180 avg distance: ", avg180)
        print("90 avg distance: ", avg90)
        if(avg180 and avg90 < 0.90):
            if abs(avg180 - avg90) < 0.20:
                # print("Distance similar with ", abs(avg180 - avg90), " tolerance")
                # sim.pauseSimulation()
                print("Average Distace 180 Degree: ", avg180)
                print("Average Distance 90 Degree: ", avg90)
                print("Distance difference of: ", abs(avg180 - avg90))
                #time.sleep(10)
                return True

#old method not used in final algorithm
#checks if obstacle is close to the car
def RearAndSideDetectionDictionary():
    global d
    for c in d:
        #if c < 0.4:
        if d[c][1] < 0.4:
            # sim.pauseSimulation()
            return True

#checks for collisions within 0,3m from the sensor
def checkCollision():
    global d
    for i in d:
        #print('i: ', i)
        #print('value: ', d[i][0])
        #if d[i][0] < 0.4:
        print([i][0], " Grad mit ", d[i][0], "Distanz")
        if d[i][0] < 0.3:
            print("Collision")
            return True



#checks for parallelism to the wall using angles with an offset from 90 degrees
def checkParallelismDictionary(offset):
    global d
    avga = 0
    avgb = 0
    # Check if dictionary has values stored for either angle
    #if (90 - offset) and (90 + offset) in d:
    if 80 and 100 in d:
        # Only compare angles when there are 3 of each
        #if len(d[90 - offset]) == 3 and len(d[180 + offset]) == 3:
        print("80 and 100 are in the list")
        itr = len(d[90 - offset])
        print("Anzahl: ", itr)
        print("d[80]: ", d[80])
        #for a in d[90 - offset]:
        for a in d[80]:
            #avga += d[90 - offset][a]
            avga += d[80][a]
        avga = avga / itr
        print("80: ", avga)

        itr = len(d[90 + offset])
        for a in d[90 + offset]:
            avgb += d[90 + offset][a]
        avgb /= itr
        print("100: ", avgb)

        if abs(avga - avgb) < 0.06:
            print("Distance similar with ", abs(avga - avgb), " tolerance")
            # sim.pauseSimulation()
            return True



#checks for parallelism to the wall using the distance values from 80 and 100 degrees
def checkPara():
    global d
    a = 0
    b = 0
    if 80 and 100 in d:
        a = d[80][0]
        b = d[100][0]
        print("80: ", a)
        print("100: ", b)
        print((abs(a -b)))
        if abs(a - b) < 0.025:
            print("Distanz unter 0.05 mit ", abs(a - b))
            return True


#returns the required distance values to check the cars position inside the parking spot
def positioningDictionary():
    global d
    avg0 = 0
    avg180 = 0
    # Check if dictionary has values stored for either angle
    if all(key in d for key in (0, 180)):
        # if 0 and 180 in d:
        # Only compare angles when there are 3 of each
        #if len(d[0]) == 3 and len(d[180]) == 3:
        itr = len(d[0])
        for a in d[0]:
            avg0 += a
        avg0 /= itr

        itr = len(d[180])
        for a in d[180]:
            avg180 += a
        avg180 /= itr
        # print("0 degree avg distance: ", avg0)
        # print("180 degree avg distance: ", avg180)
        return avg0, avg180
    else:
        print('0 und 180 nicht in der Liste')
        return None


#returns the required distance values to check the cars position inside the parking spot
def pos():
    global d
    global eins, zwei
    a = 0
    b = 0
    #if all(key in d for key in (0, 180)):
    #print("0 und 180 in d")
    #print("0: ", d[0][0])
    #print('180: ', d[180][0])
    a = d[0][0]
    b = d[180][0]
    eins = d[0][0]
    zwei = d[180][0]
    print("eins in pos: ", eins)
    print("zwei in pos: ", zwei)
    #print("0 :", a)
    #print("180 :", b)
    return a, b
    #else:
        #print("0 und 180 nicht vorhanden")
        #return None


#checks whether the positioning of the car within the parking spot is parallel to both obstacles
def posLoop(): #param1, param2):
    global a, b
    global d
    global isBackwards, algorithmus
    if 180 and 0 in d:
        a = d[180][0]
        b = d[0][0]
    #if param1 < param2:
    #if eins < zwei:
    #if d[180][0] < d[0][0]:
        if a < b:
            #if not isBackwards:
            drive('backward', 0)
                #isBackwards = True
    #elif param2 < param1:
    #elif eins > zwei:
    #elif d[180][0] > d[0][0]:
        elif a > b:
            #if isBackwards:
            drive('forward', 0)
                #isBackwards = False

    #if abs(param1 - param2) < 0.01:
    #if abs(d[180][0] - d[0][0]) < 0.01:
        if abs(a - b) < 0.3:
            print('Absolute Differenz: ', abs(a-b))
        # print("Tolerance from perfect parallel distance: ", abs(param1 - param2))
            print("Positioned!")
            change_pwm_speed(150)
            algorithmus = False
        # sim.pauseSimulation()
            return True
        else:
            print("Not positioned yet...")
            print('Vorne :', a)
            print('Hinten :', b)
            return False
    else:
        print("180 und 0 nicht in Liste")


#processes sensor data to use for parking algorithm (from CoppeliaSim)
def addDict():
    sample_dict = {}
    for x in range(0, len(inFloats), 3):
        if int(inFloats[x]) not in sample_dict:
            sample_dict.setdefault(inFloats[x], []).append(inFloats[x + 1])
            # sample_dict.setdefault(int(inFloats[x]), []).append(inFloats[x + 1])
        else:
            sample_dict.setdefault(inFloats[x], []).append(inFloats[x + 1])
    return sample_dict


#processes sensor data to use for parking algorithm (from actual sensor)
def addToList():
    sample_list = {}
    for x in range(0, len(obstacle_list), 1):
        if int(obstacle_list[x][1]) == 0:
            sample_list.setdefault(int(0), []).append(int(obstacle_list[x][2])/1000)
        else:
            #sample_list.setdefault((360-(int(obstacle_list[x][1])+180)%359), []).append(int(obstacle_list[x][2])/1000)
            #sample_list.setdefault(360-int(obstacle_list[x][1]), []).append(int(obstacle_list[x][2])/1000)
            sample_list.setdefault(int(obstacle_list[x][1]), []).append(int(obstacle_list[x][2])/1000)
    return sample_list


#parking algorithm
def BurakAlgorithmus(obstacle_list):
    global doOnce, doOnce2, position, notParallel, d, allowedToCheck, parked
    global finito, algorithmus
    print("in BurakAlgorithmus")

    d = addToList()
   
    if doOnce:
        print("doOnce")
        drive('backward', 10)
        doOnce = False
    if doOnce2:
        print("doOnce2")
        if compareAnglesDictionary():
            print("compareAngles")
            drive('backward', -20)
            doOnce2 = False
            allowedToCheck = True
    if allowedToCheck:
        print("allowedToCheck")
        #if RearAndSideDetection():
        if checkCollision():
            print("rearAndSideDetection")
            drive('forward', 20)
            allowedToCheck = False
            notParallel = True
    if notParallel:
        print("notParallel")
        #if checkParallelismDictionary(10):
        if checkPara():
            print("checkParallelism")
            drive('backward', 0)
            #measuredTime("Vorwärts", 20)
            notParallel = False
            position = True
    if not notParallel and position:

        print("In if Bedingung")
        
        if posLoop():
            print("PosLoop")
            drive_slow('stop', 0)
            position = False
            notParallel = True
        elif not posLoop():
            print("Nicht Posloop")
       
 
if __name__ == '__main__':
    global doOnce, doOnce2, position, notParallel, d, allowedToCheck, parked
    global a, b, isBackwards, finito, algorithmus
    spot = False
    count = 0
    doOnce = True
    doOnce2 = True
    allowedToCheck = False
    notParallel = False
    position = False
    lidar = RPLidar('/dev/ttyUSB0')
    counter = 0
    isBackwards = True
    algorithmus = True
    ###
    while algorithmus:
        lidar.clean_input()
        try:
            for obstacle_list in lidar.iter_scans(scan_type='express'):
                 BurakAlgorithmus(obstacle_list)
                 #lidar.clean_input()
                 #print("Volle Liste: ", d)
                 d = {}
                 # print("Leere Liste: ", d)
        except Exception:
            lidar.clean_input()
            print('clean')
    print("Programm verlassen")
    change_pwm_speed(150)
    drive('stop', 0)
    lidar.stop()
    lidar.disconnect()

