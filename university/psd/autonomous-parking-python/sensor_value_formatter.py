# Author:       Mirco Janisch
# Description:  Easy way for other teams to format the available sensor values into a JSON structure,
#               that can be sent to the app.

import json
import time


# todo sensor team: create formatter, add values and export
class Formatter:
    def __init__(self):
        self.data = {"type": "sensor", "time": time.time(), "values": []}

    # call in for loop for every entry
    def add_entry(self, angle, distance):
        entry = {"angle": angle, "distance": distance}
        self.data["values"].append(entry)

    # export values
    def export_json_string(self):
        return json.dumps(self.data)


# example for sensor team
if __name__ == "__main__":
    f = Formatter()
    for i in range(0, 960):
        f.add_entry(i, i)  # i is to replace with actual values

    final_json = f.export_json_string()  # networking.set_sensor_values(final_json)
