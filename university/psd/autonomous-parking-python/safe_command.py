# Author:       Mirco Janisch
# Description:  HTTP server and client for safe commands

import asyncio
import json
import logging
import threading
import time
from abc import ABC
from enum import Enum

import requests
from tornado import web

import networking
import udpserver

HTTP_PORT_RECEIVE = 8090
HTTP_PORT_SEND = 8091


# all possible safe command types
class SafeMessageTypes(Enum):
    activate_sensor = "activate_sensor"
    deactivate_sensor = "deactivate_sensor"
    search_spot = "search_spot"
    spot_found = "spot_found"
    parking_start = "parking_start"
    parking_finished = "parking_finished"
    parking_cancel = "parking_cancel"
    send_enable = "send_enable"
    send_stop = "send_stop"
    ping = "ping"


# region send
# todo software team: call this function when finished parking
def send_finished_parking():
    networking.sending = True
    networking.remote_control = True
    return send_safe_command(SafeMessageTypes.parking_finished.value)


# TODO Software Team: call this function, if parking spot found
def set_parking_spot_found():
    return send_safe_command(SafeMessageTypes.spot_found.value)


# send a safe command to the car
def send_safe_command(command_type):
    ip = networking.get_dest_ip()

    if ip is not None:
        command = {
            "type": networking.PayloadTypes.safe.value,
            "time": time.time(),
            "command": command_type
        }
        command_json = json.dumps(command)

        return requests.post(url=f"http://{ip}:{HTTP_PORT_SEND}", data=f"{command_json}\n\n").status_code
    else:
        logging.warning(f"IP is not set, can't send \"{command_type}\" command")


# endregion

# region receive
# Processes received safe commands JSON payload
def process_body(body):
    safe_type = body["type"]

    if safe_type == networking.PayloadTypes.safe.value:
        safe_status = body["command"]

        if safe_status == SafeMessageTypes.activate_sensor.value:
            logging.info("Activate sensor")
            # todo sensor team: activate the sensor, like
            # activateSensor()

        if safe_status == SafeMessageTypes.deactivate_sensor.value:
            logging.info("Deactivate sensor")
            # todo sensor team: deactivate the sensor, like
            # deactivateSensor()

        if safe_status == SafeMessageTypes.search_spot.value:
            logging.info("Search parking spot")
            # todo software team: call a function, that starts search, like
            # searchParkingSpot()

        if safe_status == SafeMessageTypes.parking_start.value:
            logging.info("Start parking")
            networking.remote_control = False
            networking.sending = False
            # todo software team: call a function, that starts the parking procedure, like
            # parkCar()

        if safe_status == SafeMessageTypes.parking_cancel.value:
            logging.info("Cancel parking")
            networking.remote_control = True
            networking.sending = True
            # todo software team: call a function, that stops the parking procedure, like
            # cancelParking()

        if safe_status == SafeMessageTypes.send_enable.value:
            logging.info("Enable sending data")
            networking.set_sending(True)

        if safe_status == SafeMessageTypes.send_stop.value:
            logging.info("Stop sending data")
            networking.set_sending(False)
            udpserver.latest_datagram = None

        if safe_status == SafeMessageTypes.ping.value:
            logging.info("Received ping")


# Main handler for receiving messages, that will hand off the body of the messages to process_body()
class MainHandler(web.RequestHandler, ABC):
    def post(self):
        # Decode message and process
        data = self.request.body.decode()
        logging.debug(f"Received safe command {data} over HTTP")
        json_data = json.loads(data)
        process_body(json_data)

        # Send empty 200 - Ok answer
        self.write('')


# Create a new web application with handler
def make_app():
    return web.Application([
        (r"/", MainHandler),
    ])


# main entry point for async execution, creates web application and makes it listen to the right port
async def main():
    app = make_app()
    app.listen(HTTP_PORT_RECEIVE)
    await asyncio.Event().wait()


# starts the async execution
def start_app():
    asyncio.run(main())


# endregion

if __name__ == "__main__":
    threading.Thread(target=start_app).start()
