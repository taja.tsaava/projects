# Author:
# Description:   

import time
import sys
from multiprocessing import Process, Queue
import queue
# TABULATE_INSTALL=lib-only pip install tabulate
#from tabulate import tabulate
import os
from enum import Enum
from typing import TypedDict, Tuple, List, Callable, Optional

# SERVO_BLASTER_DEVICE_FILE = "./test.csv"
SERVO_BLASTER_DEVICE_FILE = "/dev/servoblaster"

device = os.open(SERVO_BLASTER_DEVICE_FILE, os.O_RDWR | os.O_SYNC)


class Direction(Enum):
    forward = "forward"
    backward = "backward"


Step = Tuple[int, int]


class Command(TypedDict):
    direction: Direction
    steps: List[Step]


def cumulate_steps(in_array: List[Tuple[int, int]]):
    acc = 0
    for i in range(len(in_array)):
        acc += in_array[i][0]
        in_array[i] = (acc, in_array[i][1])

def change_gear():
	os.write(device, str.encode(f'1={150}' + '\n'))
	time.sleep(0.1)
	os.write(device, str.encode(f'1={147}' + '\n'))
	time.sleep(0.1)
	os.write(device, str.encode(f'1={150}' + '\n'))
	time.sleep(0.1)

def get_should_steer(position: int, in_array: List[Tuple[int, int]]) -> int:
    """
    - find out should-be steer position
    - delete the already passed steps from array
    :param position:
    :param in_array:
    :return:
    """
    found: Optional[int] = None
    for i in range(len(in_array)):
        if position < in_array[i][0]:
            found = i
            break
    if found is None:
        in_array.clear()
        return 0
    else:
        in_array[:] = in_array[found:]
        return in_array[0][1]*8/5


def consume(sha_queue: Queue):
    """
    - loop for auto control
    - first event must be a Command object
    - loop can be canceled with an event 'exit'
    """
    import RPi.GPIO as GPIO
    pin = 4  # pin assignment
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN)
    os.write(device, str.encode(f'1={150}' + '\n'))  # ensure auto is not moving
    global_counter = 0
    previous_counter = 0
    counter = 0
    last_error = 0
    sum_error = 0
    log = []
    iteration = 0

    def count(self):
        nonlocal counter
        nonlocal global_counter
        counter += 1
        global_counter += 1

    GPIO.add_event_detect(pin, GPIO.FALLING, count)
    KP = 7
    KI = 1.75
    KD = 0.3
    time_interval = 0.2
    target = 10
    steps: Optional[List[Step]] = None
    direction_coefficient: Optional[int] = None
    previous_output = 0
    while True:
        # todo: read sensor data
        event = None
        if steps is None:  # block until receiving steps
            command: Command = sha_queue.get(block=True)
            if command['direction'] == Direction.backward:
                # change gear
                change_gear()
                direction_coefficient = -1
            else:
                direction_coefficient = 1
            steps = command['steps']

            cumulate_steps(steps)
        else:  # only possible event other than command is 'exit'
            try:
                event = sha_queue.get(block=False)
            except queue.Empty:
                ...
        if event is not None and event == 'exit':  # operation is canceled
            break
        steer = get_should_steer(global_counter, steps)
        if steps is None or len(steps) == 0:  # there is no more step
            break
        os.write(device, str.encode(f'0={50+steer}%' + '\n'))
        counter = 0
        time.sleep(time_interval)  # own tick of auto process
        current_value = 0.7 * counter + 0.3 * previous_counter
        previous_counter = counter

        current_error = target - current_value
        sum_error = sum_error + current_error
        output = KP * current_error + sum_error * KI + (current_error - last_error) * KD
        if previous_output == 150 and output == 150:
            change_gear()
            previous_counter = 0
            counter = 0
            last_error = 0
            sum_error = 0
            log = []
            iteration = 0
            continue
        previous_output = output
        if output > 150:
            output = 150
        if output < -150:
            output = -150
        last_error = current_error
        iteration += 1
        log.append([iteration, current_value, target, output, steer, global_counter])
        # print([iteration, counter ,current_value, target, output, steer], steps)

        os.write(device, str.encode(f'1={1500 + int(output) * direction_coefficient}us' + '\n'))

    # print(tabulate(log, headers=['time', 'cur val', 'target', 'pwm - 1500us', 'steer', 'interrupts']))
    GPIO.cleanup()
    os.write(device, str.encode(f'1={150}' + '\n'))


def make_step(steps: List[Step], direction: Direction) -> (Callable[[], None], Callable[[], None]):
    shared_queue = Queue()
    auto_process = Process(target=consume, args=(shared_queue,))
    auto_process.start()
    shared_queue.put({'steps': steps, 'direction': direction}, block=False)
    return lambda : auto_process.join(), lambda : shared_queue.put('exit')

    # KP = 4
    # KI = 1.5
    # KI = 1.5
    # KD = -0.15
def forward(angle: float):
       return make_step([(4000, angle)], direction= Direction.forward)
       
def backward(angle: float):
       return make_step([(4000, angle)], direction= Direction.backward)

cancel = None
join = None
is_driving = False
def drive(command, angle):
        global cancel
        global join
        global is_driving
        if is_driving:
            cancel()
            join()
        if command == 'forward':
            join, cancel = forward(-angle)
            is_driving = True
        elif command == 'backward':
            join, cancel = backward(-angle)
            is_driving = True

def test():
        drive('forward', 20)
        time.sleep(2)
        drive('backward', 0)
def steps(steps, direction):
    for step in steps:
        join, cancel = make_step([step], direction=direction)
        join()
        time.sleep(0.1)
    os.write(device, str.encode(f'0={50}%' + '\n'))
    time.sleep(0.1)
