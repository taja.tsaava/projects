import math
import numpy as np
from matplotlib import pyplot as plt

scan_data_list = []
obstacle_points_list = []
eigenvector_list = []
corner_list = []
# [0] and [1] corner and [2] center_of_parkingspot
final_parking_corner = []
parking_spot_found = False


def detectParkingSpot(dataList):
    import time
    start_time = time.time()
    global scan_data_list
    scan_data_list = dataList
    fill_obstacle_point_list()
    fill_eigenvector_list()
    fill_corner_list()
    stop = detect_parkingspot()
    for corner in corner_list:
        print(obstacle_points_list[corner])

    print(final_parking_corner)
    print("%s seconds" % (time.time() - start_time))
    return stop


def main():
    import time
    start_time = time.time()
    global scan_data_list
    read_test_data()
    fill_obstacle_point_list()
    fill_eigenvector_list()
    fill_corner_list()
    detect_parkingspot()
    for corner in corner_list:
        print(obstacle_points_list[corner])

    print(final_parking_corner)
    print("%s seconds" % (time.time() - start_time))




# 2/3 und 6 -1.15 0.51 und -1.16 -0.49
    x, y = read_test_data_np().T
    x1, y1 = np.array(final_parking_corner).T

    plt.scatter(x, y, c='red')

    arr = []
    i = 0
    while i < len(corner_list):
        arr.append(obstacle_points_list[corner_list[i]])
        i += 1

    x2, y2 = np.array(arr).T
    plt.scatter(x2, y2, c='yellow')

    plt.scatter(x1, y1, c='blue')




    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.show()


def fill_corner_list():
    global corner_list
    corner_list = []
    print(eigenvector_list)
    i = 1
    while i < len(eigenvector_list):
        difference = abs(abs(eigenvector_list[i-1][0]) - abs(eigenvector_list[i][0])) + abs(abs(eigenvector_list[i-1][1]) - abs(eigenvector_list[i][1]))
        if difference < 0.1:
            i += 1
            continue
        angle = calculate_degree_between_vectors(eigenvector_list[i-1], eigenvector_list[i])
        if 88 < angle < 92:
            corner_list.append(i)
            i += 1
            continue
        if i < len(eigenvector_list)-5:
            for j in range(i, i+4):
                angle = calculate_degree_between_vectors(eigenvector_list[i - 1], eigenvector_list[j])
                if 88 < angle < 92:
                    corner_list.append(j)
                    i = j-1
                    break
        i += 1


def detect_parkingspot():
    global final_parking_corner
    final_parking_corner = []
    global parking_spot_found
    parking_spot_found = False
    i = 0
    while i < len(corner_list) and parking_spot_found is False:
        first_corner = obstacle_points_list[corner_list[i]]
        j = i
        corner_counter = 0
        while corner_counter < 7 and parking_spot_found is False:
            if j < len(corner_list) and obstacle_points_list[corner_list[j]] is not None:
                second_corner = obstacle_points_list[corner_list[j]]
                distance_between_corner = get_distance(first_corner, second_corner)
                if 0.99 < distance_between_corner < 1.01:
                    index_begin = corner_list[i]
                    index_end = corner_list[j]
                    idx = index_begin + 10
                    middle_of_corner = middle_of_corners(first_corner, second_corner)
                    while idx < index_end:
                        dis = get_distance(obstacle_points_list[idx], middle_of_corner)
                        if 0.491 < dis < 0.509:
                            # we got our parking spot
                            final_parking_corner.append(first_corner)
                            final_parking_corner.append(second_corner)
                            final_parking_corner.append(middle_of_corners(obstacle_points_list[idx], middle_of_corner))
                            parking_spot_found = True
                            break
                        idx += 1
            j += 1
            corner_counter += 1
        i += 1
    return parking_spot_found


def fill_eigenvector_list():
    global eigenvector_list
    eigenvector_list = []
    i = 1
    while i < len(obstacle_points_list):
        try:
            eigenvector_list.append(calculate_eigenvector(obstacle_points_list[i-1], obstacle_points_list[i]))
            i += 1
        except:
            obstacle_points_list.remove(obstacle_points_list[i-1])


def fill_obstacle_point_list():
    global obstacle_points_list
    obstacle_points_list = []
    for i in range(len(scan_data_list)):
        obstacle_points_list.append(calculate_x_y(scan_data_list[i][0], scan_data_list[i][1]))


def calculate_degree_between_vectors(point1, point2):
    # Calculating the dot product
    dot_product = point1[0] * point2[0] + point1[1] * point2[1]

    # Calculate the magnitude of vector 1 and vector 2
    mag1 = math.sqrt(point1[0] ** 2 + point1[1] ** 2)
    mag2 = math.sqrt(point2[0] ** 2 + point2[1] ** 2)

    # Calculate the cosine value
    cos_val = dot_product / (mag1 * mag2)
    if cos_val > 1:
        cos_val = 1
    if cos_val < -1:
        cos_val = -1

    # calculate the angle in degrees
    angle = math.acos(cos_val)

    return math.degrees(angle)


def read_test_data_np():
    file = open("./res/test_data.txt", 'r')
    lines = file.readlines()
    coord_list = []
    for line in lines:
        angle, dist = line.split(' ')
        dist = dist.replace("\n", "")
        coord = calculate_x_y(float(angle), float(dist))
        coord_list.append(coord)
    return np.array(coord_list)


def read_test_data():
    global scan_data_list
    file = open("./res/test_data.txt", 'r')
    lines = file.readlines()
    scan_data_list = []
    prev = None
    for line in lines:
        angle, dist = line.split(' ')
        dist = dist.replace("\n", "")
        if prev is not None and prev[0] == float(angle) and prev[1] == float(dist):
            continue
        scan_data_list.append([float(angle), float(dist)])
        prev = scan_data_list[len(scan_data_list)-1]


def calculate_eigenvector(point1, point2):
    dx = point2[0] - point1[0]
    dy = point2[1] - point1[1]
    norm = (dx ** 2 + dy ** 2) ** 0.5

    return [dx / norm, dy / norm]


def calculate_gradient(point1, point2):
    temp = point2[0] - point1[0]
    if temp == 0:
        temp += 0.1

    return (point2[1] - point1[1]) / temp


def degree_distance(x, y):
    dist = ((x ** 2) + (y ** 2)) ** 0.5
    degree = math.degrees(math.atan2(y, x))
    return degree, dist


def calculate_x_y(degrees, distance):
    x = distance * math.cos(math.radians(degrees))
    y = distance * math.sin(math.radians(degrees))
    return [round(x, 2), round(y, 2)]


def get_distance(point1, point2):
    dis = math.sqrt(((point2[0] - point1[0]) ** 2) + ((point2[1] - point1[1]) ** 2))
    return dis


def middle_of_corners(point1, point2):

    mx = (point1[0] + point2[0]) / 2
    my = (point1[1] + point2[1]) / 2

    return [mx, my]


if __name__ == "__main__":
    main()