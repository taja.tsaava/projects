"""
Path steering
by Dominique Hellbach based on
modifications from Prof. Horsch of the work
Path tracking simulation with LQR speed and steering control
author Atsushi Sakai (@Atsushi_twi)  https://github.com/AtsushiSakai/PythonRobotics
based on the publication
https://arxiv.org/pdf/1604.07446.pdf
"""
import math
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg as la
import pathlib
from bezier_path import *
from zmqRemoteApi import RemoteAPIClient

sys.path.append(str(pathlib.Path(__file__).parent))

# === Parameters =====
L = 0.4391  # Wheelbase of the vehicle [m]
max_steer = np.deg2rad(23)  # maximum steering angle[rad]
d = 0.25735


################################################################################
# gets the position of the middle of the 2 rear wheels
def getPosition(sim, leftBackWheelHandle, rightBackWheelHandle):
    backWheelLeft = sim.getObjectPosition(leftBackWheelHandle, sim.handle_world)
    backWheelRight = sim.getObjectPosition(rightBackWheelHandle, sim.handle_world)
    mx = (backWheelLeft[0] + backWheelRight[0]) / 2
    my = (backWheelLeft[1] + backWheelRight[1]) / 2
    return [mx, my]


def steer(sim, angle, steeringLeft_, steeringRight_):
    desiredSteeringAngle = angle * math.pi / 180
    steeringAngleLeft = math.atan(L / (-d + L / math.tan(desiredSteeringAngle)))
    steeringAngleRight = math.atan(L / (d + L / math.tan(desiredSteeringAngle)))
    sim.setJointTargetPosition(steeringLeft_, steeringAngleLeft)
    sim.setJointTargetPosition(steeringRight_, steeringAngleRight)


def driveReverse(sim, motorLeft_, motorRight_):
    wheelRotSpeedDx = 100 * math.pi / 180
    desiredWheelRotSpeed_ = - wheelRotSpeedDx * 2
    sim.setJointTargetVelocity(motorLeft_, desiredWheelRotSpeed_)
    sim.setJointTargetVelocity(motorRight_, desiredWheelRotSpeed_)


def driveForward(sim, motorLeft_, motorRight_):
    wheelRotSpeedDx = 100 * math.pi / 180
    desiredWheelRotSpeed_ = wheelRotSpeedDx * 2
    sim.setJointTargetVelocity(motorLeft_, desiredWheelRotSpeed_)
    sim.setJointTargetVelocity(motorRight_, desiredWheelRotSpeed_)


def neutral(sim, motorLeft_, motorRight_):
    sim.setJointTargetVelocity(motorLeft_, 0)
    sim.setJointTargetVelocity(motorRight_, 0)


def pi_2_pi(angle):
    return (angle + math.pi) % (2 * math.pi) - math.pi


def calc_nearest_index(state, cx, cy, cyaw):
    dx = [state.x - icx for icx in cx]
    dy = [state.y - icy for icy in cy]

    d = [idx ** 2 + idy ** 2 for (idx, idy) in zip(dx, dy)]
    mind = min(d)

    ind = d.index(mind)  # index of minimal distance
    mind = math.sqrt(mind)  # minimal distance

    dxl = cx[ind] - state.x
    dyl = cy[ind] - state.y

    angle = pi_2_pi(cyaw[ind] - math.atan2(dyl, dxl))
    if angle < 0:
        mind *= -1

    return ind, mind


def update(state, delta, client, sim, carObjHandle,
           leftWheelHandle, rightWheelHandle, steeringLeft_, steeringRight_, motorLeft_, motorRight_, forward):
    delta = np.clip(delta, -max_steer, max_steer)
    deltaAngle = delta * 180 / math.pi  # [deg]
    if not forward:
        deltaAngle = -deltaAngle
    steer(sim, deltaAngle, steeringLeft_, steeringRight_)
    if forward:
        driveForward(sim, motorLeft_, motorRight_)
    else:
        driveReverse(sim, motorLeft_, motorRight_)
    client.step()

    currPos = getPosition(sim, leftWheelHandle, rightWheelHandle)
    state.x = currPos[0]
    state.y = currPos[1]
    state.yaw = sim.getObjectOrientation(carObjHandle, sim.handle_world)[2]
    return state


class cc:  # curvature controller

    #################################################################################
    def __init__(self):
        pass

    class State:

        # yaw = Drehwinkel bzgl z-Achse in rad in Bezug auf das Weltkoordinatensystem

        def __init__(self, x=0.0, y=0.0, yaw=0.0, v=0.0):
            self.x = x
            self.y = y
            self.yaw = yaw
            self.v = v

    ##############################################################################

    def do_simulation(self, cx, cy, cyaw, ck, goal, sim, carObjHandle,
                      start_x, start_y, start_yaw, client,
                      leftWheelHandle, rightWheelHandle, steeringLeft_, steeringRight_, motorLeft_, motorRight_,
                      forward):
        goal_dis = 0.05

        state = self.State(x=start_x, y=start_y, yaw=start_yaw, v=0.0)

        while True:
            target_ind, e = calc_nearest_index(state, cx, cy, cyaw)
            curvature = ck[target_ind]  # aktuelle Krümmung des target-index
            if forward:
                dl = math.atan2(curvature * L, 1)
            else:
                dl = math.atan2(curvature * L, -1)
            state = update(state, dl, client, sim, carObjHandle,
                           leftWheelHandle, rightWheelHandle, steeringLeft_, steeringRight_, motorLeft_,
                           motorRight_, forward)

            # check goal
            dx = state.x - goal[0]
            dy = state.y - goal[1]
            dist = math.hypot(dx, dy)
            if dist <= goal_dis:
                print("Goal: ", dist)
                print("current pos: x: ", state.x, "  y: ", state.y)
                neutral(sim, motorLeft_, motorRight_)
                break

    def drive_to_position(self, sim, carObjHandle, client, steeringLeft, steeringRight, motorLeft,
                          motorRight, endposx, endposy, forward):

        leftWheel = '/rearLeftSpringDamper'
        rightWheel = '/rearRightSpringDamper'
        leftWheelHandle = sim.getObject(leftWheel)
        rightWheelHandle = sim.getObject(rightWheel)

        #  target course
        currPos = getPosition(sim, leftWheelHandle, rightWheelHandle)
        start_x = currPos[0]
        start_y = currPos[1]
        print("x: ", start_x, " y: ", start_y)

        yaw = sim.getObjectOrientation(carObjHandle, sim.handle_world)  # bezogen auf x-Achse WeltK in grad
        start_yaw = -yaw[2]  # [rad]
        if not forward:
            start_yaw = -start_yaw

        end_x = endposx
        end_y = endposy

        # parallel ausrichtung zur linie der corners, also orientation[2] der linie bzw eines Hindernisses  # [rad]
        end_yaw = start_yaw

        offset = 3.0  # 1/offset des kürzesten wegs von start zu ende für Konstruktion der Kontrollpunkte
        spline = BezierSpline2D(start_x, start_y, start_yaw, end_x, end_y, end_yaw, offset)
        spline.number_pts = 20  # Feinheit der Beziér-Kurve
        cx, cy, cyaw, ck, s = spline.calc_bezier_path_items()

        goal = [end_x, end_y]
        lineSize = 2
        maximumLines = 9999
        red = [1, 0, 0]
        floor = '/ResizableFloorSmall'
        floorHandle = sim.getObject(floor)
        print("lines: ", sim.drawing_lines)
        drawingContainer = sim.addDrawingObject(sim.drawing_lines, lineSize, 0, floorHandle, maximumLines, red)

        for x in range(0, len(cx) - 1, 1):
            data = [cx[x], cy[x], 0.1, cx[x + 1], cy[x + 1], 0.1]
            sim.addDrawingObjectItem(drawingContainer, data)

        self.do_simulation(cx, cy, cyaw, ck, goal, sim, carObjHandle, start_x,
                           start_y, start_yaw, client, leftWheelHandle, rightWheelHandle, steeringLeft, steeringRight,
                           motorLeft, motorRight, forward)

        return True


##########################################################################################

client = RemoteAPIClient()
sim = client.getObject('sim')

hokuyo = '/Hokuyo'
objHandle = sim.getObject(hokuyo)
scriptHandle = sim.getScript(sim.scripttype_childscript, objHandle)

car = '/nakedAckermannSteeringCar'
carObjHandle = sim.getObject(car)
carScriptHandle = sim.getScript(sim.scripttype_childscript, carObjHandle)

steeringLeft = sim.getObject('/steeringLeft')
steeringRight = sim.getObject('/steeringRight')
motorLeft = sim.getObject('/motorLeft')
motorRight = sim.getObject('/motorRight')

client.setStepping(True)
sim.startSimulation()

startTime = sim.getSimulationTime()
client.step()
end_x1 = 0.850
end_y1 = -0.735
end_x2 = 1.33574
end_y2 = 0.62864
end_x3 = 1.3461
end_y3 = 0.25764
positioner = cc()
positioner.drive_to_position(sim, carObjHandle, client, steeringLeft, steeringRight, motorLeft, motorRight, end_x1,
                             end_y1, True)
positioner2 = cc()
positioner2.drive_to_position(sim, carObjHandle, client, steeringLeft, steeringRight, motorLeft, motorRight, end_x2,
                              end_y2, False)
positioner3 = cc()
positioner3.drive_to_position(sim, carObjHandle, client, steeringLeft, steeringRight, motorLeft, motorRight, end_x3,
                              end_y3, True)
