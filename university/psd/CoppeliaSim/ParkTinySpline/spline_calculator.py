from tinyspline import *

def get_points_for_bezier (points_from_simulation):
    formatted_point_set = []
    for point in points_from_simulation:
        formatted_point_set.append(point[0])
        formatted_point_set.append(point[1])
    return formatted_point_set

def calculateBezier(sim, setOfPoints):
    #the array for bezier curve points
    allPointsOfBezier = []
    #build spline
    spline = BSpline(len(setOfPoints) // 2, 2, (len(setOfPoints) // 2) - 1, BSpline.Clamped)
    #setting the control points of the spline
    spline.control_points = setOfPoints
    #drawing the line in coppeliaSim + consolidating all points in an array
    floor = '/ResizableFloorSmall'
    floorHandle = sim.getObject(floor)
    drawingContainer = sim.addDrawingObject(sim.drawing_lines, 2, 0, floorHandle, 9999, [1, 0, 0])
    points = spline.sample(100)
    x = points[0::2]
    y = points[1::2]
    for i in range(len(x)-1):
            data = []
            data.append(x[i])
            data.append(y[i])
            data.append(0.1)
            data.append(x[i + 1])
            data.append(y[i + 1])
            data.append(0.1)
            sim.addDrawingObjectItem(drawingContainer, data)
            allPointsOfBezier.append([x[i], y[i], 0])
    return allPointsOfBezier