#Created by Liudmila Pankratyeva

#This algorithm is finding the closest point on a path to the current position of a car.
#It does this by using a for loop to iterate through all points in the path, and calculating
#the distance between each point and the car's position. The closest point is stored along with
#the distance and its index in the path aray (in our case an array of points on the spline).
#The angle between the car's current heading and the vector from the car's position to the closest
#point on the path is then calculated. The radius of the circle that the car's wheels must follow
#to stay on the path is then calculated using the closest distance and the angle error. The target
#wheel angle is then calculated as the inverse sine of the value derived from the radius. The final
#step is to return the target wheel angle, which is limited to a max steering angle of the vehicle.

import math


def pure_pursuit_alogrithm(path, car_position, wheel_base, max_wheel_angle):
    # Find the closest point on the path to the car's current position
    closest_distance = float('inf')
    closest_point = None
    index = 0
    distance = 0
    for i, point in enumerate(path):
        #the distance is the hypotenuse in a triangle formed by car heading and perpendicular to the path curve
        distance = math.sqrt((point[0] - car_position[0]) ** 2 + (point[1] - car_position[1]) ** 2)
        if distance < closest_distance:
            closest_distance, closest_point, index= distance, point,i
    # Calculate the angle between the car's current heading and the vector from the car's position to the closest point on the path
    car_heading = math.atan2(car_position[1] - closest_point[1], car_position[0] - closest_point[0])
    angle_error = car_heading - math.atan2(car_position[1] - path[-1][1], car_position[0] - path[-1][0])
   #checking if sin = 0 to avoid float division by 0
    if math.sin(angle_error) == 0:
        return 0, index
    # Calculate the radius of the circle that the car's wheels must follow to stay on the path
    radius = closest_distance / math.sin(angle_error)
    value = wheel_base / radius
    #checking if the value is within -1 and 1 for asin:
    value = max(-1, min(value, 1))
    # Calculate the target wheel angle
    target_wheel_angle = math.asin(value)
    print (target_wheel_angle*180/math.pi)
    target_wheel_angle = min(max(target_wheel_angle, -max_wheel_angle), max_wheel_angle)
    # Return the target wheel angle
    print(index)
    return target_wheel_angle, index