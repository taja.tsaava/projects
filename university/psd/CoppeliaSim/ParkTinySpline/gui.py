#Created by Liudmila Pankratyeva
#Widget for steering and controlling a car based on coppeliaSim "BubbleRob" tutorial
from park import *
from spline_calculator import *
from path_tracker import *
from manouver_enum import *
import numpy as np
from PySide6.QtWidgets import (QPushButton, QApplication,
                               QDialog, QGridLayout)


class Form(QDialog):

    def __init__(self, Client, sim, leftMotor, rightMotor, leftSteering, rightSteering, parent=None):
        super(Form, self).__init__(parent)

        # Initialize client, motors and speed
        self.Client = Client
        self.sim = sim
        self.rightSteering = rightSteering
        self.leftSteering = leftSteering
        self.leftMotor = leftMotor
        self.rightMotor = rightMotor
        self.desired_speed = 0
        self.wheel_rotation_speed = 20 * math.pi / 180
        self.wheelbase = 2.15 #meter
        self.wheel_distance = 1.57 #meter
        self.maxSteeringAngle = np.radians(25)
        self.AngleDx = np.radians(5)

        # Create button objects
        self.buttonStart = QPushButton("Start Simulation")
        self.buttonStopSim = QPushButton("Stop Simulation")
        self.buttonForward = QPushButton("↑")
        self.buttonBackward = QPushButton("↓")
        self.buttonLeft = QPushButton("←")
        self.buttonRight = QPushButton("→")
        self.buttonStop = QPushButton("Stop")
        self.buttonPark = QPushButton("PARK")


        # Create the layout
        layout = QGridLayout()
        # Order the buttons in the layout first number is the row, second is the column
        layout.addWidget(self.buttonForward, 0, 1)
        layout.addWidget(self.buttonBackward, 2, 1)
        layout.addWidget(self.buttonLeft, 2, 0)
        layout.addWidget(self.buttonRight, 2, 2)
        layout.addWidget(self.buttonStart, 4, 1)
        layout.addWidget(self.buttonStopSim, 5, 1)
        layout.addWidget(self.buttonStop, 6, 1)
        layout.addWidget(self.buttonPark, 7, 1)


        # Set dialog layout
        self.setLayout(layout)
        # Add the methods to the buttons that should get executed when you press on them
        self.buttonStart.clicked.connect(self.startSim)
        self.buttonStopSim.clicked.connect (self.stopSim)
        self.buttonStop.clicked.connect(self.stopCar)
        self.buttonForward.clicked.connect(self.forward)
        self.buttonBackward.clicked.connect(self.backward)
        self.buttonLeft.clicked.connect(self.left)
        self.buttonRight.clicked.connect(self.right)
        self.buttonPark.clicked.connect (self.park)
        #self.buttonPark.setProperty("MANOUVER", manouver)
    # This method starts the simulation


    def startSim(self):
        self.sim.startSimulation()
        self.sim.setJointTargetVelocity(self.leftMotor, 0)
        self.sim.setJointTargetVelocity(self.rightMotor, 0)
        self.sim.setJointTargetPosition(self.leftSteering, 0)
        self.sim.setJointTargetPosition(self.rightSteering, 0)
        print("Simulation started")

    def stopSim(self):
        self.sim.stopSimulation()
        print("Simulation stopped")

    def stopCar(self):
        self.sim.setJointTargetVelocity(self.leftMotor, 0)
        self.sim.setJointTargetVelocity(self.rightMotor, 0)


    #steer car by the given angle. The angles for right and left wheel are counted on base of angle,
    # wheelbase and distance between wheels
    def steerCar(self, angle):
        if angle != 0:
            steeringAngleLeft = math.atan(self.wheelbase / (-self.wheel_distance + self.wheelbase / math.tan(angle)))
            steeringAngleRight = math.atan(self.wheelbase / (self.wheel_distance + self.wheelbase / math.tan(angle)))
            self.sim.setJointTargetPosition(self.leftSteering, steeringAngleLeft)
            self.sim.setJointTargetPosition(self.rightSteering, steeringAngleRight)
        else:
            self.sim.setJointTargetPosition(self.leftSteering, angle)
            self.sim.setJointTargetPosition(self.rightSteering, angle)
        print ("Steering angle is: ", angle*180/math.pi)


    def accelerate(self, speed):
        self.sim.setJointTargetVelocity(self.leftMotor, speed)
        self.sim.setJointTargetVelocity(self.rightMotor, speed)

    def forward(self):
        self.desired_speed+=self.wheel_rotation_speed
        self.accelerate(self.desired_speed)



    def backward(self):
        self.desired_speed = 0
        self.desired_speed -= self.wheel_rotation_speed
        self.accelerate(self.desired_speed)



    def left(self):
        positionLeft = self.sim.getJointPosition(self.leftSteering)
        positionRight = self.sim.getJointPosition(self.rightSteering)
        desiredSteeringAngle = math.atan(
            (math.tan(positionLeft) * self.wheel_distance + self.wheelbase) / self.wheelbase) + math.atan(
            (math.tan(positionRight) * self.wheel_distance - self.wheelbase) / self.wheelbase)
        angle = min(max(desiredSteeringAngle+self.AngleDx, -self.maxSteeringAngle), self.maxSteeringAngle)
        print("turning left")
        print("\n\n")
        self.steerCar(angle)

    def right(self):
        positionLeft = self.sim.getJointPosition(self.leftSteering)
        positionRight = self.sim.getJointPosition(self.rightSteering)
        desiredSteeringAngle = math.atan(
            (math.tan(positionLeft) * self.wheel_distance + self.wheelbase) / self.wheelbase) + math.atan(
            (math.tan(positionRight) * self.wheel_distance - self.wheelbase) / self.wheelbase)
        angle = min(max(desiredSteeringAngle - self.AngleDx, -self.maxSteeringAngle), self.maxSteeringAngle)
        print("turning right")
        print("\n\n")
        self.steerCar(angle)


    def park(self):
        #here can be switched what manouver it will be depending on the environment
        manouver = Manouver.ORTHOGONALTWO

        #parameters of the car
        wheelbase = 2.15 #meter
        max_steering_angle = 25 * math.pi / 180 #radians
        constant_speed = 2 #m/s

        #Getting handles of car rear axle, front axle and middle, getting points on the plane
        #To be combined with coodrinate system from sensor for hardware
        middle_back_axle_handle = self.sim.getObjectHandle('/MiddleRearAxle')
        middle_front_axle_handle = self.sim.getObjectHandle('/MiddleFrontAxle')
        middle_of_the_car_handle = self.sim.getObjectHandle('/nakedAckermannSteeringCar')
        middle_parking_lot_handle_o = self.sim.getObjectHandle('/MiddleParkingLotOrthogonal')
        middle_parking_lot_handle_p = self.sim.getObjectHandle('/MiddleParkingLotParallel')
        outside_point_handle = self.sim.getObjectHandle('/RandomPointOutside')
        point_parallel_handle_o = self.sim.getObjectHandle('/PointBetweenOrthogonal')
        point_parallel_handle_p = self.sim.getObjectHandle('/PointBetweenParallel')
        point_on_cuboid_handle = self.sim.getObjectHandle('/PointOnCuboid')
        # Getting positions of car rear axle, front axle and middle, getting points on the plane
        middle_back_axle = self.sim.getObjectPosition(middle_back_axle_handle, self.sim.handle_world)
        middle_front_axle = self.sim.getObjectPosition(middle_front_axle_handle, self.sim.handle_world)
        middle_of_the_car = self.sim.getObjectPosition(middle_of_the_car_handle, self.sim.handle_world)
        middle_parking_lot_o = self.sim.getObjectPosition(middle_parking_lot_handle_o, self.sim.handle_world)
        #middle_parking_lot_p = self.sim.getObjectPosition(middle_parking_lot_handle_p, self.sim.handle_world)
        point_parallel_to_parking_lot_o = self.sim.getObjectPosition(point_parallel_handle_o, self.sim.handle_world)
        #point_parallel_to_parking_lot_p = self.sim.getObjectPosition(point_parallel_handle_p, self.sim.handle_world)
        random_outside_point = self.sim.getObjectPosition(outside_point_handle, self.sim.handle_world)
        point_on_cuboid = self.sim.getObjectPosition(point_on_cuboid_handle, self.sim.handle_world)


        print ("Start position of the car: ", middle_of_the_car)
        print ("Start orientation of the car: ", self.sim.getObjectOrientation(middle_of_the_car_handle,self.sim.handle_world))

        if manouver == Manouver.NO_MANOUVER:
            print ("Parking not possible")

        if manouver == Manouver.PARALLEL:
            print("Parallel Parking")
            #adding necessary points to build a curve
            points_from_coppelia = []
            points_from_coppelia.append(middle_back_axle)
            points_from_coppelia.append(point_parallel_to_parking_lot_p)
            points_from_coppelia.append(point_on_cuboid)
            points_from_coppelia.append(middle_parking_lot_p)
            park(self, points_from_coppelia, middle_of_the_car_handle, wheelbase, max_steering_angle, constant_speed)

        if manouver == Manouver.ORTHOGONAL:
            print ("Orthogonal parking variant 1")
            # adding necessary points to build a curve
            points_from_coppelia = []
            points_from_coppelia.append(middle_back_axle)
            points_from_coppelia.append(middle_front_axle)
            points_from_coppelia.append(random_outside_point)
            points_from_coppelia.append(middle_parking_lot_o)
            print(points_from_coppelia)
            set_of_points_bezier = []
            for coppeliaPoint in points_from_coppelia:
                set_of_points_bezier.append(coppeliaPoint[0])
                set_of_points_bezier.append(coppeliaPoint[1])
            # build spline
            park(self, points_from_coppelia, middle_of_the_car_handle, wheelbase, max_steering_angle, constant_speed)


        if manouver == Manouver.ORTHOGONALTWO:

            coppelia_points_first_spline = []
            coppelia_points_second_spline = []
            # point_outside = sim.simxCreateDummy(clientID, 0.1, 1, sim.simx_opmode_blocking)
            # if point_outside != -1:
            # sim.simxSetObjectPosition(clientID, point_outside, -1, )
            coppelia_points_first_spline.append(middle_back_axle)
            coppelia_points_first_spline.append(middle_front_axle)
            coppelia_points_first_spline.append(point_parallel_to_parking_lot_o)
            coppelia_points_first_spline.append(random_outside_point)
            first_path_passed = False
            park(self, coppelia_points_first_spline, middle_back_axle_handle, wheelbase, max_steering_angle, constant_speed)
            first_path_passed = True

            if (first_path_passed):
                car_position = update_car_postion(self, middle_back_axle_handle)
                constant_speed = -constant_speed
                coppelia_points_second_spline.append(self.sim.getObjectPosition(middle_back_axle_handle, self.sim.handle_world))
                coppelia_points_second_spline.append(point_parallel_to_parking_lot_o)
                coppelia_points_second_spline.append(middle_parking_lot_o)
                set_of_points_to_parking_lot = get_points_for_bezier(coppelia_points_second_spline)
                second_path = calculateBezier(self.sim, set_of_points_to_parking_lot)
                while(True):
                    steering_angle, index = pure_pursuit_alogrithm(second_path, car_position, wheelbase, max_steering_angle)
                    self.stopCar()
                    if index == len(second_path) - 1:
                        self.steerCar(0)
                        print("Second checkpoint reached!!!")
                        break
                    self.steerCar(-steering_angle)
                    self.accelerate(constant_speed)
                    car_position = update_car_postion(self,  middle_back_axle_handle)