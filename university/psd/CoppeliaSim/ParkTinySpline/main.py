#Created by Liudmila Pankratyeva
import sys
from gui import *
from zmqRemoteApi import RemoteAPIClient

if __name__ == '__main__':
    Client = RemoteAPIClient()
    sim = Client.getObject("sim")
    print(sim.handle_world)

    leftMotor = sim.getObject('/motorLeft')
    rightMotor = sim.getObject('/motorRight')
    leftSteering = sim.getObject('/steeringLeft')
    rightSteering = sim.getObject('/steeringRight')

    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create form
    form = Form(Client, sim, leftMotor, rightMotor, leftSteering, rightSteering)
    # Show Form
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec())
    # Stop the Simulation
    sim.stopSimulation()
    print("Simulation stopped")



