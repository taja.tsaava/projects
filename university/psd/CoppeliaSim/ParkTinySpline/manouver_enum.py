#Created by Liudmila Pankratyeva

#Class of enums, representing the available manouvers
import enum

class Manouver (enum.Enum):
    NO_MANOUVER = 0
    PARALLEL = 1
    ORTHOGONAL = 2
    ORTHOGONALTWO = 3