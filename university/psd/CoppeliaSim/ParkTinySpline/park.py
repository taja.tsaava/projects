#Created by Liudmila Pankratyeva
import time
from spline_calculator import *
from path_tracker import *

# Get the position of car from CoppeliaSim
def update_car_postion (self, handle):
    x = self.sim.getObjectPosition(handle, self.sim.handle_world)[0]
    y = self.sim.getObjectPosition(handle, self.sim.handle_world)[1]
    car_position = [x, y]
    return car_position

def park (self, points_from_coppelia, car_reference_point, wheelbase, max_steering_angle, constant_speed):
    print("Parallel Parking")
    set_of_points_bezier = get_points_for_bezier(points_from_coppelia)
    # build spline
    all_points_of_bezier = calculateBezier(self.sim, set_of_points_bezier)
    car_position = update_car_postion(self, car_reference_point)
    # steering to the point
    while (True):
        # make pauses to update
        self.stopCar()
        self.steerCar(0)
        time.sleep(0.2)
        # find out steering angle
        steering_angle, index = pure_pursuit_alogrithm(all_points_of_bezier, car_position, wheelbase,
                                                       max_steering_angle)
        # if we are at the end of the path break
        if index == len(all_points_of_bezier) - 1:
            self.steerCar(0)
            self.stopCar()
            print("checkpoint reached!!!")
            break
        # steer in the direction to the path to keep track
        self.steerCar(steering_angle)
        self.accelerate(constant_speed)
        # update position to know where we are
        car_position = update_car_postion(self, car_reference_point)
