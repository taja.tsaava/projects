# authored by Burak Palaz
# State Machine Parking Algorithm and Parking Spot Detection for CoppeliaSim
# Make sure to have the add-on "ZMQ remote API" running in
# CoppeliaSim and have the following scene loaded:
#
# ParkingSimulation_Parallel_State_Machine.ttt
#
# Do not launch simulation, but run this script
#
import collections
import datetime
from datetime import datetime
import calendar
import math
import time
import parkingspotdetector as pd
from zmqRemoteApi import RemoteAPIClient
import numpy as np

# from drive_to_optimal_position import *

print('Program started')


###############################
# drive function
def driveForward1():
    if isDrivingForward:
        global currPos
        currPos = sim.getObjectPosition(carObjHandle, sim.handle_world)
        currPos[1] -= 0.01
        sim.setObjectPosition(carObjHandle, sim.handle_world, currPos)


# processes sensor data
def modifyDataList():
    global inFloats
    dataList = []
    for i in range(0, len(inFloats), 2):
        dataPoint = [inFloats[i], inFloats[i + 1]]
        dataList.append(dataPoint)
    return dataList


###############################
# Make empty black image
image = np.zeros((600, 600, 3), np.uint8)


# unused function to draw coordinates
def drawCoordinates():
    for d in range(0, len(inFloats), 2):
        coord = polar2cart(inFloats[d], inFloats[d + 1])
        offset = sim.getObjectPosition(objHandle, sim.handle_world)
        offset[0] * 100
        offset[1] * 100
        # print('ObjPosition: ', offset)
        # print('Coordinates: ', coord)
        image[int(offset[0]) + int(coord[0] * 100), int(offset[1]) + int(coord[1] * 100)] = [255, 255, 255]


def polar2cart(a, d):
    x = d * np.cos(a)
    y = d * np.sin(a)
    coordinates = [x, y]
    return coordinates


# steering function
def steer(angle):
    desiredSteeringAngle = angle * math.pi / 180
    if angle == 0:
        steeringAngleLeft, steeringAngleRight = 0, 0
    else:
        steeringAngleLeft = math.atan(ll / (-dd + ll / math.tan(desiredSteeringAngle)))
        steeringAngleRight = math.atan(ll / (dd + ll / math.tan(desiredSteeringAngle)))
    sim.setJointTargetPosition(steeringLeft, steeringAngleLeft)
    sim.setJointTargetPosition(steeringRight, steeringAngleRight)
    # print("Left Angle: ", sim.getJointPosition(steeringLeft))
    # print("Right Angle: ", sim.getJointPosition(steeringRight))


def changeDirection():
    if lastDirection == 'forward':
        driveReverse()
    else:
        driveForward()


def driveReverse():
    global lastDirection
    lastDirection = 'reverse'
    wheelRotSpeedDx = 100 * math.pi / 180
    desiredWheelRotSpeed_ = - wheelRotSpeedDx * 2
    sim.setJointTargetVelocity(motorLeft, desiredWheelRotSpeed_)
    sim.setJointTargetVelocity(motorRight, desiredWheelRotSpeed_)
    # print("Left motor: ", sim.getJointVelocity(motorLeft))
    # print("Right motor: ", sim.getJointVelocity(motorRight))


def driveForward():
    global lastDirection
    lastDirection = 'forward'
    wheelRotSpeedDx = 100 * math.pi / 180
    desiredWheelRotSpeed_ = wheelRotSpeedDx * 2
    sim.setJointTargetVelocity(motorLeft, desiredWheelRotSpeed_)
    sim.setJointTargetVelocity(motorRight, desiredWheelRotSpeed_)


def neutral():
    sim.setJointTargetVelocity(motorLeft, 0)
    sim.setJointTargetVelocity(motorRight, 0)


def Corner180():
    cnt = 0
    var = []
    for i in range(0, len(inFloats), 2):
        if int(inFloats[i]) == 180:
            cnt += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            var.append(inFloats[i])
            var.append(inFloats[i + 1])

    if cnt == 3:
        for i in range(1, len(var) - 1, 2):
            if var[i] > var[i + 1]:
                print("Corner detected")
                sim.pauseSimulation()


# old method not used in final algorithm
# detects corners
def compareAngles():
    cunt180 = 0
    cunt90 = 0
    var180 = []
    var90 = []
    avg180 = 0
    avg90 = 0
    for i in range(0, len(inFloats), 2):
        if int(inFloats[i]) == 180:
            cunt180 += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            var180.append(inFloats[i])
            var180.append(inFloats[i + 1])
        elif int(inFloats[i]) == 90:
            cunt90 += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            var90.append(inFloats[i])
            var90.append(inFloats[i + 1])

    if cunt90 == 3 and cunt180 == 3:
        for i in range(1, len(var180), 2):
            avg180 += var180[i]
        avg180 /= 3
        for i in range(1, len(var90), 2):
            avg90 += var90[i]
        avg90 /= 3
        # print("180 avg distance: ", avg180)
        # print("90 avg distance: ", avg90)
        if abs(avg180 - avg90) < 0.2:
            # print("Distance similar with ", abs(avg180 - avg90), " tolerance")
            # sim.pauseSimulation()
            return True


# global list used for state machine
global d


# compares specific angles used to switch to another state
def compareAnglesDictionary():
    avg180 = 0
    avg90 = 0
    # Check if dictionary has values stored for either angle
    if 90 and 180 in d:
        # Only compare angles when there are 3 of each
        if len(d[90]) == 3 and len(d[180]) == 3:
            itr = len(d[180])
            for a in d[180]:
                avg180 += a
            avg180 /= itr

            itr = len(d[90])
            for a in d[90]:
                avg90 += a
            avg90 /= itr
            print("180 avg distance: ", avg180)
            print("90 avg distance: ", avg90)
            # if abs(avg180 - avg90) < 0.2:
            if abs(avg180 - avg90) < 0.2 and (avg90 and avg180 < 0.8):
                # print("Distance similar with ", abs(avg180 - avg90), " tolerance")
                # sim.pauseSimulation()
                return True


# old method not used in final algorithm
# checks if obstacle is close to the car
def RearAndSideDetection():
    relevantAngles = []
    for i in range(0, len(inFloats), 2):
        if 89 < int(inFloats[i]) < 360:
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            relevantAngles.append(inFloats[i + 1])
    for d in relevantAngles:
        if d < 0.3:
            # sim.pauseSimulation()
            return True


# old method not used in final algorithm
# checks if obstacle is close to the car using a dictionary
def RearAndSideDetectionDictionary():
    for c in d:
        if c < 0.3:
            # sim.pauseSimulation()
            return True


# checks for collisions within 0,35m from the sensor
def checkCollision():
    for i in range(0, len(inFloats), 2):
        if inFloats[i + 1] < 0.35:
            return True


# checks for collisions within 0,4m from the sensor
def checkCollision2():
    relevantAngles = []
    for i in range(0, len(inFloats), 2):
        if 89 > int(inFloats[i]) or (360 > int(inFloats[i]) > 270):
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            relevantAngles.append(inFloats[i + 1])
    for dis in relevantAngles:
        if dis < 0.4:
            # sim.pauseSimulation()
            return True
        else:
            return False


# old method not used in final algorithm
# checks for parallelism to the wall using angles with an offset from 90 degrees
def checkParallelism(offset):
    cunta = 0
    cuntb = 0
    vara = []
    varb = []
    avga = 0
    avgb = 0
    for i in range(0, len(inFloats), 2):
        if int(inFloats[i]) == 90 + offset:
            cunta += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            vara.append(inFloats[i])
            vara.append(inFloats[i + 1])
        elif int(inFloats[i]) == 90 - offset:
            cuntb += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            varb.append(inFloats[i])
            varb.append(inFloats[i + 1])

    if cunta == 3 and cuntb == 3:
        for i in range(1, len(vara), 2):
            avga += vara[i]
        avga /= 3
        for i in range(1, len(varb), 2):
            avgb += varb[i]
        avgb /= 3
        # print(90+offset, " avg distance: ", avga)
        # print(90-offset, " avg distance: ", avgb)
        if abs(avga - avgb) < 0.001:
            # print("Distance similar with ", abs(avga - avgb), " tolerance")
            # sim.pauseSimulation()
            return True


# checks for parallelism to the wall using angles with an offset from 90 degrees
def checkParallelismDictionary(offset):
    global reverse
    avga = 0
    avgb = 0
    # Check if dictionary has values stored for either angle
    if 90 - offset and 90 + offset in d:
        # Only compare angles when there are 3 of each
        # if len(d[90 - offset]) == 3 and len(d[180 + offset]) == 3:
        itr = len(d[90 - offset])
        for a in d[90 - offset]:
            avga += a
        avga /= itr

        itr = len(d[90 + offset])
        for a in d[90 + offset]:
            avgb += a
        avgb /= itr

        if checkCollision2():
            reverse = True

        if abs(avga - avgb) < 0.001:
            # print("Distance similar with ", abs(avga - avgb), " tolerance")
            # sim.pauseSimulation()
            return True


# old method not used in algorithm
# returns the required distance values to check the cars position inside the parking spot
def positioning():
    c90 = 0
    c180 = 0
    list90 = []
    list180 = []
    avgOf90 = 0
    avgOf180 = 0
    for i in range(0, len(inFloats), 2):
        if int(inFloats[i]) == 0:
            c90 += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            list90.append(inFloats[i])
            list90.append(inFloats[i + 1])
        elif int(inFloats[i]) == 180:
            c180 += 1
            # print("Grad: ", inFloats[x], "\tDistance: ", inFloats[x+1])
            list180.append(inFloats[i])
            list180.append(inFloats[i + 1])

    if c90 == 3 and c180 == 3:
        for i in range(1, len(list90), 2):
            avgOf90 += list90[i]
        avgOf90 /= 3
        for i in range(1, len(list180), 2):
            avgOf180 += list180[i]
        avgOf180 /= 3
        # print("0 degree avg distance: ", avgOf90)
        # print("180 degree avg distance: ", avgOf180)
        return avgOf90, avgOf180
        """
        print("Distance similar with ", abs(avgOf90 - avgOf180), " tolerance")
        sim.pauseSimulation()
        return True
        """


# returns the required distance values to check the cars position inside the parking spot
def positioningDictionary():
    avg0 = 0
    avg180 = 0
    # Check if dictionary has values stored for either angle
    if all(key in d for key in (0, 180)):
        # if 0 and 180 in d:
        # Only compare angles when there are 3 of each
        if len(d[0]) == 3 and len(d[180]) == 3:
            itr = len(d[0])
            for a in d[0]:
                avg0 += a
            avg0 /= itr

            itr = len(d[180])
            for a in d[180]:
                avg180 += a
            avg180 /= itr
            # print("0 degree avg distance: ", avg0)
            # print("180 degree avg distance: ", avg180)
            return avg0, avg180
    else:
        print('0 und 180 nicht in der Liste')
        return None


# checks whether the positioning of the car within the parking spot is parallel to both obstacles
def posLoop(param1, param2):
    if param1 < param2:
        driveReverse()
    elif param2 < param1:
        driveForward()

    if abs(param1 - param2) < 0.01:
        # print("Tolerance from perfect parallel distance: ", abs(param1 - param2))
        print("Positioned!")
        # sim.pauseSimulation()
        return True
    else:
        # print("Not positioned yet...")
        return False


###############################

dd = 14.9  # 2*d=distance between left and right wheels
ll = 57  # l=distance between front and rear wheels

client = RemoteAPIClient()
sim = client.getObject('sim')

hokuyo = '/Hokuyo'
objHandle = sim.getObject(hokuyo)
scriptHandle = sim.getScript(sim.scripttype_childscript, objHandle)

car = '/nakedAckermannSteeringCar'
carObjHandle = sim.getObject(car)
carScriptHandle = sim.getScript(sim.scripttype_childscript, carObjHandle)

steeringLeft = sim.getObject('/steeringLeft')
steeringRight = sim.getObject('/steeringRight')
motorLeft = sim.getObject('/motorLeft')
motorRight = sim.getObject('/motorRight')

client.setStepping(True)
sim.startSimulation()

startTime = sim.getSimulationTime()
client.step()

###
currPos = sim.getObjectPosition(carObjHandle,
                                sim.handle_world)  # Startposition des Fahrzeuges relativ zum Weltkoordinatensystem
print(currPos)
deltat = 1  # Zurueckgelegte Strecke pro Zeiteinheit t
isDrivingForward = True  # Vorwaerts oder Rueckwaerts Fahrtrichtung
accel = 0.5
###
spot = False
count = 0
doOnce = True
doOnce2 = True
allowedToCheck = False
notParallel = False
position = False
###
start_time = time.time()
measureTime = False
countState = 0


# measures time between two timestamps
def measuredTime(dir, steer):
    global countState
    end_time = time.time()
    total_time = end_time - start_time
    countState += 1
    print("Das Programm hat ", total_time, " Sekunden gedauert bis zum Zustand ", countState, " mit der Richtung ", dir,
          " und der Lenkrichtung ", steer)


# processes sensor data from coppeliasim for parking algorithm
def addDict():
    sample_dict = {}
    for x in range(0, len(inFloats), 2):
        if int(inFloats[x]) not in sample_dict:
            sample_dict.setdefault(inFloats[x], []).append(inFloats[x + 1])
            # sample_dict.setdefault(int(inFloats[x]), []).append(inFloats[x + 1])
        else:
            sample_dict.setdefault(inFloats[x], []).append(inFloats[x + 1])
    return sample_dict


# checks for a parking spot
def checkParkingSpot():
    global foundSpot
    global distance
    global firstTime
    global secondTime
    global timeCount
    avgDistance = 0
    prevDist = 50000
    if 90 in d:
        itr = len(d[90])
        for a in d[90]:
            avgDistance += a
        avgDistance /= itr
    else:
        print('90 nicht in der Liste')
    if avgDistance > 0.4:
        print("Enough Distance")
        firstTime = time.time()
        print(firstTime)
        timeCount += 1
    if avgDistance < prevDist and timeCount == 1:
        secondTime = time.time()
        print(secondTime)
        timeCount += 1
    if timeCount == 2:
        distance = distance_traveled(wheelRotSpeedDx, firstTime, secondTime)
        print("Distance: ", distance)
        timeCount = 0
        if distance > 0.80:
            foundSpot = True


# measures distance traveled given two timestamps
def distance_traveled(velocity, time1, time2):
    # Convert the input time measurements to floats
    time1 = float(time1)
    time2 = float(time2)
    # Calculate the elapsed time by subtracting the two times
    elapsed_time = time2 - time1
    # Calculate the distance traveled using the formula:
    # distance = velocity * elapsed time
    distance = velocity * elapsed_time
    return distance


# measures distance traveled given two timestamps
def disTime(velocity, time1, time2):
    # Convert the input time measurements to floats
    time1 = float(time1)
    time2 = float(time2)
    # Calculate the elapsed time by subtracting the two times
    elapsed_time = time2 - time1
    # Calculate the distance traveled using the formula:
    # distance = velocity * elapsed time
    distance = velocity * elapsed_time
    return distance


firstDetection = False


# parking spot detection algorithm
def parkingSpotDetection():
    global foundSpot
    global distance
    global firstTime
    global secondTime
    global timeCount
    global firstDetection
    global current_GMT
    global secondDetection
    avgDistance = 0
    prevDist = 0
    secondDetection = False
    scanning = True
    # global times otherwise the calculation will not work due to scopes
    global time1
    global time2
    global time3
    global time4
    global findStartPoint
    if 90 in d:
        itr = len(d[90])
        for a in d[90]:
            avgDistance += a
        avgDistance /= itr
    """Fehlerbehandlung wenn NULL"""
    # else:
    # print('90 nicht in der Liste')

    ###
    if avgDistance - prevDist >= 0.4 and not firstDetection:
        firstDetection = True
        time1 = time.perf_counter()
        print("First time: ", time1)

    if abs(avgDistance - prevDist) < 0.4 and firstDetection:
        time2 = time.perf_counter()
        print("Second time: ", time2)
        elapsed = time2 - time1
        print("Elapsed: ", elapsed)
        print("Passed time: ", time2 - time1)
        if time2 - time1 > 10:
            # foundSpot = True
            time3 = time.perf_counter()
            secondDetection = True
        else:
            firstDetection = False
    ###
    """
    if avgDistance > 0.4 and not firstDetection:
        firstDetection = True
        time1 = time.perf_counter()
        print("First time: ", time1)

    if avgDistance < 0.4 and firstDetection:
        time2 = time.perf_counter()
        print("Second time: ", time2)
        elapsed = time2 - time1
        print("Elapsed: ", elapsed)
        print("Passed time: ", time2 - time1)
        if time2 - time1 > 10:
            #foundSpot = True
            time3 = time.perf_counter()
            secondDetection = True
        else:
            firstDetection = False
"""
    if firstDetection and secondDetection:
        findStartPoint = True
        # time4 = time.perf_counter()
        # if time4 - time3 > 3:
        # foundSpot = True
    """
    if avgDistance > 0.4:
        print("Enough Distance")
        firstTime = time.time()
        print(firstTime)
        timeCount += 1
    if avgDistance < prevDist and timeCount == 1:
        secondTime = time.time()
        print(secondTime)
        timeCount += 1
    if timeCount == 2:
        distance = distance_traveled(wheelRotSpeedDx, firstTime, secondTime)
        print("Distance: ", distance)
        timeCount = 0
        if distance > 0.80:
            foundSpot = True
    """


# parking spot detection
def perfectSpot():
    global firstDetection
    global secondDetection
    global foundSpot
    global time4
    global time3
    if firstDetection and secondDetection:
        time4 = time.perf_counter()
        if time4 - time3 > 3:
            foundSpot = True


####
"""
positioner = cc()
positioner.drive_to_position(position, sim, carObjHandle, client, steeringLeft, steeringRight, motorLeft, motorRight)
"""
foundSpot = False
current_GMT = time.gmtime()
wheelRotSpeedDx = (100 * math.pi / 180) * 2
firstTime = 0
secondTime = 0
timeCount = 0
distance = 0
###
reverse = False
###
findStartPoint = False


# checks whether or not car needs to keep driving reverse
def checkReverse():
    global reverse

    # if not checkCollision2():
    # reverse = False
    if checkCollision():
        reverse = False


# while loop for simulation with parking spot detection and parking algorithm
while True:
    global lastDirection
    count += 1
    # print("------------------------------------------------------------------------------")
    # get sensor data from coppeliaSim
    inInts, inFloats, inStrings, inBuffer = sim.callScriptFunction('getData', scriptHandle)
    # process data for the parking spot algorithm
    d = addDict()

    # Comment out this block and set boolean foundSpot to True to directly start the parking algorithm instead of
    # finding a parking spot first
    if not foundSpot and not findStartPoint:
        driveForward()
        parkingSpotDetection()
    if not foundSpot and findStartPoint:
        perfectSpot()
    # Comment out this block and set boolean foundSpot to True to directly start the parking algorithm instead of
    # finding a parking spot first

    # parking spot algorithm
    if doOnce and foundSpot:
        steer(20)
        driveReverse()
        doOnce = False
    if doOnce2:
        if compareAnglesDictionary():  # compareAngles():
            measuredTime("Rückwärts", 20)
            steer(-20)
            # driveReverse()
            doOnce2 = False
            allowedToCheck = True
    if allowedToCheck:
        # if RearAndSideDetection():
        if checkCollision():
            measuredTime("Rückwärts", -20)
            driveForward()
            steer(20)
            driveForward()
            allowedToCheck = False
            notParallel = True
    if notParallel and not reverse:
        driveForward()
        steer(20)
        if checkParallelismDictionary(10):
            measuredTime("Vorwärts", 20)
            notParallel = False
            position = True
        ###
    if notParallel and reverse:
        steer(0)
        driveReverse()
        checkReverse()
        ###
    if not notParallel and position:
        steer(0)
        params = positioningDictionary()
        if params is not None:
            if posLoop(params[0], params[1]):
                measuredTime("Rückwärts", 0)
                neutral()
                position = False
                measureTime = True
                # sim.pauseSimulation()
    client.step()
sim.stopSimulation()

print('Program ended')
