import os
import time

SERVO_BLASTER_DEVICE_FILE = "/dev/servoblaster"

init_pwm = 150

max_pwm_sp = 200
min_pwm_sp = 100

min_angle = -26
max_angle =  26
min_pwm_ag = 110
max_pwm_ag = 295
pwm_angle_compensation = 6

break_pwm_dif = 11
min_pwm_dif = 7

last_dir = "forward"
is_driving = False
 
def main():
    drive_slow("forward",26)


def test_driveing(time):
    count = time *10
    while(count > 0):
        drive_slow("forward",0)
        count = count -1
    
    drive_slow("stop",0)


def drive_glide():
    change_pwm_speed(init_pwm + min_pwm_dif)
    time.sleep(0.1)
    change_pwm_speed(init_pwm )
    time.sleep(0.2)


def drive_manuel(speed,angle):
    change_pwm_speed((speed/255)*200-100)
    change_pwm_ankle(map_angle_on_pwm(angle))


def drive_slow(direction, angle):
    if not direction == last_dir:
        change_dir()
    if dir == "forward":
        if not is_driving:
            start_drive(direction)
        #change_pwm_speed(init_pwm + min_pwm_dif)
        drive_glide() # just testing
    
    if direction == "backward":
        if not is_driving:
            start_drive(direction)
        change_pwm_speed(init_pwm + min_pwm_dif)

    if direction == "stop":
        if is_driving:
            change_pwm_speed(min_pwm_sp)
            is_driving = False;
        else:
            change_pwm_speed(init_pwm)

    change_pwm_ankle(map_angle_on_pwm(angle))


def we_did_not_start():
    break_pwm_dif = break_pwm_dif + 1


def we_stop_but_shouldnt():
    min_pwm_dif = min_pwm_dif + 1


def change_pwm_speed(pwm):
    message: str = "1="+pwm+"\n" 
    print(message)
    device = open(SERVO_BLASTER_DEVICE_FILE, 'w')
    device.write(message)
    device.close()


def change_pwm_ankle(pwm):
    message: str = "0="+pwm+"\n" 
    
    device = open(SERVO_BLASTER_DEVICE_FILE, 'w')
    device.write(message)
    device.close()


def start_drive(direction):
    if direction == "backward":
        mult = -1
    else: 
        mult = 1

    change_pwm_speed(init_pwm + mult * break_pwm_dif)
    time.sleep(0.5)
    change_pwm_speed(init_pwm + mult * min_pwm_dif)


def change_dir():
    if last_dir == "backward":
        change_pwm_speed(init_pwm-1);
        change_pwm_speed(init_pwm)
        last_dir = "forward"
        is_driving = False
    else:
        change_pwm_speed(init_pwm+1);
        change_pwm_speed(init_pwm)
        last_dir = "backward"
        is_driving = False


def map_angleper_on_pwm(side, per):
    if side == "right":
        return ((init_pwm + max_pwm_ag)* per /100)
    else:
        return ((init_pwm + min_pwm_ag)* per /100)


def map_angle_on_pwm(angle):
    angle_range = max_angle - min_angle
    if angle > 0:
        angle_pwm_dif = max_pwm_ag - 150 + pwm_angle_compensation
        return (angle/angle_range) * angle_pwm_dif + 150
    else: 
        if angle < 0:
            angle_pwm_dif = 150 + pwm_angle_compensation - min_pwm_ag
            return (angle/angle_range) * angle_pwm_dif + 150
        else:
            return 150 + pwm_angle_compensation



def border_pwm_angle():
    i = 0
    while():
        print(init_pwm + i)
        change_pwm_ankle(init_pwm + i)
        i = i + 1
        time.sleep(1)




