import math
import numpy as np

# es sollten immer nur makante Punkte übergeben wernden, welche ersten nicht
# all zuviel werden und warscheinlich im nächsten scann wieder ausgewählt
# werden um die Genauigkeit zu verbesern. Sommit sollten z.B. die Eckpunkte
# verwendeet werden, da diese relativ sicher wieder auftauchen. Da mit allen
# Puntken gerechnet wir sollte über eine Filterung außerhalb nachgedacht werden

def new_pint_map(pointMap):#MAIN usage funcion gets a list op Points to be "watched" and calculates distance moved in a line or in a partial circle


    if len(oldPoints) >= 0:
        for p in pointMap:
            oldPoints.append(point(p.x, p.y))

        return
        
    if len(oldPoints) < len(pointMap):
        for op in oldPoints:
            find_best_fit(op, pointMap)

    if len(pointMap) < len(oldPoints):
        for np in pointMap:
            find_best_fit(np, oldPoints)
        
        for v in vects:
            v = (v[0]*-1, v[1]*-1)
    
    if moved_strait(): #we should have moved in a strait line so distance is easy
        dist = 0;
        for v in vects:
            dist = dist + v[1]
        dist = dist/len(vects)
        return dist

    #we didnt move straind and now distance is not starit forward (pun intended)
    
    get_lines(pointMap)

    intersectsAll = []
    for l in lines:
        for ol in lines:
            if(l != ol):
                intersectsAll.append( line_intersec_point(l[0],l[1], ol[0],ol[1]) )

    filter_intercests(intersectsAll)
    
    # calc the good avg
    avgX = 0
    avgY = 0
    for i in intersectsAll:
        avgX = avgX + i.x
        avgY = avgY + i.y

    avgPoint = point(avgX / len(intersectsAll), avgY / len(intersectsAll))

    angle = get_avg_angle(avgPoint)# angle in radient not degre

    circleRadius = avgPoint.dist(point(0,0))

    drivendistance = (circleRadius * 2 * 3.14159265359) * (angle/(2 * 3.14159265359))

    PointPais = []
    oldPoints = []
    vects = []
    lines = []
    for p in pointMap:
        oldPoints.append(p)

    return drivendistance




def get_avg_angle(center): #angle in radient not degree
    angleAvg = 0;
    for pp in PointPais:
        v1 = (center. x - pp[0].x, center.y - pp[0].y)
        v2 = (center. x - pp[1].x, center.y - pp[1].y)

        angleAvg = angleAvg + angle_between(v1,v2)

    return angleAvg / len(PointPais)


def unit_vector(vector):
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def filter_intercests(intersects):
    
    avgX = 0
    avgY = 0

    for i in intersects:
        avgX = avgX + i.x
        avgY = avgY + i.y

    avgPoint = point(avgX / len(intersects), avgY / len(intersects)) #still a bad avg
    #clean up the avg
    avgDist = 0;
    for i in intersects:
        avgDist = avgDist + avgPoint.dist(i)
    avgDist = avgDist/ len(intersects)

    for i in intersects:
        if avgPoint.dist(i) *1.5 > avgDist:
            i.x = avgPoint.x
            i.y = avgPoint.y


def moved_strait():
    leftRightDif = 0
    for v in vects:
        leftRightDif = leftRightDif + (v[0]/v[1])

    leftRightDif = leftRightDif / len(vects)

    if leftRightDif > 0.125:
        return True
    
    return False

def get_lines(pointList):
    lines = []
    for i in [0..len(pointList)]:
        #append tuple of (A , B) for line AB
        lines.append( point(pointList[i].x + 0.5* vects[i][0],  pointList[i].y + 0.5* vects[i][1]), point(pointList[i].x + 0.5* vects[i][0] - vects[i][1], pointList[i].y + 0.5* vects[i][1] + vects[i][0]))


      
def find_best_fit(p, points): #get one point and list of points an findes the best match for the point and appends to the vec list
    bestYet = 10000
    bestVec = (0,0)
    for op in points:
        dist =  p.dist(op)
        if dist < bestYet:
            bestYet = dist
            bestVec = (op.x - p.x, op.y - p.y)
    
    vects.append(bestVec)
    PointPais.append(p, point(p.x+bestVec[0], p.y+bestVec[1]) )#for later use if we moved in a curve
    

def line_intersec_point(A, B, C, D):
	# Line AB represented as a1x + b1y = c1
	a1 = B.y - A.y
	b1 = A.x - B.x
	c1 = a1*(A.x) + b1*(A.y)

	# Line CD represented as a2x + b2y = c2
	a2 = D.y - C.y
	b2 = C.x - D.x
	c2 = a2*(C.x) + b2*(C.y)

	determinant = a1*b2 - a2*b1

	if (determinant == 0):
		# The lines are parallel.
		return point(0, 0)
	else:
		x = (b2*c1 - b1*c2)/determinant
		y = (a1*c2 - a2*c1)/determinant
		return point(x, y)



#VARS FOR GLOBAL USE

PointPais = []
oldPoints = []
vects = []
lines = []



class point:
    def __init__(self,x,y) -> None:
        self.x = x
        self.y = y

    def dist(self,O) -> float:
        return math.sqrt( pow((self.x - O.x), 2) + pow(( self.y - O.y), 2))

