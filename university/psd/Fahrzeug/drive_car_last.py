import logging
import os
import time

SERVO_BLASTER_DEVICE_FILE = "/dev/servoblaster"

try:
    device = os.open(SERVO_BLASTER_DEVICE_FILE, os.O_RDWR | os.O_SYNC)
except:
    logging.warning(f"File not found ({SERVO_BLASTER_DEVICE_FILE})")


def drive_manuel(speed, angle):
    change_pwm_speed(speed)
    change_pwm_angle(angle)


def change_pwm_speed(pwm):
    message: str = f"1={str(pwm)}%"
    logging.debug(f"Message for acceleration is {message}")
    write_to_device_file(message)


def change_pwm_angle(pwm):
    message: str = f"0={str(pwm)}%"
    logging.debug(f"Message for steering is {message}")
    write_to_device_file(message)


def write_to_device_file(message):
    try:
        os.write(device, str.encode(message + '\n'))
        logging.debug(f"Wrote ({message}) to ({SERVO_BLASTER_DEVICE_FILE})")
    except:
        logging.warning(f"Can't write to {SERVO_BLASTER_DEVICE_FILE}")

def drive_demo():
    # steer left
    drive_manuel(50, 70)

    # steer right
    time.sleep(2)
    drive_manuel(50, 30)

    # forward
    time.sleep(2)
    drive_manuel(55, 50)

    # backward
    time.sleep(1)
    drive_manuel(45, 50)
    time.sleep(1)
    drive_manuel(45, 50)

    # back to neutral
    time.sleep(2)
    drive_manuel(50, 50)

if __name__ == '__main__':
    drive_demo()
