## Automatic Wi-Fi Connection

Raspberry Pi should be configured so that it connects automatically to a previously determined
Wi-Fi.

It can be done either

- during flushing the operating system on Raspberry Pi with the help of
  [rpi-imager](https://github.com/raspberrypi/rpi-imager)

or

- on a running system by configuring the network settings. Official
  documentation can be found here.
  [Configuring Networking on Raspberry Pi](https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-networking)

Default Wi-Fi Connection to which Pi's in this project will connect:

- SSID: `fahrzeug`
- Password: `fahrzeug`

## Installing and Configuring `Servoblaster`

- To drive servo motors, a program named `Servoblaster` will be used, which is a part of [an open source repository.](https://github.com/richardghirst/PiBits/tree/master/ServoBlaster/user)

### Installing

- After cloning the repo [richardghirst/PiBits](https://github.com/richardghirst/PiBits), change the working directory to `ServoBlaster/user/`

```shell
git clone https://github.com/richardghirst/PiBits
cd PiBits/ServoBlaster/user
```

- Installation will be done by `Makefile`.

```shell
sudo make install
```

- This step will compile the source code and copy the executable with the name `servod` to the `/usr/local/sbin`.
  It will be reachable from command line.
- Additionally, it will configure that `servod` will start at start up of system with default configuration. `init-script` will be copied to `etc/init.d/servoblaster`.

### Configuring
`audio` is disrupting the generation of signal through pwm hardware. It should be disabled. Otherwise, `servod` should be use with `--pcm` option to use PCM.

#### To disable audio in Rasbperry Pi:

> Change the configuration in `/boot/config.txt`
> ```
> dtparam=audio=off
> ```
> it will be effective after reboot

To make `servod` automatically start at system boot with our own default configuration we need to edit start script.

- Start script will reside in `etc/init.d/servoblaster`
- Change the Line 16

from

```shell
OPTS="--idle-timeout=2000" # Line 16
```

to

```shell
OPTS="--p1pins=12,33" # Line 16,  add ' --pcm' to use PCM
```

It will **NOT** take effect immediately, instead take effect after reboot or restarting the `servod`.

To configure the `servod` without restarting the system:
```shell
sudo killall servod
sudo servod --p1pins=12,33 --pcm
```
- Servo motor for steering will be connected to `P1 Pinout Nr:12`, which will be Servo Number `0` in our configuration.
- Speed Control will be connected to `P1 Pinout Nr:33`, which will be Servo Number `1` in our configuration.

![](images/pi4j-rpi-1a-pinout-small.png)
![](images/j8header-photo.png)

### How to use `Servoblaster`

- For detailed description refer to explanations in [repo](https://github.com/richardghirst/PiBits/tree/master/ServoBlaster)
- Basically:
  - `servod` service will listen a named pipe in the path `/dev/servoblaster`
  - writing in this pipe will change the pulse width.

#### Examples

```shell
echo 0=50% > /dev/servoblaster # will get steering in neutral position.
```

```shell
echo 1=150 > /dev/servoblaster # auto will stop
```
