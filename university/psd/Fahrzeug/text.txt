  time    cur val    target    pwm - 1500us    steer
------  ---------  --------  --------------  -------
     0          0         0
     1          0         0            0          50
     2          0        10           90.5        50
     3          5        10           59.75       50
     4          5        10           70          50
     5          9        10           42.55       50
     6          6        10           72.65       50
     7          9        10           51.6        50
     8          7        10           72.35       50
     9         10        10           49.85       50
    10          7        10           77.9        50
    11         10        10           55.1        50
    12          8        10           74.1        50
    13         10        10           58.9        50
    14          8        10           77.6        50
    15         11        10           53.35       50
    16          8        10           79.65       50
    17         10        10           64.15       50
    18         10        10           64.75       50
    19         10        10           64.75       50
    20         10        10           64.75       50
    21         11        10           55.7        50
    22         10        10           63.3        50
    23         10        10           63          50
    24         10        10           63          50
    25         11        10           53.95       50
    26          9        10           70.6        50
    27         11        10           53.65       50
    28         12        10           43.45       50
    29          9        10           67.4        50
    30         25        10          -76.55       50
    31         13        10           10.6        50
    32         10        10           28.9        50
    33          6        10           64.2        50
    34          6        10           70          50
    35          6        10           77          50
    36          9         0          -33.65       50
    37          6         0          -18.35       50
    38          4         0          -11.65       50
    39          1         0            7.9        50
    40          0         0           14.3        50
