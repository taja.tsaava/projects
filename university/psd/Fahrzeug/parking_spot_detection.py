import math
# import collectdata as sensor
import numpy as np
from rplidar import RPLidar
#from matplotlib import pyplot as plt
#from collections import deque
from auto_process import drive, steps, Direction
from reeds_shepp_path_planning import reeds_shepp_path_planning

# import optimized_code

# test data paths
scan_data_path = "res/scan_data.txt"
test_data_path = "res/test_data.txt"
plot_data_path = "res/plotData.txt"

# degrees = 360 / 135
# radians = np.deg2rad(degrees)
# cos_rad = np.cos(radians)
# sin_rad = np.sin(radians)

degree = 270
radian = np.deg2rad(degree)
cos_rad = np.cos(radian)
sin_rad = np.sin(radian)

d = []

scan_data_list = []
obstacle_points_list = []
eigenvector_list = []
corner_list = []
# [0] and [1] corner and [2] center_of_parkingspot
final_parking_corner = []
parking_spot_found = False
vehicle_position = [0.0, 0.0]
old_column_pos = 0.0

norm = np.linalg.norm

# needed for bezier-curve
bezier_point = None

# Needed for the RRTStarReedsShepp Algorithm from AtsushiSakai
goal = []
new_obstacle_list = []


def main():
    global obstacle_points_list
    global scan_data_list
    is_detected = False
    lidar = RPLidar('/dev/ttyUSB0')
    drive("forward", 0)
    while True:
        scan_data_list = []
        lidar.clean_input()
        try:
            for scan in lidar.iter_scans(scan_type="express"):
                scan_data_list = scan
                fill_obstacle_point_list()
                fill_eigenvector_list()
                fill_corner_list()
                is_detected = detect_parkingspot()
                if is_detected:
                    drive('stop', 0)
                    L = 34.2
                    curvature = math.sin(np.radians(20)) / L
                    step_size = 0.05
                    xs, ys, yaws, modes, lengths = reeds_shepp_path_planning(0, 0,
                                                                             180, goal[0],
                                                                             goal[1], goal[2],
                                                                             curvature,
                                                                             step_size)
                    step_list = []
                    for i in range(len(lengths)):
                        angle = 20 if modes[i] == 'R' else -20 if modes[i] == 'L' else 0
                        step_list.append((abs(lengths[i]), angle,
                                          Direction.forward if np.sign(lengths[i]) == 1 else Direction.backward))

                    if not xs:
                        assert False, "No path"
                    else:
                        for step in step_list:
                            steps([(step[0], step[1])], direction=step[2])
        except Exception:
            print("Manually Clearing Needed")
            lidar.clean_input()


def get_point_for_optimal_parkprocess():
    global degree
    global radian
    global cos_rad
    global sin_rad
    global d
    hoeher = [0.0, 0.0]
    niedriger = [0.0, 0.0]
    hoeher = final_parking_corner[0]
    niedriger = final_parking_corner[1]

    if hoeher[1] < niedriger[1]:
        tmp = hoeher
        hoeher = niedriger
        niedriger = tmp

    mid = middle_of_corners(hoeher, niedriger)
    mid_of_mid = middle_of_corners(hoeher, mid)

    degree = 180
    radian = np.deg2rad(degree)
    cos_rad = np.cos(radian)
    sin_rad = np.sin(radian)

    first_rot = rotate(mid_of_mid, hoeher)
    d = [first_rot]
    degree = 270
    radian = np.deg2rad(degree)
    cos_rad = np.cos(radian)
    sin_rad = np.sin(radian)

    second_rot = rotate(hoeher, first_rot)
    print(second_rot)
    point = []
    point.append(second_rot[0] * 100)
    point.append(second_rot[1] * 100)

    return point


"""""
    p = [get_parkingpoint(-5, 3, -4,
                         3)]
    list = [[-5, 3],  [-4, 3], p]
    print(list)
    plot_data(list, "blue")
    plt.xlim(-10, 10)
    plt.ylim(-10, 10)
    plt.show()
"""""


def find_parking_spot(scan):
    global scan_data_list
    global obstacle_points_list
    global final_parking_corner
    final_parking_corner = []
    for data in scan:
        scan_data_list.append([float(data[1]), float(data[2])])
    fill_obstacle_point_list()
    convert_to_obstacle_list_for_rttalgorithm(obstacle_points_list)
    plot_data(obstacle_points_list, 'red')
    fill_eigenvector_list()
    fill_corner_list()

    corners = []
    for index in range(len(corner_list)):
        corners.append(obstacle_points_list[corner_list[index]])

    plot_data(corners, 'blue')
    detect_parkingspot()
    plot_data(final_parking_corner, 'black')

    print(goal)
    plt.show()
    return final_parking_corner


def find_parking_spot_advanced(scan):
    global scan_data_list
    global obstacle_points_list
    for data in scan:
        scan_data_list.append([float(data[1]), float(data[2])])
    fill_obstacle_point_list()
    fill_eigenvector_list()
    fill_corner_list()
    straighten_lines()
    detect_parkingspot()
    return final_parking_corner


def plot_data(data_points, color):
    for i in range(len(data_points)):
        data_points[i] = [data_points[i][0], data_points[i][1]]
    x, y = np.array(data_points).T
    plt.scatter(x, y, c=color)


def fill_corner_list():
    global corner_list
    print(eigenvector_list)
    corner_list = []
    i = 1
    while i < len(eigenvector_list):
        difference = abs(abs(eigenvector_list[i - 1][0]) - abs(eigenvector_list[i][0])) + abs(
            abs(eigenvector_list[i - 1][1]) - abs(eigenvector_list[i][1]))
        if difference < 0.1:
            i += 1
            continue
        angle = calculate_degree_between_vectors(eigenvector_list[i - 1], eigenvector_list[i])
        if 70 < angle < 110:
            corner_list.append(i)
            i += 1
            continue
        if i < len(eigenvector_list) - 5:
            for j in range(i, i + 4):
                angle = calculate_degree_between_vectors(eigenvector_list[i - 1], eigenvector_list[j])
                if 70 < angle < 110:
                    corner_list.append(j)
                    i = j - 1
                    break
        i += 1


def straighten_lines():
    # artifical line to detect hits on this line [start_point, end_point]
    line = []
    line_rotations = 135
    line_rotation_offset = 360 / line_rotations
    line_inaccuracy_offset = 0.010
    line_length = .4
    artificial_line = []
    straightened_list = []
    straightened_list.append(obstacle_points_list[0])
    obstacle_points_list.remove(obstacle_points_list[0])
    debugCounter = 0
    blubCounter = 0
    while len(obstacle_points_list) > 0:
        artificial_line = [straightened_list[len(straightened_list) - 1],
                           [straightened_list[len(straightened_list) - 1][0],
                            straightened_list[len(straightened_list) - 1][1] + line_length]]

        best_line_params = get_best_line_params(artificial_line)
        # append the current point to straightened_list if it does not belong to any line
        if len(best_line_params[2]) == 0:
            straightened_list.append(obstacle_points_list[0])
            obstacle_points_list.remove(obstacle_points_list[0])
            blubCounter += 1
            continue
        # removes the points belonging to the line from the obstacle list
        remove_line_points_from_obstacle_list(best_line_params[2])
        end_point = best_line_params[2][len(best_line_params[2]) - 1]
        # has to be removed later
        debugCounter += 1
        if best_line_params[2][0] == end_point:
            continue
        # gets the straightened line points
        line_points = get_line_points(artificial_line[0], end_point)
        # puts the line into the straigtened list
        if len(line_points) > 0:
            straightened_list.extend(line_points)
    return straightened_list


def get_best_line_params(artificial_line):
    line_rotations = 135
    line_inaccuracy_offset = 0.01
    best_line_params = [0, 0, []]
    for i in range(line_rotations):
        hits = 0
        miss = 0
        points_on_line = []
        # counts points which are on the artificial line
        for point in obstacle_points_list:
            artificial_line_points = get_line_points(artificial_line[0], artificial_line[1])
            if is_point_on_artificial_line(artificial_line_points, point, line_inaccuracy_offset):
                hits += 1
                miss = 0
                # obstacle_points_list.remove(point)
                points_on_line.append(point)
            else:
                miss += 1
                if miss == 2:
                    break
        if hits > best_line_params[0]:
            best_line_params = [hits, artificial_line, points_on_line]
        artificial_line = [artificial_line[0], rotate(artificial_line[1], artificial_line[0])]
    return best_line_params


def remove_line_points_from_obstacle_list(line):
    for i in range(len(obstacle_points_list)):
        if i == len(line) - 1:
            obstacle_points_list.remove(obstacle_points_list[0])
            return
        obstacle_points_list.remove(obstacle_points_list[0])


def get_line_points(start_point, end_point):
    # list of points that are on the line
    points = [start_point]
    # amount of points that should be on the line
    points_count = int(get_distance(start_point, end_point) * 50)
    try:
        x_diff = (start_point[0] - end_point[0]) / points_count * -1
    except:
        x_diff = 0
    try:
        y_diff = (start_point[1] - end_point[1]) / points_count * -1
    except:
        y_diff = 0
    for i in range(points_count - 2):
        points.append([points[i][0] + x_diff, points[i][1] + y_diff])
    points.append(end_point)
    return points


def get_line_endpoint(start_point, points_list):
    max_dist = -1
    end_point = None
    for point in points_list:
        if get_distance(start_point, point) > max_dist:
            max_dist = get_distance(start_point, point)
            end_point = point
    return end_point


def is_point_on_artificial_line(artificial_line_points, point, inaccuracy_tolerance):
    min_dist = 99999
    for i in range(len(artificial_line_points)):
        dist = math.dist(artificial_line_points[i], point)
        if dist <= min_dist:
            min_dist = dist

    if inaccuracy_tolerance >= min_dist:
        return True

    return False


def detect_parkingspot():
    global final_parking_corner
    final_parking_corner = []
    global parking_spot_found
    global goal
    parking_spot_found = False
    i = 0
    while i < len(corner_list) and parking_spot_found is False:
        first_corner = obstacle_points_list[corner_list[i]]
        j = i
        corner_counter = 0
        while corner_counter < 7 and parking_spot_found is False:
            if j < len(corner_list) and obstacle_points_list[corner_list[j]] is not None:
                second_corner = obstacle_points_list[corner_list[j]]
                distance_between_corner = get_distance(first_corner, second_corner)
                if 0.9 < distance_between_corner < 1.1:
                    index_begin = corner_list[i]
                    index_end = corner_list[j]
                    idx = index_begin + 37
                    middle_of_corner = middle_of_corners(first_corner, second_corner)
                    while idx < index_end:
                        dis = get_distance(obstacle_points_list[idx], middle_of_corner)
                        if 0.50 < dis < 0.60:
                            # we got our parking spot
                            final_parking_corner.append(first_corner)
                            final_parking_corner.append(second_corner)
                            final_parking_corner.append(middle_of_corners(obstacle_points_list[idx], middle_of_corner))
                            parking_spot_found = True
                            # rttalgorithm values

                            direction = get_goal_orientation(second_corner, first_corner)
                            direction = math.degrees(direction)
                            direction = direction + 180
                            optimal_parkingpos = get_point_for_optimal_parkprocess()
                            goal = [optimal_parkingpos, direction]

                            print(goal)
                            convert_to_obstacle_list_for_rttalgorithm(obstacle_points_list)
                            return True
                        idx += 1
            j += 1
            corner_counter += 1
        i += 1
    return False

def detect_parkingspot_horizontal():
    global final_parking_corner
    final_parking_corner = []
    global parking_spot_found
    global goal
    parking_spot_found = False
    i = 0
    while i < len(corner_list) and parking_spot_found is False:
        first_corner = obstacle_points_list[corner_list[i]]
        j = i
        corner_counter = 0
        while corner_counter < 7 and parking_spot_found is False:
            if j < len(corner_list) and obstacle_points_list[corner_list[j]] is not None:
                second_corner = obstacle_points_list[corner_list[j]]
                distance_between_corner = get_distance(first_corner, second_corner)
                if 0.494 < distance_between_corner < 0.505:
                    index_begin = corner_list[i]
                    index_end = corner_list[j]
                    idx = index_begin + 10
                    middle_of_corner = middle_of_corners(first_corner, second_corner)
                    while idx < index_end:
                        dis = get_distance(obstacle_points_list[idx], middle_of_corner)
                        if 0.97 < dis < 1.03:
                            # we got our parking spot
                            final_parking_corner.append(first_corner)
                            final_parking_corner.append(second_corner)
                            final_parking_corner.append(middle_of_corners(obstacle_points_list[idx], middle_of_corner))
                            parking_spot_found = True
                            # rttalgorithm values
                            x, y = calculate_difference_vector(first_corner, second_corner)
                            goal_orientation = math.atan2(y, x)

                            convert_to_obstacle_list_for_rttalgorithm(obstacle_points_list)
                            x1, y1 = middle_of_corners(obstacle_points_list[idx],
                                                       middle_of_corner)  # little overhead, but it's fine
                            goal = [x1, y1, goal_orientation]
                            break
                        idx += 1
            j += 1
            corner_counter += 1
        i += 1


def get_bezier_point():
    middle_between_outer_corners = middle_of_corners(final_parking_corner[0], final_parking_corner[1])
    x_diff = middle_between_outer_corners.x - final_parking_corner[2].x
    y_diff = middle_between_outer_corners.y - final_parking_corner[2].y
    bezier_point = [middle_between_outer_corners.x + x_diff * 2, middle_between_outer_corners.y + y_diff * 2]


def fill_eigenvector_list():
    global eigenvector_list
    eigenvector_list = []
    i = 1
    while i < len(obstacle_points_list):
        try:
            eigenvector_list.append(calculate_difference_vector(obstacle_points_list[i - 1], obstacle_points_list[i]))
            i += 1
        except:
            obstacle_points_list.remove(obstacle_points_list[i - 1])


def fill_obstacle_point_list():
    global obstacle_points_list
    obstacle_points_list = []
    for i in range(len(scan_data_list)):
        obstacle_points_list.append(calculate_x_y(scan_data_list[i][0], scan_data_list[i][1]))


def calculate_degree_between_vectors(point1, point2):
    # Calculating the dot product
    dot_product = point1[0] * point2[0] + point1[1] * point2[1]

    # Calculate the magnitude of vector 1 and vector 2
    mag1 = math.sqrt(point1[0] ** 2 + point1[1] ** 2)
    mag2 = math.sqrt(point2[0] ** 2 + point2[1] ** 2)

    # Calculate the cosine value
    cos_val = dot_product / (mag1 * mag2)
    if cos_val > 1:
        cos_val = 1
    if cos_val < -1:
        cos_val = -1

    # calculate the angle in degrees
    angle = math.acos(cos_val)

    return math.degrees(angle)


def read_test_data_np(path):
    file = open(path, 'r')
    lines = file.readlines()
    coord_list = []
    for line in lines:
        angle, dist = line.split(' ')
        dist = dist.replace("\n", "")
        coord = calculate_x_y(float(angle), float(dist))
        coord_list.append(coord)
    return np.array(coord_list)


def modify_file():
    file = open("./res/test_data.txt", 'r')
    lines = file.readlines()
    new_lines = []
    for line in lines:
        split = line.split(" ")
        newline = split[0] + " " + split[1] + "\n"
        new_lines.append(newline)
    file.close()
    file = open("./res/test_data.txt", "w")
    file.write("")
    file = open("./res/test_data.txt", 'a')
    for line in new_lines:
        file.write(line)
    file.close()


def read_test_data(path):
    global scan_data_list
    file = open(path, 'r')
    lines = file.readlines()
    scan_data_list = []
    prev = None
    for line in lines:
        angle, dist = line.split(' ')
        dist = dist.replace("\n", "")
        # if prev is not None and prev[0] == float(angle) and prev[1] == float(dist):
        #    continue
        scan_data_list.append([float(angle), float(dist)])
        prev = scan_data_list[len(scan_data_list) - 1]


def calculate_difference_vector(point1, point2):
    dx = point2[0] - point1[0]
    dy = point2[1] - point1[1]
    norm = (dx ** 2 + dy ** 2) ** 0.5

    return [round((dx / norm), 2), round((dy / norm), 2)]


def calculate_gradient(point1, point2):
    temp = point2[0] - point1[0]
    if temp == 0:
        temp += 0.1

    return (point2[1] - point1[1]) / temp


def degree_distance(x, y):
    dist = ((x ** 2) + (y ** 2)) ** 0.5
    degree = math.degrees(math.atan2(y, x))
    return degree, dist


# start = [1.949, -0.469]
# end = [2.2382857142857144, -0.026142857142857107]
def calculate_x_y(degrees, distance):
    x = distance * math.cos(math.radians(degrees))
    y = distance * math.sin(math.radians(degrees))
    return [round(x, 3), round(y, 3)]


def get_distance(point1, point2):
    dis = math.sqrt(((point2[0] - point1[0]) ** 2) + ((point2[1] - point1[1]) ** 2))
    return dis


def middle_of_corners(point1, point2):
    mx = (point1[0] + point2[0]) / 2
    my = (point1[1] + point2[1]) / 2

    return [mx, my]


def get_distance_point2line(point, line):
    global norm
    p1 = np.array([line[0][0], line[0][1]])
    p2 = np.array([line[1][0], line[1][1]])

    p3 = np.array([point[0], point[1]])
    d = np.abs(norm(np.cross(p2 - p1, p1 - p3))) / norm(p2 - p1)
    return d


def rotate(point, origin):
    global radians
    global cos_rad
    global sin_rad

    x, y = point
    offset_x, offset_y = origin
    adjusted_x = (x - offset_x)
    adjusted_y = (y - offset_y)

    qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
    qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y
    return qx, qy


# NOT TESTED VERSION of a referencepoint to generate and update a world coordinate system. This is necessary to know the positio of the
# vehicle respectivley to the optimal parkspot pos, where we need to go to park correctly and respectivley to the parkspot
# code should be fast enought, if not(what i higly doubt), adjust to np functions
def adjust_point(point, diff_x_y):
    x, y = point
    x += diff_x_y[0]
    y += diff_x_y[1]
    return [x, y]


def point_difference(point1, point2):
    x_diff = point1[0] - point2[0]
    y_diff = point1[1] - point2[1]
    return [x_diff, y_diff]


def update_world_coords(diff_x_y):
    global new_vehicle_optimalparkposition_parkspot_coords
    global old_vehicle_optimalparkposition_parkspot_coords

    for i in range(new_vehicle_optimalparkposition_parkspot_coords):
        new_vehicle_optimalparkposition_parkspot_coords[i] = adjust_point(
            old_vehicle_optimalparkposition_parkspot_coords[i], diff_x_y)


def generate_and_update_world_coords():
    global old_column_pos
    global new_column_pos
    global obstacle_points_list
    global vehicle_position

    # Our Pillar min and max points in the obstacle_points_list
    # here we need to adjust due to our pillar
    min_number_points_for_pillar_threshhold = 3
    max_number_points_for_pillar_threshhold = 10
    counter = 0

    # the idea is, that the real worl pillar has in his neighborhood no near obstacles, so the difference between the endpoints of the diameter
    # to the following neighbor in the collected obstace_points_list must be huge. If we hit 2 huge diffs within the max threshhold to classify the points as
    # the pillar, we can update the whole coord system
    huuuge_diff = 2.0
    mid_point_of_circle = [0.0, 0.0]
    is_done = False
    for i in range(obstacle_points_list) and not is_done:
        point_counter = 0
        # detect our real world Column
        dist_bet_points = get_distance(obstacle_points_list[i], obstacle_points_list[i + 1])
        if dist_bet_points >= huuuge_diff:
            j = i + 1
            for j in range(obstacle_points_list) and not is_done:
                # is there annother huge dist difference?
                dist = get_distance(obstacle_points_list[j], obstacle_points_list[j + 1])
                if dist >= huuuge_diff and point_counter >= min_number_points_for_pillar_threshhold:
                    # safe our endpoints from
                    first_diameter_endpoint = obstacle_points_list[i]
                    second_diameter_endpoint = obstacle_points_list[j]
                    # its the middlepoint of the circle in this case
                    new_column_pos = middle_of_corners(first_diameter_endpoint, second_diameter_endpoint)
                    diff_x_y = point_difference(old_column_pos, new_column_pos)
                    adjust_point(vehicle_position, diff_x_y)
                    is_done = True

                else:
                    ++counter
                    if max_number_points_for_pillar_threshhold <= counter:
                        i = j
                        break
                ++point_counter


# needed for rttstarreedshepp algorithm
def convert_to_obstacle_list_for_rttalgorithm(old_obstacle_list):
    global new_obstacle_list
    global obstacle_points_list
    new_obstacle_list = []
    for point in old_obstacle_list:
        x, y = point

        new_obstacle_list.append((y * (-1), x * (-1), 1))
    # obstacle_points_list = new_obstacle_list


def get_goal_orientation(corner1, corner2):
    x1 = corner1[0]
    y1 = corner1[1]
    x2 = corner2[0]
    y2 = corner2[1]
    # steigung
    m = (y2 - y1) / (x2 - x1)
    goal_orientation = math.atan(m)

    return goal_orientation


if __name__ == "__main__":
    main()
