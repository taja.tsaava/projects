/*
 ============================================================================
 Name        : myshell.c
 Author      : Taja
 Version     : 1.0
 ============================================================================
*/

#include <stdio.h>

#include <stdlib.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define TOKEN_BUFSIZE 64
#define TOKEN_DELIM " \t\r\n\a"
#define MAXLINE 256
#define ANSWERCHAR 1

//Declaration of functions
char **split_line(char *line);
//int checkamp(char **exec);
//pid_t execute(char **exec);

// ################# BEGINNING OF MAIN ####################
int main(int argc, char **argv) {

char **args;
char buf[MAXLINE];
char logout[] = "logout";
pid_t pid, wpid;
int status;
char yes[] = "y";
int terminate = 1;
int amp = 1;
char a = '&';
int counter = 0;
int bgp[MAXLINE];

while (terminate != 0) {
	printf("%% "); /*print prompt*/
	fgets(buf, MAXLINE, stdin);
	//while (fgets(buf, MAXLINE, stdin) != NULL) {
	buf[strlen(buf) - 1] = '\0'; /*replace newline with null*/


	printf("command: %s \n", buf); /*print prompt*/
	if(strcmp(buf, logout) == 0){
		printf("Do you really want to exit?\n [y/n]: ");
		printf("%% "); /*print prompt*/
		fgets(buf, MAXLINE, stdin);
		buf[strlen(buf) - 1] = 0;

		if(strcmp(buf, yes) == 0){

			printf("Terminating...");
			terminate = 0;
			break;
		}

	}



	else{

		for (int i = 0; i == i; i++) {
			if (buf[i] == '\0') {
			   if (buf[i-1] == a) { /*checks if & is at end of the line */
			      buf[i-1] = '\0';
			      amp = 1;
			      break;
			    }
			   else{
					amp = 0;
					break;
			   }
			}
		}



		args = split_line(buf);

		if (args[0] == NULL) // An empty command was entered.
		    	break;

		/*
		 exec = split_line(buf);
		pid = execute(exec);

		 */

		if((pid = fork()) < 0) {
			printf("fork error\n");
		}

		else if(pid == 0) {    /* child process */

			//execlp(buf, buf, (char *) 0);
			if (execvp(args[0], args) == -1) {
				perror("arguments");
			}
		 	printf("couldn't execute: %s\n", buf);
		 	exit(127);
		}

		/* parent process */
		if(amp == 1){
			bgp[counter] = pid;
						counter++;
			printf("gestartete Hintergrundprozesse:\n");
			for (int i = 0; i < counter; i++){
				printf("pid: %i\n", bgp[i]);
			}
		}

		else{
			do {
				wpid = waitpid(pid, &status, WUNTRACED);
			} while (!WIFEXITED(status) && !WIFSIGNALED(status));
		}

		}

	}
	exit(0);
}

	// ################# END OF MAIN ####################


	char **split_line(char *line)
	{
	 int bufsize = TOKEN_BUFSIZE, position = 0;
	 char **tokens = malloc(bufsize * sizeof(char*));
	 char *token;

	 if (!tokens) {
	   fprintf(stderr, "allocation error\n");
	   exit(EXIT_FAILURE);
	 }

	 token = strtok(line, TOKEN_DELIM);
	 while (token != NULL) {
	   tokens[position] = token;
	   position++;

	   if (position >= bufsize) {
	     bufsize += TOKEN_BUFSIZE;
	     tokens = realloc(tokens, bufsize * sizeof(char*));
	     if (!tokens) {
	       fprintf(stderr, "allocation error\n");
	       exit(EXIT_FAILURE);
	     }
	   }

	   token = strtok(NULL, TOKEN_DELIM);
	 }
	 tokens[position] = NULL;
	 return tokens;
	}
