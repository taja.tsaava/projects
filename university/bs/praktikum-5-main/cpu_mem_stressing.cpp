#include <chrono>
#include <iostream>
#include <limits>
#include <sstream>
#include <thread>

int main()
{
	std::cout << "cpu stressing, fixed interval of 500us!" << std::endl;
		for(uint64_t i=0; i<std::numeric_limits<uint64_t>::max(); i++)
			std::this_thread::sleep_for(std::chrono::nanoseconds(500));

}