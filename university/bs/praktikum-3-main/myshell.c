/* 
 * File:   myshell.c
 * Author: Dominique Hellbach
 * Author: Anastasia Tasaava
 * 
 * Created on 15 November 2021, 19:52
 */

#include "myshell.h"

/** @brief speichert die PID eines aktiven Vordergrundprozesses */
pid_t active_pid;
/** @brief Pointer auf die Liste der gestoppten Vordergrundprozesse */
pid_t *stoppedforeground;
/** @brief aktuelle Position zum Eintragen in die Liste der gestoppten Vordergrundprozesse */
int posforeground;
/** @brief Aktuell allokierte Größe der Liste der gestoppten Vordergrundprozesse */
int sizeforeground;
/** @brief Pointer auf die Liste der Hintergrundprozesse */
pid_t *bckgrpid;
/** @brief aktuelle Position zum Eintragen in die Liste der Hintergrundprozesse */
int posbackground;
/** @brief Aktuell allokierte Größe der Liste der Hintergrundprozesse */
int sizebackground;
/** @brief Flag, welche gesetzt wird, falls SIGTSTP gehandelt und an einen Prozess gesendet wird */
int sigtstpflag = 0;
/** @brief repräsentiert Ergebnis des Sendens von SIGTSTP an einen Prozess */
int sigtstpsuc = 0;


char *readline() {
    char *line = NULL;
    size_t buf = 0; //getline weist buf zu
    if (getline(&line, &buf, stdin) == -1){
        perror("getline() Error\n");
        exit(EXIT_FAILURE);
    }
    return line;
}

char **parse(char *line) {
    int bufsize = BUFSIZE;
    int position = 0;
    char **tokens = malloc(bufsize * sizeof(char*));
    char *token;
    
    if (!tokens) {
        perror("malloc() Error\n");
        exit(EXIT_FAILURE);
    }
    
    //Beim ersten Aufruf von strtok ruft man es mit der ersten Adresse des aufzuteilendes char arrays auf
    token = strtok(line, DELIM);
    //strtok gibt NULL zurück, wenn es das Ende erreicht hat
    while (token != NULL) {
        tokens[position] = token;
        position++;
        //Wenn Eingabe Buffer überschreiten könnte, wird Buffer vergrößert
        if (position >= bufsize) {
            bufsize += BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char*));
            if (!tokens) {
                perror("realloc() Error\n");
                exit(EXIT_FAILURE);
            }
        }
        //Bei folgenden Aufrufen wird NULL statt Adresse erwartet, dann macht strtok weiter, wo es beim vorherigen
        //Aufruf aufgehört hat
        token = strtok(NULL, DELIM);
    }
    //Token Liste wird am Ende mit NULL terminiert
    tokens[position] = token;
    return tokens;
}

//findet das Ende der Liste und überprüft dann auf '&'
int checkamp(char **exec) {
    for (int i = 0; i == i; i++) {
        if (exec[i] == NULL) {
            if (exec[i-1][0] == '&' && exec[i-1][1] == '\0') {
                exec[i-1] = '\0';
                return 1;
            } else
                return 0;
        }
    }
}

pid_t execute(char **exec) {
    pid_t pid;
    int status;
    int amp = checkamp(exec);
    
    //klont aktuellen Prozess mit neuer pid
    pid = fork();
    setpgid(pid, pid);
    if (pid == 0) {
        // Child Process führt Befehl aus, exec[0] gibt Programmnamen an, exec teilt dem Programm die übergebenen Befehle mit
        if (execvp(exec[0], exec) == -1) {
            perror("exec Error\n");
            exit(EXIT_FAILURE);
        } else {
            exit(EXIT_SUCCESS);
        }
    // Ab hier für Parent Prozess
    } else if (pid < 0) {
        perror("exec Error\n");
        return 0;
    // Wenn kein '&' vorhanden, wird auf Ende des Child Prozesses gewartet
    } else if (!amp) {        
        //waitpid speichert in status den status des Child Prozesses ab
        active_pid = pid;
        waitpid(active_pid, &status, WUNTRACED);
        if (sigtstpflag) { //Wenn Flag von handler_SIGTSTP gesetzt wurde
            exec_SIGTSTP();
        }
        active_pid = 0;
    } else {
        //falls '&' vorhanden ist, wartet Parent nicht und gibt pid zurück für die Liste
        return pid;
    }
    return 0;
}

int countlists() {
    int count = 0;
    for (int i = 0; i < posbackground; i++) {
        if (bckgrpid[i]) {
            count++;
            break;
        }
    }
    if (!count) {
        for (int i = 0; i < posforeground; i++) {
            if (stoppedforeground[i]) {
                count++;
                break;
            }
        }
    }
    return count;
}

int logout(){
    char *line;
    int count = countlists();
    if (count) { // Wenn Prozesse in einer der beiden Listen gefunden wurden
        printf("Aktuell laufende Prozesse:\n");
        printprocesses();
        printf("Shell trotzdem beenden? (Alle Hintergrundprozesse und gestoppten Prozesse werden beendet!) [y/n] ");
        line = readline();
        if (strcmp(line, YES) == 0){ //wenn bestätigt
            //terminiert alle eingetragenen Hintergrundprozesse und trägt sie aus Liste aus
            for (int i = 0; i < posbackground; i++) { 
                if (bckgrpid[i]) {
                    kill(bckgrpid[i], SIGCONT);
                    kill(bckgrpid[i], SIGTERM);
                    bckgrpid[i] = (pid_t)0;
                }
            }
            //führt gestoppte Prozesse erst weiter aus und terminiert sie dann und trägt sie aus der Liste aus
            for (int i = 0; i < posforeground; i++) { 
                if (stoppedforeground[i]) {
                    kill(stoppedforeground[i], SIGCONT);
                    kill(stoppedforeground[i], SIGTERM);
                    stoppedforeground[i] = (pid_t)0;
                }
            }
            count = countlists();
            if (count) { //Wenn trotzdem noch Prozesse eingetragen sind
                printf("Es konnten nicht alle Prozesse beendet werden!\n");
                printprocesses();
                return 2; //Logout nicht erfolgreich
            } else return 1; //Logout erfolgreich
        } else return 2; //Logout abgelehnt
    } else {
        printf("Shell beenden? [y/n] ");
        line = readline();
        if (strcmp(line, YES) == 0){
            return 1; //Logout erfolgreich
        } else return 2; //Logout abgelehnt
    }
}

int stop(pid_t pid){
    if (pid > 0) {
        int success = kill(pid, SIGSTOP);
        if (!success) {
            printf("Prozess mit PID %i angehalten\n", pid);
        } else if (errno == 1) {
            printf("Prozess mit PID %i konnte nicht gestoppt werden, da die Berechtigung fehlt!\n", pid);
        } else if (errno == 3) {
            printf("Prozess mit PID %i existiert nicht!\n", pid);
        } else {
            printf("Prozess mit PID %i konnte aus unbekannten Gruenden nicht gestoppt werden!\n", pid);
        }
    } else {
        printf("PID ungueltig!\n");
    }
    return 2; //interner Shell Befehl ausgeführt
}

int cont(pid_t pid) {
    int status;
    if (pid > 0) { 
        int failure = kill(pid, SIGCONT);
        if (!failure) { //Wenn SIGCONT erfolgreich gesendet
            if (checkstoppedforeground(pid)) { //Wenn PID in Liste der gestoppten Vordergrundrozesse eingetragen
                active_pid = pid;
                waitpid(active_pid, &status, WUNTRACED); //Ausführung erfolgt im Vordergrund
                if (sigtstpflag) { //Wenn Flag von handler_SIGTSTP gesetzt wurde
                    exec_SIGTSTP();
                }
                active_pid = 0;
            } else {
                printf("Prozess mit PID %i wird im Hintergrund fortgefuehrt\n", pid);
            }
        } else if (errno == 1) {
            printf("Prozess mit PID %i konnte nicht fortgefuehrt werden, da die Berechtigung fehlt!\n", pid);
        } else if (errno == 3) {
            printf("Prozess mit PID %i existiert nicht!\n", pid);
        } else {
            printf("Prozess mit PID %i konnte aus unbekannten Gruenden nicht fortgefuehrt werden!\n", pid);
        }
    } else {
        printf("PID ungueltig!\n");
    }
    return 2; //interner Shell Befehl ausgeführt
}

int checkinput(char **exec){
    pid_t pid;
    if (strcmp(exec[0], LOGOUT) == 0 && exec[1] == NULL) { // bei Eingabe von 'logout'
        return logout();
    } else if (strcmp(exec[0], STOP) == 0 && exec[1] != NULL && exec[2] == NULL) { // Eingabe von:      stop LEER 'irgendwas' \n
        pid = (pid_t)(atoi(exec[1])); //wandelt 2. Token in int um
        return stop(pid);
    } else if (strcmp(exec[0], CONT) == 0 && exec[1] != NULL && exec[2] == NULL) { // Eingabe von:      cont LEER 'irgendwas' \n
        pid = (pid_t)(atoi(exec[1])); //wandelt 2. Token in int um
        return cont(pid);
    } else {
        return 0;
    }
}

void start() {
    int contin = 1; // while Bedingung für Hauptloop
    int check = 0;
    pid_t pid;
    char *line;
    char **exec;
    //Initialisierung der Listen
    posforeground = 0;
    sizeforeground = ARRAY_SIZE;
    posbackground = 0;
    sizebackground = ARRAY_SIZE;
    pid_t *backgrpid = malloc(ARRAY_SIZE * sizeof(pid_t));
    pid_t *stoppedforground = malloc(ARRAY_SIZE * sizeof(pid_t));
    bckgrpid = backgrpid;
    stoppedforeground = stoppedforground;

    
    do {
        printf("> ");
        line = readline();
        exec = parse(line);
        check = checkinput(exec);
        if (check == 1) { // bei Eingabe und Bestätigung von 'logout'
            contin = 0; // while Bedingung
        } else if (!check) { // Kein Shell interner Befehl wurde verarbeitet, da interner Befehl -> 2          
            pid = execute(exec);
            if (pid) {
                insertbackground(pid);
            }
            printprocesses();
        }
        free(exec);
        free(line);
    } while (contin);
}

void printprocesses(){
    int printcheck = 0; // Wurde was gedruckt?
    if (posbackground == 0) {
        printf("Keine laufenden Hintergrundprozesse\n");
    } else {
        printf("gestartete Hintergrundprozesse:\n");
        for (int i = 0; i < posbackground; i++) { //Hintergrundprozesse ausgeben
            if (bckgrpid[i]) {
                printf("pid: %i\n", bckgrpid[i]);
                printcheck++;
            }
        }
        if (!printcheck) {
            printf("Keine laufenden Hintergrundprozesse\n");
        }
    }
    printcheck = 0;
    if (posforeground == 0) {
        printf("Keine gestoppten Prozesse\n");
    } else {
        printf("gestoppte Prozesse:\n");
        for (int i = 0; i < posforeground; i++) { // gestoppte Vordergrundprozesse ausgeben
            if (stoppedforeground[i]) {
                printf("pid: %i\n", stoppedforeground[i]);
                printcheck++;
            }
        }
        if (!printcheck) {
            printf("Keine gestoppten Prozesse\n");
        }
    }    
}

void exec_SIGTSTP(){
    sigtstpflag = 0; //Flag reset
    if (!sigtstpsuc) {
        printf("\nProzess mit PID %i angehalten\n", active_pid);
        insertstoppedforeground(active_pid); // Eintragen des gestoppten Prozesses
    } else if (errno == 1) {
        printf("Prozess mit PID %i konnte nicht angehalten werden, da die Berechtigung fehlt!\n", active_pid);
    } else if (errno == 3) {
        printf("Prozess mit PID %i existiert nicht!\n", active_pid);
    } else {
        printf("Prozess mit PID %i konnte aus unbekannten Gruenden nicht angehalten werden!\n", active_pid);
    }
}

void handle_SIGTSTP(int signum){  
    if (active_pid) {
        sigtstpflag = 1;
        sigtstpsuc = kill(active_pid, SIGSTOP);
    }
}

void handle_SIGCHLD(int signum){
    int status;
    // iteriert durch alle eingetragenen Prozesse, hat dadurch Chance mit Prozessen umzugehen, die inzwischen terminiert wurden
    for (int i = 0; i < posbackground; i++) { 
        waitpid(bckgrpid[i], &status, WNOHANG | WUNTRACED | WCONTINUED);
        if (WIFSTOPPED(status) || WIFCONTINUED(status)) {
            continue;
        }
        else if (WIFEXITED(status) || WIFSIGNALED(status)) { // Wenn Prozess beendet, wird er ausgetragen
            bckgrpid[i] = (pid_t)0;
        }
    }
}

void insertstoppedforeground(pid_t pid){
    stoppedforeground[posforeground] = pid;
    posforeground++;
    if (posforeground >= sizeforeground) { //wenn allokierter Speicher ausgereizt
        sizeforeground += ARRAY_SIZE;
        stoppedforeground = realloc(stoppedforeground, sizeforeground * sizeof(int));
        if (!stoppedforeground) {
            perror("realloc() Error\n");
            exit(EXIT_FAILURE);
        }
    }
}

int checkstoppedforeground(pid_t pid){
    for (int i = 0; i < posforeground; i++) {
        if (pid == stoppedforeground[i]) {
            stoppedforeground[i] = (pid_t)0;
            return 1;
        }
    }
    return 0;
}

void insertbackground(pid_t pid) {
    bckgrpid[posbackground] = pid;
    posbackground++;
    if (posbackground >= sizebackground) { //wenn allokierter Speicher ausgereizt
        sizebackground += ARRAY_SIZE;
        bckgrpid = realloc(bckgrpid, sizebackground * sizeof(pid_t));
        if (!bckgrpid) {
            perror("realloc() Error\n");
            exit(EXIT_FAILURE);
        }
    }
}