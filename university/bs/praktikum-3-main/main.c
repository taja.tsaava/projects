/* 
 * File:   main.c
 * Author: Dominique Hellbach
 * Author: Anastasia Tsaava
 * 
 * Created on 15 November 2021, 19:52
 */

#include "myshell.h"

int main(int argc, char** argv) {
    
    signal(SIGTSTP, handle_SIGTSTP);
    signal(SIGCHLD, handle_SIGCHLD);
    
    start();
    
    return (EXIT_SUCCESS);
}