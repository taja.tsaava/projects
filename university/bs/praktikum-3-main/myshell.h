/* 
 * File:   myshell.h
 * Author: Dominique Pierre Hellbach
 * Author: Anastasia Tsaava
 *
 * Created on 15 November 2021, 19:52
 */

#ifndef MYSHELL_H
#define MYSHELL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>

/**
 * @class myshell
 * @brief Diese Klasse stellt eine Shell dar
 * 
 * Diese Klasse kümmert sich um die Ausführung einer Shell
 * Sie fragt Eingaben ab und ist in der Lage Prozesse nach Name aufzurufen
 * ein '&' am Ende einer Angabe lässt den Prozess im Hintergrund laufen
 * Die PID's von Hintergrundprozessen werden gespeichert
 * Mit der Eingabe von 'logout' kann die Shell beendet werden
 * 'stop' mit Angabe einer PID stoppt per SIGSTOP einen Prozess
 * 'cont' mit Angabe einer PID führt einen gestoppten Prozess wieder aus
 */

/** @brief Größe der für Listen */
#define ARRAY_SIZE 8
/** @brief Größe für allokierten Speicher, wird auch zum weiteren Vergrößern benutzt */
#define BUFSIZE 128
/** @brief Deliminatoren für strtok(), welche in parse() benutzt wird */
#define DELIM " \t\r\n"
/** @brief string welcher für logout aus der Shell benutzt wird */
#define LOGOUT "logout"
/** @brief string, welcher für Bestätigung innerhalb der Shell benutzt wird */
#define YES "y\n"
/** @brief string, welcher zum Stoppen eines Hintergrundprozesses benutzt wird */
#define STOP "stop"
/** @brief string, welcher zum Fortführen eines Prozesses benutzt wird */
#define CONT "cont"

/**
 * @brief readline liest die Eingabe im Terminal aus
 * @return ein Pointer der auf den ersten char der Eingabe zeigt
 */
char *readline();

/**
 * @brief Teilt die Eingabe mittels strtok() in Token auf
 * @param [in] line ist der Pointer auf den ersten char der Terminal-Eingabe
 * @return ein Pointer der auf den ersten Pointer auf ein char des ersten Tokens zeigt, wie eine 2-dimensionale Liste
 * 
 * Es wird die Eingabe des Terminals übergeben und mittels strtok() und den definierten Deliminatoren
 * die Eingabe in einzelne Tokens aufgeteilt. Es wird eine Art Liste erzeugt aus den Pointern
 * die auf den jeweils ersten char der einzelnen Tokens zeigen. Als Rückgabe gibt es einen Pointer auf
 * den Anfang dieser Liste
 */
char **parse(char *line);

/**
 * @brief Die eingelesene Line wird auf ein '&' am Ende überprüft
 * @param [out] exec ist die von parse() erzeugte Tokenliste
 * @return ein int welches 1 ist, wenn ein '&' am Ende war. 0 wenn nicht
 * 
 * Die Tokenliste wird nur am Ende auf ein einzelnes '&' überprüft. Es muss in der Eingabe also
 * direkt nach einem Deliminator stehen und danach dürfen auch nur Deliminatoren kommen.
 * Wenn es gefunden wird, wird es entfernt und durch ein '\0' ersetzt und eine 1 returned,#
 * ansonsten eine 0
 */
int checkamp(char **exec);

/**
 * @brief Sorgt für die Erstellung eines neuen Prozesses, welcher die Eingabe ausführt
 * @param [in] exec die aufgeteilte Liste von Tokens
 * @return Die PID, wenn Hintergrundprozess, ansonsten 0
 * 
 * Es wird ein child Prozess erzeugt, welcher versucht den Befehl auszuführen
 * Mit '&' am Ende des Befehls, wartet der parent Prozess nicht, anonsten wird
 * auf den child Prozess gewartet
 */
pid_t execute(char **exec);

/**
 * @brief enthält den Hauptloop zur Ausführung der Shell
 * 
 * Hier ist der Hauptloop der Shell enthalten. Diese Funktion wird von der Main aufgerufen
 * und kümmert sich um alle weiteren Funktionsaufrufe und Terminalausgaben
 */
void start();

/**
 * @brief Signalhandler für SIGTSTP
 * @param [in] signum ist das Signal
 * 
 * sendet das SIGTSTP Signal und setzt ein Flag für weitere Verarbeitung
 * durch exec_SIGTSTP
 */
void handle_SIGTSTP(int signum);

/**
 * @brief gibt Ergebnis des kill(pid,SIGTSTP) aus und trägt PID in Liste der gestoppten Prozesse ein
 */
void exec_SIGTSTP();

/**
 * @brief Signalhandler für SIGCHLD
 * @param [in] signum ist das Signal
 */
void handle_SIGCHLD(int signum);

/**
 * @brief Überprüft Eingabe auf interne Befehle 'logout', 'stop', 'cont'
 * @param [in] exec ist die aufgeteilte Liste von Tokens
 * @return ein int, welches das Ergebnis des Inputchecks repräsentiert
 * 
 * 0 = kein interner Befehl gefunden
 * 1 = Logout bestätigt
 * 2 = interner Befehl ausgeführt
 */
int checkinput(char **exec);

/**
 * @brief Überprüft, ob noch gestoppte oder Hintergrundprozesse in den Listen eingetragen sind
 * @return ein int, welches 1 ist, wenn es einen Eintrag in den Listen gibt
 */
int countlists();

/**
 * @brief Kümmert sich um den Logout aus der Shell
 * @return int, welches das Ergebnis des Logout repräsentiert
 * 
 * Wird nur aus checkinput() aufgerufen. Return dieser Funktion wird direkt von checkinput() returned
 * 1 = Logout bestätigt und erfolgreich
 * 2 = Logout abgelehnt oder nicht erfolgreich (z.b. auf Grund nicht beendbarer Prozesse)
 */
int logout();

/**
 * @brief Kümmert sich um den internen Shell Befehl zum Stoppen von Prozessen
 * @param [in] pid des Prozesses, welcher gestoppt werden soll
 * @return int 2, welches Ausführung eines internen Shell Befehls repräsentiert
 * 
 * Wird nur aus checkinput() aufgerufen. Return dieser Funktion wird direkt von checkinput() returned
 */
int stop(pid_t pid);

/**
 * @brief Kümmert sich um den internen Shell Befehl zum Weiterlaufen von gestoppten Prozessen
 * @param [in] pid des Prozesses, welcher weiterlaufen soll
 * @return int 2, welches Ausführung eines internen Shell Befehls repräsentiert
 * 
 * Wird nur aus checkinput() aufgerufen. Return dieser Funktion wird direkt von checkinput() returned
 */
int cont(pid_t pid);

/**
 * @brief Kümmert sich um Eintrag einer PID in die Liste der Hintergrundprozesse
 * @param [in] pid des Prozesses, welcher als Hintergrundprozess gestartet wurde
 */
void insertbackground(pid_t pid);

/**
 * @brief Kümmert sich um Eintrag einer PID in die Liste der gestoppten Vordergrundprozesse
 * @param [in] pid des Vordergrundprozesses, welcher gestoppt wurde 
 */
void insertstoppedforeground(pid_t pid);

/**
 * @brief Überprüft, ob eine PID in der Liste der gestoppten Prozesse steht
 * @param [in] pid welche überprüft werden soll
 * @return int, welches Fund repräsentiert. 1-> wahr, 0-> falsch
 */
int checkstoppedforeground(pid_t pid);

/**
 * @brief Ausgabe der PID der Prozesse, welche in den Liste der Hintergrund- und gestoppten Prozessen stehen
 */
void printprocesses();

#endif /* MYSHELL_H */
