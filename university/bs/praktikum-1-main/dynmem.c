#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <unistd.h>
#include <signal.h>
#include <setjmp.h>
#include <string.h>

struct rusage r_usage;
long int maxMem;
void* startp, *endp;
jmp_buf restore_point;

void sig_term_handler(int signum)
{
    longjmp(restore_point, signum);
}

void catch_sigsegv()
{
    static struct sigaction _sigact;

    memset(&_sigact, 0, sizeof(_sigact));
    _sigact.sa_handler = sig_term_handler;
    _sigact.sa_flags = SA_SIGINFO;

    sigaction(SIGSEGV, &_sigact, NULL);
}

void funcMem() {
    unsigned long int size = 1;
    int counter = 0;
    int *intarr = malloc(size * sizeof(int));
    maxMem = 0;
    FILE * fp = fopen("funcMEM.txt", "w");
    fclose(fp);
    while (1) {
        intarr[size -1] = size -1;
        getrusage(RUSAGE_SELF, &r_usage);
        if (maxMem == r_usage.ru_maxrss)
            counter++;
        else
            counter = 0;
        if (counter > 3000000) {
            maxMem = r_usage.ru_maxrss;
            printf("Determined maximum memory usage = %li kb\n", r_usage.ru_maxrss); 
            free(intarr);
            break;
        }
        maxMem = r_usage.ru_maxrss;
        if (size % 40000000UL == 0) {
            printf("User CPU time at element %lu = %li\n",size,r_usage.ru_utime.tv_sec);
            printf("System CPU time at element %lu = %li\n",size,r_usage.ru_stime.tv_sec);
            printf("Used memory at element %lu = %li MB\n",size,r_usage.ru_maxrss / 1024L);
        }
        size++;
        intarr = realloc(intarr, size * sizeof(int));
    }
}

int funcRec(int i) {
    if (i == 0)
        startp = (void*)&i;
    endp = (void*)&i;
    int temp;
    temp = i;
    if (i % 10000 == 0 || i == 0) {
        getrusage(RUSAGE_SELF, &r_usage);
        printf("User CPU time at depth %i = %li.%li\n",i,r_usage.ru_utime.tv_sec,r_usage.ru_utime.tv_usec);
        printf("System CPU time at depth %i = %li.%li\n",i,r_usage.ru_stime.tv_sec,r_usage.ru_stime.tv_usec);
        printf("Stack size at depth %i = %i kB\n",i, (int)(startp-endp) / 1024);
    }
    i++;
    i = funcRec(i);
    return i;
}


int main(int argc, char** argv) {

    catch_sigsegv();
    struct rlimit limit;
    limit.rlim_cur = -1;
    limit.rlim_max = -1;
    setrlimit(RLIMIT_AS, &limit);

    getrusage(RUSAGE_SELF, &r_usage);
    long int maxMem = 0;
    
    //funcMem();
    
    limit.rlim_cur = 8192*1024;
    limit.rlim_max = 8192*1024;
    setrlimit(RLIMIT_AS, &limit);
    
    int fault_code = setjmp(restore_point);
    if (fault_code == 0)
    {
        funcRec(0);
    }
    
    struct rlimit r_limit_stack;
    getrlimit(RLIMIT_AS, &r_limit_stack);
    struct rlimit r_limit_mem;
    getrlimit(RLIMIT_MEMLOCK, &r_limit_mem);
    
    printf("Determined maximum memory size = %li MB\n", maxMem / 1024L);
    printf("Determined maximum stack size = %i kB\n", (int)(startp-endp) / 1024);
    printf("System set soft limit for memory usage = %lu MB\n",r_limit_mem.rlim_cur / 1024UL / 1024UL);
    printf("System set soft limit for stack size = %lu kB\n",r_limit_stack.rlim_cur / 1024UL);
    
    return (EXIT_SUCCESS);
}

