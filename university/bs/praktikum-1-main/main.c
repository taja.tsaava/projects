/* 
 * File:   main.c
 * Author: dh
 *
 * Created on 24 November 2021, 11:15
 */

/*
 * fragt Eingabe nach m/n und Empfängervariante ab, erstellt zusteller/empfängerobjekte variable
 * erstellt m/n Zusteller-/Empfängerthreads und Logistikzentrumthreads
 * führt Liste mit ID's von Zustellern
 * führt liste mit Empfängerthread-ID, zum erstellen von Briefen benutzt
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <time.h>

typedef struct{
    unsigned long long inhalt;
    int empfaengerID;
} brief;

// Liste ZustellerKernelThreadID's
int zustellerID[100];

// Liste EmpfaengerKernelThreadID's
int empfaengerID[100];

// speichert Input
int anzahlZusteller;
int anzahlEmpfaenger;
char variante[10];
int wartezeit;
//berechnet aus wartezeit*3 als Obergrenze für zufällige Wartezeiten von Empfänger und Zustellern
int upperBound;

// speichert die Lager(Briefliste) der Zusteller als Liste
brief zustellerLager[100][10];

// speichert die Briefkästen der Empfänger als Liste
brief empfaengerBriefkasten[100][1000];

// Semaphor Signale für die Zusteller von Logistikzentrum -> Zusteller kann los
sem_t lagerSem[100];

// Mutexe sichern die Lager der Zusteller als kritischen Bereich
pthread_mutex_t lagerMutex[100];

// Mutexe sichern die Briefkästen der Empfänger als kritischen Bereich
pthread_mutex_t briefkastenSignale[100];

// Semaphor Signal mit dem die Zusteller die EMpfänger über Brief benachrichtigen (Klingel für Var B)
sem_t klingelSignale[100];

// Semaphor Signal mit dem die Empfänger den Zustellern den Erhalt von Briefen bestätigen (Für Var B)
sem_t empfangsSignale[100];

// Kapazität der Zustellerlager
static const int zustellerKapazitaet = 10;

// initialisiert Zustellerlager, Lager Mutexe, Lager Semaphore und Zustellerthreads
void zustellerErstellen();

// initialisiert Briefkästen, Briefkasten Mutexe, Var B Semaphore und Empfängerthreads
void empfaengerErstellen();

// Briefkasten max Größe
const int kapazitaetBriefkasten = 1000;

// Inhalt auf Konsole ausgeben und Briefe aus Liste entfernen
void zumBriefkasten(int nr);

// Variante A
// wartet mit usleep und geht dann zumBriefkasten()
void randomWarten(int nr);

// Variante B
// wartet auf Klingel Signal, setzt Empfangsbestätigungssignal
// geht zumBriefkasten()
void klingelWarten(int nr);

// Erstellt Briefkasten (struct Brief liste) und trägt pointer in liste in main ein
void* initEmpfaenger(void* arg);

// erstellt liste mit Kapazität und setzt Signale
// Loop der mit start() wartet und dann je nach Variante verteilen() aufruft
// speichert Pointer zur eigenene Lagerliste in Liste in main ab
void* initZusteller(void* arg);

//liste mit pointer zu gelagerten Briefen mit Kapazität kapazitaet

//loop in dem signalvar überprüft wird, wenn signal, dann verteilen.
void startZusteller(int nr);

// Wirft Brief bei Empfänger ein, 
int einwurf(unsigned int id, int briefNr, int nr);

// hochzählende Briefnachricht
unsigned long long briefNachricht = 0;

// hochzählender nächster Empfänger für das rstellen von Briefen
int naechsterEmpfaenger = 0;

// erzeugt brief mit stringinhalt = aufsteigende Zahl und EmpfängerThreadID aus liste in main. ID nacheinander bis Ende, dann wieder von vorn
void brief_erzeugen(int naechsterZusteller, int i);

// wartet r mit usleep und ruft brief_erzeugen auf und verteilt Brief an Zustellerthread aus Liste
// in main bis Liste voll. Dann wird das dem Zusteller signalisiert.
// geht dann an nächsten Thread weiter, bis Ende der Liste, dann wieder von vorne
// Wenn alle Zusteller voll im festen Intervall (paar mal pro Sekunde) beim nächsten Anfragen bis einer gefunden ist der frei ist
void startLogistik();

void empfaengerErstellen(){
    // Briefkasten empfaengerID mit 0 initialisieren ( 0 = "NULL" Brief )
    for (int i = 0; i < anzahlEmpfaenger; i++) {
            for (int j = 0; j < 10; j++){
                empfaengerBriefkasten[i][j].empfaengerID = 0;
            }
        }
    pthread_t thread_id;
    for (int i = 0; i < anzahlEmpfaenger; i++) {
        pthread_mutex_init(&briefkastenSignale[i], NULL);
        sem_init(&klingelSignale[i],0,0);
        sem_init(&empfangsSignale[i],0,0);
        pthread_create(&thread_id, NULL, &initEmpfaenger, &i);
        empfaengerID[i] = thread_id;
        // warten, damit i sich nicht verändert, bis Empfänger initialisiert ist
        usleep(200);
    }
}

void zustellerErstellen(){
    // Zusteller Lager empfaengerID mit 0 initialisieren ( 0 = "NULL" Brief )
    for (int i = 0; i < anzahlZusteller; i++) {
        for (int j = 0; j < 10; j++){
            zustellerLager[i][j].empfaengerID = 0;
        }
    }
    pthread_t thread_id;
    for (int i = 0; i < anzahlZusteller; i++) {
        sem_init(&lagerSem[i],0,0);
        pthread_mutex_init(&lagerMutex[i], NULL);
        pthread_create(&thread_id, NULL, &initZusteller, &i);
        zustellerID[i] = thread_id;
        // warten, damit i sich nicht verändert, bis Empfänger initialisiert ist
        usleep(200);
    }
}

// Inhalt auf Konsole ausgeben und Briefe aus Liste entfernen
void zumBriefkasten(int nr){
    //printf("Empfaenger %i pre Briefkasten Mutex lock\n", nr);
    pthread_mutex_lock(&briefkastenSignale[nr]);
    //printf("Empfaenger %i post Briefkasten Mutex lock\n", nr);
    //for Schleife bis leere Stelle im Briefkasten erreicht ist
    for (int i = 0; empfaengerBriefkasten[nr][i].empfaengerID != 0; i++){
        printf("Briefinhalt: %llu\n", empfaengerBriefkasten[nr][i].inhalt);
        empfaengerBriefkasten[nr][i].empfaengerID = 0;
    }
    //printf("Empfaenger %i pre Briefkasten mutex unlock\n", nr);
    pthread_mutex_unlock(&briefkastenSignale[nr]);
    //printf("Empfaenger %i post Briefkasten Mutex unlock\n", nr);
}

// Variante A
// wartet mit usleep und geht dann zumBriefkasten()
void randomWarten(int nr){
    int random;
    while (1) {
        srand(time(0));
        random = rand() % (upperBound);
        usleep(random);
        zumBriefkasten(nr);
    }
}

// Variante B
// wartet auf Klingel Signal, setzt Empfangsbestätigungssignal
// geht zumBriefkasten()
void klingelWarten(int nr){
    while (1) {
        //printf("Empfaenger %i pre Klingel Sem\n", nr);
        sem_wait(&klingelSignale[nr]);
        //printf("Empfaenger %i post Klingel Sem, pre zum Briefkasten\n", nr);
        zumBriefkasten(nr);
        //printf("Empfaenger %i post zum Briefkasten, pre Empfans Sem Sem\n", nr);
        sem_post(&empfangsSignale[nr]);
        //printf("Empfaenger %i post Empfangs Sem\n", nr);
    }
}

// Erstellt Briefkasten (struct Brief liste) und trägt pointer in liste in main ein
// Argument ist for index i Pointer, deswegen Wert kopieren
void* initEmpfaenger(void* nr){
    int mynr = *(int *)nr;
    //printf("Empfaenger %i erstellt\n", mynr);
    if (variante[0] == 'A'){
        //printf("randomWarten\n");
        randomWarten(mynr);
    } else {
        //printf("klingelWarten\n");
        klingelWarten(mynr);
    }
    return NULL;
}

// erstellt liste mit Kapazität und setzt Signale
// Loop der mit start() wartet und dann je nach Variante verteilen() aufruft
// speichert Pointer zur eigenene Lagerliste in Liste in main ab
// Argument ist for index i Pointer, deswegen Wert kopieren
void* initZusteller(void* nr){
    int mynr = *(int *)nr;
    //printf("Zusteller %i erstellt\n", mynr);
    startZusteller(mynr);
    return NULL;
}

//loop in dem signalvar überprüft wird, wenn signal, dann verteilen.
void startZusteller(int nr){
    unsigned int id;
    int random;
    int index;
    while (1) {
        //printf("Zusteller %i pre Sem\n", nr);
        sem_wait(&lagerSem[nr]);
        //printf("Zusteller %i post Sem\n", nr);
        pthread_mutex_lock(&lagerMutex[nr]);
        //printf("Zusteller %i post Mutex lock\n", nr);
        for (int i = 0; i < zustellerKapazitaet; i++){
            //printf("Zusteller %i for i: %i\n", nr, i);
            id = zustellerLager[nr][i].empfaengerID;
            //printf("Zusteller %i in slot %i gelesene id %u, Briefinhalt: %llu\n", nr, i, id, id2);
            srand(time(0));
            random = rand() % (upperBound);
            //printf("Zusteller %i random warten: %i\n", nr, random);
            usleep(random);
            index = einwurf(id, i, nr);
            if (variante[0] == 'B') {
                //printf("Zusteller %i pre Klingel Sem\n", nr);
                sem_post(&klingelSignale[index]);
                //printf("Zusteller %i post Klingel Sem, pre Empfangs Sem\n", nr);
                sem_wait(&empfangsSignale[index]);
                //printf("Zusteller %i post Empfangs Sem\n", nr);
            }
        }
        //printf("Zusteller %i pre Mutex unlock\n", nr);
        pthread_mutex_unlock(&lagerMutex[nr]);
        //printf("Zusteller %i post Mutex unlock\n", nr);
    }
}

// Wirft Brief bei Empfänger ein
int einwurf(unsigned int id, int briefNr, int nr){
    int index;
    for (int i = 0; i < anzahlEmpfaenger; i++) {
        //printf("Zusteller %i sucht id %u\n", nr, id);
        if (empfaengerID[i] == id) {
            index = i;
            break;
        }
    }
    pthread_mutex_lock(&briefkastenSignale[index]);
    //printf("Zusteller %i post Briefkasten lock\n", nr);
    for (int i = 0; i < kapazitaetBriefkasten; i++) {
        if (empfaengerBriefkasten[index][i].empfaengerID == 0) {
            empfaengerBriefkasten[index][i] = zustellerLager[nr][briefNr];
            //printf("Zusteller %i Brief bei Empfaenger %i mit ID %u in index %i eingeworfen\n", nr,index,id,i);
            zustellerLager[nr][briefNr].empfaengerID = 0;
            break;
        }
    }
    pthread_mutex_unlock(&briefkastenSignale[index]);
    return index;
}

// erzeugt brief mit stringinhalt = aufsteigende Zahl und EmpfängerThreadID aus liste in main. ID nacheinander bis Ende, dann wieder von vorn
void brief_erzeugen(int naechsterZusteller, int i){
    zustellerLager[naechsterZusteller][i].inhalt = briefNachricht;
    zustellerLager[naechsterZusteller][i].empfaengerID = empfaengerID[naechsterEmpfaenger];
    //printf("Brief erzeugt bei Zusteller %i and index %i mit Nachricht %llu für Empfaenger %i\n", naechsterZusteller,i,briefNachricht,naechsterEmpfaenger);
    briefNachricht++;
    naechsterEmpfaenger = (naechsterEmpfaenger + 1) % anzahlEmpfaenger;
}

// wartet r mit usleep und ruft brief_erzeugen auf und verteilt Brief an Zustellerthread aus Liste
// in main bis Liste voll. Dann wird das dem Zusteller signalisiert.
// geht dann an nächsten Thread weiter, bis Ende der Liste, dann wieder von vorne
void startLogistik(){
    for (int naechsterZusteller = 0; ; naechsterZusteller = (naechsterZusteller + 1) % anzahlZusteller) { 
        pthread_mutex_lock(&lagerMutex[naechsterZusteller]);
        for (int i = 0; i < zustellerKapazitaet; i++){
            usleep(wartezeit);
            brief_erzeugen(naechsterZusteller, i);
        }
        pthread_mutex_unlock(&lagerMutex[naechsterZusteller]);
        sem_post(&lagerSem[naechsterZusteller]);
    }
}

int main(int argc, char** argv) {
    
    printf("Anzahl Zusteller(1-99): ");
    scanf("%d", &anzahlZusteller);
    printf("Anzahl Empfanger(1-99): ");
    scanf("%d", &anzahlEmpfaenger);
    printf("Warteperiode zwischen Erstellung von Briefen in Mikrosekunden (0 - 99 999): ");
    scanf("%d", &wartezeit);
    upperBound = wartezeit * 3 + 1;
    printf("Variante A: Empfaenger schauen periodisch in den Briefkasten\n");
    printf("Variante B: Empfaenger muessen benachrichtigt werden und Zusteller warten\n");
    printf("Variante 'A' oder 'B'?: ");
    scanf("%9s", variante);
    zustellerErstellen();
    empfaengerErstellen();
    startLogistik();
    
    return (EXIT_SUCCESS);
}

