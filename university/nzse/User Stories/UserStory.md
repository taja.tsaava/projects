# User Stories -> E-Mobility

--- 
### Raum 1:  Nutzer/Autofahrer
Als Nutzer will ich... 
1. Probleme melden / Ansprechpartner
2. divers filtern können (Betreiber/Herstellerfilter).
3. Ladestationen in der Nähe anzeigen lassen
4. freie Plätze anzeigen lassen
5. verfügbare Betreiber anzeigen lassen
6. Kosten angezeigt bekommen
7. Ladegeschwindigkeit anzeigen lassen
8. Steckertypen anzeigen lassen
9. Infos über Ladevorgang anzeigen lassen
10. einen Routenplaner
11. eine bestimme Adresse eingeben können
12. wissen, wann mein Auto vollgeladen ist
13. Öffnungszeiten angezeigt bekommen
14. Akkustand immer wissen
15. Wo mein Auto steht
16. Favoriten speichern
---
### Raum 2:  Servicepersonal
Als Servicepersonal möchte ich...
1. auf das Interface der Ladefunktion remote zugreifen können, um die Funktion bequemer kontrollieren zu können.
2. die geographische Lage der Ladestation auf einer Karte einsehen können, um die Wartung durch den nächstgelegenen Mitarbeiter garantieren zu können.
3. eine Liste mit dem Funktionszustand (defekt/bereit) sehen können, um einen Überblick über das gesamte Netz zu haben.
4. spezifische Informationen zur Funktion der Ladesäule einsehen können, um das Problem möglichst präzise analysieren zu können.
5. den Status der Ladestation (defekt/funktionsbereit) im System ändern können, um Ladestationen wieder ans Netz geben zu können.
6. unterschiedlich detaillierte Informationen vom Nutzer erhalten können, um das Problem analysieren zu können.
7. auslesen können wer für wie viel getankt hat, falls es Probleme gibt
8. ein Logbuch der bisherigen Fehler+Reperaturen einsehen können, um andere Fehler auszuschließen bzw. zu entdecken.

---
### Raum 3: Entwickler

Entwickler:
Als Entwickler möchte ich...
1. dass user sich nicht in der app verlaufen sollten(höchstens 3 klicks für das gewünschte ergebnis)
2. Erweiterbarkeit (Modularer aufbau)
3. einfache Wartbarkeit (kommentierter, dokumentierter code)
4. Trennung von app zu Daten(Ladesäulen)
5. dass Fehler leicht auslesbar sind -> Logs
6. nutzer-Statistiken extrahierbar sind
7. den Status von Ladesäulen persistent speichern
8. möglichst leichtgewichtige Kommunikation zwischen Server und Gerät
9. Cache auf dem mobilen Endgerät zum speichern von Daten

---
### Raum 4:  Projektleiter
Ich als Projektleiter möchte…

1. dass Softwareprodukt-Qualitätsstandards eingehalten werden.

Abgleichen mit Kriterien:
* Portierbarkeit, Wartbarkeit, Sicherheit, Zuverlässigkeit, Funktionalität, Performanz / Effizienz, Kompatibilität, Benutzbarkeit
* Wie können die Standards eingehalten werden?

2. Startbesprechung über Anforderungen und Standards
3. Expertise der Mitarbeiter sicherstellen
4. 2 wöchige Sprints, abgleichen der Ziele mit dem Auftraggeber
5. Vorgehensmodelle / Vorgehensrichtlinien
6. Pair programming, Test First, UML/Modellierung
7. dass Prozess-Qualitätsstandards eingehalten werden.
8. Einhaltung von Entwicklungsstandards (code conventions)
9. Systematische Verbesserung des Entwicklungsprozesses
10. aktive Suche nach Schwachstellen im Entwicklungsprozess
11. Einbeziehung von Normen und Standards, die einen „ordentlichen Prozess“ sicherstellen
12. dass der Abgabetermin eingehalten wird.
13. Regelmäßige Prüfung des Arbeitsfortschritts / Fortschrittsabgleich im Projektterminplan
15. dass die Vorgaben des Auftraggebers eingehalten werden.
16. Regelmäßige Prüfung der Funktionalität
17. dass eine gute Atmosphäre im Projektteam herrscht und Probleme offen und oft kommuniziert werden.
18. Bezugspersonen, richtig Feedback geben, vertrauensvolle Atmosphäre schaffen
19. innerhalb der Budgetgrenzen bleiben.
20. Keine überflüssige Funktionen einbauen
21. dass Alle Arbeitsschritte ordnungsgemäß dokumentiert werden
    
