# Use Case -> E-Mobility

## Ladestation für Auto Suchen
---
### Bereich
Bei diesem Anwendungsfall geht es spezifisch um das Feature der App, eine Ladestation für den Nutzer der App zu finden.


### Ziele
Es soll eine Ladestation für den User gefunden werden. Dabei kann der User bei der Filtereinstellung aussuchen nach welchen Kriterien ihm/ihr die Ladestationen aufgelistet werden sollen, z.B: Nähe, Preis, Betreiber, Anschlüsse/Einrichtung, etc. Es soll aber auch möglich sein für den User Ladestationen, die schon einmal besucht wurden schnell wiederzufinden, bspw. durch die Menüoptionen "Favoriten" oder "Zuletzt Besucht".


### Beteiligte & Anwendungskontext 
Wer nutzt das System unter welchen Bedingungen/in welcher Umgebung für welches Ziel?

Der User XY benutzt die E-Mobility-App, um schnell und einfach eine Ladestation in seiner Nähe finden zu können.*

Der User XY befindet sich momentan zu Hause und möchte bei Gelegenheit sein Auto an der nächstmöglichen Ladestation aufladen. Dafür öffnet dieser die App und kriegt direkt unter seiner Map, die am nächstgelegensten Ladestationen anzeigt. Der User XY wählt nun die erste Ladestation aus, die ihm auf dem Screen in der App angezeigt wird. Jetzt werden ihm die Adresse und diverse weitere Informationen zu der Tankstelle beim ausklappen des Anzeigepfeils (auf der rechten Seite) anzeigt. Der User XY beschließt nun zu dieser Ladestation hinzufahren und wählt für eine Navigation die Routenplan-Option aus. Jetzt wird dem User eine Karte und ein Weg mit Navigation gezeigt und der User XY kann sicher losfahren. 

Desweiteren kann der User XY jederzeit nach dem Ankommen den Status der Batterie und den Standort live checken. Dies erfolgt durch die "Auto Informationen" Option in dem Menü der App.


### Weitere Betroffene/Stakeholder
Wer ist neben dem direkten Anwender betroffen? Welche Interessen 
gibt es?

Sowohl das Sevicepersonal als auch die Entwickler und die Projektleiter sind Stakeholder in diesem Szenario. 
Sollte es im Allgemeinen zu Schwierigkeiten bei der Bedinung der Ladestation/App geben, dann kann das Servicepersonal kontaktiert werden. Dieses kann dem User XY unterstützen durch den remoten Zugriff auf das Interface der Ladestation und den spezifischen Informationen zur Funktion der Ladesäule, welche eingesehen werden können, um das Problem möglichst präzise analysieren zu können. Dabei werden auch unterschiedlich detaillierte Informationen vom User XY benötigt, um das Problem analysieren zu können. 

Auch der Entwickler ist hier involviert, da dieser dafür sorgt, dass der Status von den Ladesäulen persistent gespeichert wird und der Cache auf dem mobilen Endgerät, in dem Fall das Gerät vom User XY, zum speichern von Daten verwendet wird, da die Daten für das Servicepersonal zur Fehlerbehebung eine große Rolle spielen können. Zu dem sollte es möglichst eine  leichtgewichtige Kommunikation zwischen Server und Gerät geben, sodass alle beteiligten schnell an die gewünschten Informationen rankommen können.


### Hauptakteur 
Wer ist der Hauptakteur? Was löst den Anwendungsfall aus?

In diesem konkreten Fall ist User XY der Hauptakteur und Auslöser des Anwendungfalles, da dieser aktiv auf die Funktionen der E-Mobility-App zugreifen möchte und diese auch benutzt.


### Voraussetzung/Vorbedingung
Welchen Ausgangszustand der Umgebung benötigt der 
Anwendungsfall?

Der Anwendungsfall setzt voraus, das der User XY ein Elektro/Hybrid-Auto besitzt, welches an der Ladestation aufgeladen werden kann. Zu dem muss der User XY auch sein mobiles Endgerät mit der darauf instalierten App besitzen und Internetzugriff haben.


### Nachbedingung im Erfolgsfall/im Fehlerfall
Welchen Systemzustand garantiert der Anwendungsfall im Erfolgsfall 
und im Fehlerfall?

Erfolgsfall: Der User XY bekommt seine Ladestation angezeigt, zu die er dann hinfahren kann. 

Fehlerfall: Die App konnte dem User XY keine Ladestation anzeigen und oder keine Informationen über diese Ausgeben, wodurch die Routenplanung wie auch die Überwachung vom Batteriestand des Autos bzw. auch der Zustand der Ladesäule ausbleibt.




*Andere Szenariomöglichkeiten währen bezogen auf andere Filtereigenschaften in der App wie: Preis, Betreiber, Anschlüsse/Einrichtung, etc.
