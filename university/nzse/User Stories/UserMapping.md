# User Mapping
---
Sprints| Feature 1 | Feature 2 | Feature 3 | Feature 4 | Feature 5 | Feature 6 | Feature 7 | Feature 8 | Feature 9 |
-------| --------  | --------  | --------  | --------  | --------  | --------  | --------- | --------- | --------- |
1      | Ladestationen in der Nähe anzeigen lassen (N)| Informationen zu Ladestation ausgeben lassen (N, S, E)| |     | unterschiedlich detaillierte Informationen vom Nutzer erhalten können, um das Problem analysieren zu können (S)| Filter für Suche (Betreiber/Herstellerfilter) (N)|   |wissen, wann mein Auto vollgeladen ist (N)| erleichterte Fehlerauslese -> Logs (E)|
|| spezifische Informationen zur Funktion der Ladesäule einsehen können, um das Problem möglichst präzise analysieren zu können (S)|     den Status von Ladesäulen persistent speichern (E) |  |     | nutzer-Statistiken extrahierbar sind (E)|  |   |Akkustand immer wissen (N)| ein Logbuch der bisherigen Fehler+Reperaturen einsehen können, um andere Fehler auszuschließen bzw. zu entdecken (S)|
2      |   |freie Plätze anzeigen lassen (N)| Favoriten speichern (N)| Routenplaner (N)| |     |     |auslesen können wer für wie viel getankt hat, falls es Probleme gibt (S)|
||  |verfügbare Betreiber anzeigen lassen (N)|     | eine bestimme Adresse eingeben können (N)|     |     |     ||
||  |Kosten angezeigt bekommen (N)|     | Standort vom Auto (N)|     |     |     ||
||  |Ladegeschwindigkeit anzeigen lassen (N)|     |     |     |     |     ||
||  |Steckertypen anzeigen lassen (N)|     |     |     |     |     ||
||  |Infos über Ladevorgang anzeigen lassen (N)|     |     |     |     |     ||
3      | |Öffnungszeiten angezeigt bekommen (N)|     |     |     |     |     ||
4      |     | Geographische Lage der Ladestation auf einer Karte einsehen können, um die Wartung durch den nächstgelegenen Mitarbeiter garantieren zu können (S)|     |     |     |     | Probleme melden / Ansprechpartner (N)|
|    |     | eine Liste mit dem Funktionszustand (defekt/bereit) sehen können, um einen Überblick über das gesamte Netz zu haben (S)|     |     |     |     | |
|    |     | spezifische Informationen zur Funktion der Ladesäule einsehen können, um das Problem möglichst präzise analysieren zu können (S)|     |     |     |     | |





Legende:
N = Nutzer
S = Servicepersonal
E = Entwickler
(P = Projektleiter)


