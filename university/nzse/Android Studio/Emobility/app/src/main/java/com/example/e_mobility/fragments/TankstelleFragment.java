package com.example.e_mobility.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_mobility.Adapter;
import com.example.e_mobility.ChargingStation;
import com.example.e_mobility.R;
import com.example.e_mobility.StationManager;

import java.util.ArrayList;
import java.util.List;

public class TankstelleFragment extends Fragment {
    private Adapter adapter;
    private List<ChargingStation> tankstellenList;
    private RecyclerView recyclerView;
    //private RecyclerView.ViewHolder recyclerViewHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tankstelle, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        tankstellenList = new ArrayList<>();
        tankstellenList = StationManager.getInstance().getStation_list();



        adapter = new Adapter(tankstellenList);
        recyclerView.setAdapter(adapter);

        final Button favButton = view.findViewById(R.id.favoriteBtn);
        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //  StationManager.getInstance().addStationToFavourite();
            }
        });

        //recyclerViewHolder = new RecyclerView.ViewHolder();
        //recyclerViewHolder.itemView = view.findViewById(R.id.text_view1);

        return view;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }


    public boolean onCreateOptionsMenu(Menu menu) {

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
