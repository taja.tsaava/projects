/*package com.example.e_mobility;
import android.app.AppComponentActivity;
import android.app.AppComponentFactory;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppComponentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);

        findViewById(R.id.imageMenu).setOnClickListener(new View.OnClickListener()){
            @Override
                public void onClick(View view){
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        }
    }
}*/
package com.example.e_mobility;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.backendless.Backendless;
import com.backendless.exceptions.BackendlessException;
import com.example.e_mobility.fragments.HelpFragment;
import com.example.e_mobility.fragments.MapFragment;
import com.example.e_mobility.fragments.RepTankstelleFragment;
import com.example.e_mobility.fragments.ServiceProfilFragment;
import com.example.e_mobility.fragments.SettingsFragment;
import com.google.android.material.navigation.NavigationView;


public class ServiceMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;

    private static boolean isService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_main);

        isService = true;

        Toolbar toolbar = findViewById(R.id.toolbar_serv);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.service_drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        startThread();
    }


        //rightSpot?
        public void startThread() {
            Runnable downloadService = new DownloadService();
            Thread downloadThread = new Thread(downloadService);
            downloadThread.start();
            try {
                downloadThread.join();
            } catch (InterruptedException ex) {

            }
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_more_menu, menu);
        return true;
    }

    /*************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.more:
                Toast.makeText(this, "More", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.profil:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ServiceProfilFragment()).commit();
                return true;
            case R.id.logout:
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                try
                {
                    // now log out:
                    Backendless.UserService.logout();
                }
                catch( BackendlessException exception )
                {
                    System.out.println(exception.getCode());
                }
                System.out.println("Successfully logged out from Backendless");
                startActivity(new Intent(ServiceMainActivity.this, Login.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
                case R.id.nav_map:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MapFragment()).commit();
                    break;
            case R.id.nav_tankstellen_rep:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RepTankstelleFragment()).commit();
                break;

            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SettingsFragment()).commit();
                break;
            case R.id.nav_help:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HelpFragment()).commit();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public static boolean isService()
    {
        return isService;
    }
}





