package com.example.e_mobility.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_mobility.Adapter;
import com.example.e_mobility.ChargingStation;
import com.example.e_mobility.R;
import com.example.e_mobility.StationManager;

import java.util.ArrayList;
import java.util.List;

public class FavoritenFragment extends Fragment {
    private Adapter adapter;
    private List<ChargingStation> favList;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favoriten, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        favList = new ArrayList<>();
        favList = StationManager.getInstance().get_Fav_Station_list();

        adapter = new Adapter(favList);
        recyclerView.setAdapter(adapter);

        //recyclerViewHolder = new RecyclerView.ViewHolder();
        //recyclerViewHolder.itemView = view.findViewById(R.id.text_view1);

        return view;
    }
}
