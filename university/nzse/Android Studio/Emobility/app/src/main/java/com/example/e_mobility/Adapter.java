package com.example.e_mobility;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> implements Filterable {
    private List<ChargingStation> exampleList;
    private List<ChargingStation> exampleListFull;
    private List<ChargingStation> tankstellenList;
    private ChargingStation exampleStation;

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewLocation;
        TextView textViewStreet;
        TextView textViewNumber;

        ViewHolder(View itemView) {
            super(itemView);
            //  imageView = itemView.findViewById(R.id.image_view);
            textViewLocation = itemView.findViewById(R.id.text_view_location);
            textViewStreet = itemView.findViewById(R.id.text_view_street);
            textViewNumber = itemView.findViewById(R.id.text_view_number);
        }
    }

    public Adapter(List<ChargingStation> exampleList) {
        this.exampleList = exampleList;
        exampleListFull = new ArrayList<>(exampleList);
    }

    public Adapter(ChargingStation exampleStation) {
        this.exampleStation = exampleStation;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_tankstelle,
                parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChargingStation currentItem = exampleList.get(position);

        // holder.imageView.setImageResource(currentItem.getImageResource());
        holder.textViewLocation.setText(currentItem.getLocation());
        holder.textViewStreet.setText(currentItem.getStreet());
        holder.textViewNumber.setText(currentItem.getNumber());
    }

    @Override
    public int getItemCount() {
        return exampleList.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ChargingStation> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(exampleListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ChargingStation item : exampleListFull) {
                    if (item.getAdditional().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            exampleList.clear();
            exampleList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}