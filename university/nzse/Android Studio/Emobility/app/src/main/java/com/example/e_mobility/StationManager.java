package com.example.e_mobility;
/**
 * Wrapper for data
 * @author Jörg Quick, Tilo Steinmetzer
 * @version 1.0
 */

import java.util.ArrayList;

public class StationManager {
    /**
     * Main container for stations (static to get data from all activities / fragments).
     */
    private static StationManager stationManager = new StationManager();

    private static ArrayList<ChargingStation> station_storage;

    private ArrayList<ChargingStation> favourite_Stations = new ArrayList<ChargingStation>();
    private ArrayList<ChargingStation> reportedStations = new ArrayList<>();
    private ArrayList<ChargingStation> nearbyStations;

    //Object Stuff ----------------------------------

    /**
     * StationManager Standard Constructor
     *
     */
    private StationManager() {
    }

    /**
     * ensure that theres only one ManagerObject
     * @return Instance of ManagerObject
     */
    public static StationManager getInstance() {
        return stationManager;
    }

    //CRUD like Methods ----------------------------------

    /**
     * adds Station to Favourites
     * @param newFav the new Favourite Station
     */
    public void addStationToFavourite(ChargingStation newFav)
    {
        favourite_Stations.add(newFav);
    }

    /**
     * adds Station to Reporteds
     * @param newReported the new Reported Station
     */
    public void addReportedStation(ChargingStation newReported)
    {
        reportedStations.add(newReported);
    }

    /**
     * removes the given Station from the given List
     * @param id of the Station to remove
     * @param stations list where the station should get removed
     */
    public void removeStationsById(int id, ArrayList<ChargingStation> stations)
    {
        for(int i = 0; i < stations.size(); i++) {
            if(stations.get(i).getId() == id){
               stations.remove(stations.get(i));
                System.out.println("Objekt wurde repariert");
            }
        }
    }

    /**
     * finds Station with the Id
     * @param id of the Station searched
     * @param stations list to search the Station
     * @return return ChargingStation Object if found, if not it returns null
     */
    public ChargingStation findStationById(int id, ArrayList<ChargingStation> stations)
    {
        for(int i = 0; i < stations.size(); i++) {
            if(stations.get(i).getId() == id){
                return stations.get(i);
            }
        }
        return null;
    }

    //Calculations ----------------------------------

    /**
     * searches Stations near to the User
     * @param userLat Users Latitude
     * @param userLon Users Longidude
     * @param radius to Search
     * @return stationList with all nearby Stations
     */
    public ArrayList<ChargingStation> getStationsbyRadius(double userLat, double userLon, double radius){
        nearbyStations = new ArrayList<ChargingStation>();
        nearbyStations.clear();

        for(int i = 0; i < getStation_list().size(); i++) {
            //Lat and Lon on tested Stations
            double lat = getStation_list().get(i).getLat();
            double lon = getStation_list().get(i).getLon();

            //If the Station is in Radius
            if (calcDistance(userLat, userLon, lat, lon) < radius) {
                nearbyStations.add(getStation_list().get(i));
            }
        }
        return nearbyStations;
    }

    /**
     * calculates Stations in Distance
     * @param latA Users Latitude
     * @param lonA Users Longitude
     * @param latB Objects Latitude
     * @param lonB Objects Longitude
     * @return distance between User and Object
     */
    public double calcDistance(double latA, double lonA, double latB, double lonB){
        //https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
        double theta = lonA - lonB;
        double dist = Math.sin(Math.toRadians(latA))
                * Math.sin(Math.toRadians(latB))
                + Math.cos(Math.toRadians(latA))
                * Math.cos(Math.toRadians(latB))
                * Math.cos(Math.toRadians(theta)); //Calculate Distance
        dist = Math.toDegrees(Math.acos(dist));
        dist = dist * 60 * 1.1515;

        dist = dist * 1.609344;

        return dist;
    }

    //Getter and Setter ----------------------------------

    /**
     * Setter for container.
     * @param station_list of station objects
     */
    public static void setStation_list(ArrayList<ChargingStation> station_list) {
        station_storage = station_list;
    }

    /**
     * Getter for station container.
     * @return arrayList with station objects
     */
    public static ArrayList<ChargingStation> getStation_list() {
        return station_storage;
    }

    /**
     * Getter for station container.
     * @return arrayList with station objects
     */
    public static ArrayList<ChargingStation> get_Fav_Station_list() {
        return getInstance().favourite_Stations;
    }

    /**
     * Getter for station container.
     * @return arrayList with station objects
     */
    public static ArrayList<ChargingStation> get_Rep_Station_list() {
        getInstance().reportedStations.add(station_storage.get(10861));
        return getInstance().reportedStations;
    }
}