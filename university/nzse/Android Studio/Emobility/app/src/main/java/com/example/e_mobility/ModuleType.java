package com.example.e_mobility;
/**
 * ModuleType enum
 * @author Jörg Quick, Tilo Steinmetzer
 * @version 1.0
 */

import com.google.gson.annotations.SerializedName;

public enum ModuleType {
    @SerializedName("Normalladeeinrichtung") STANDARD,
    @SerializedName("Schnellladeeinrichtung") FAST_CHARGING
}

