package com.example.e_mobility;

import android.app.Application;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;


public class ApplicationClass extends Application {
    public static final String APPLICATION_ID="FC1F2387-9938-857A-FF3C-C70A4155D300";
    public static final String API_KEY="0F82F561-15B8-4F8D-81AE-50F2ADCB9B9C";
    public static final String SERVER_URL="https://eu-api.backendless.com";

    public static BackendlessUser user;

    @Override
    public void onCreate(){
        super.onCreate();

        Backendless.setUrl( SERVER_URL );
        Backendless.initApp( getApplicationContext(),
                APPLICATION_ID,
                API_KEY);
    }
}
