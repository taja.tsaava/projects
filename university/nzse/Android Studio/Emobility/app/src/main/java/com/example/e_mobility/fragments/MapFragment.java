package com.example.e_mobility.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_mobility.Adapter;
import com.example.e_mobility.ChargingStation;
import com.example.e_mobility.R;
import com.example.e_mobility.ServiceMainActivity;
import com.example.e_mobility.StationManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;


public class MapFragment extends Fragment {

    //Map and Radius
    GoogleMap mMap;
    int radius = 5;

/*    private CoordinatorLayout mBottomSheetLayout;
    private BottomSheetBehavior sheetBehavior;*/

    //Container for Fragments
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        View view2 = inflater.inflate(R.layout.bottom_sheet_info, container, false);

        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.google_map);

        //Map in Fragment
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                mMap = googleMap;
                addMarkers();

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(@NonNull Marker marker) {
                        showBottomSheetDialog(marker, view2);
                        return false;
                    }
                });
            }

        });

        return view;
    }

    //UserLocation and near ChargingStations
    private void addMarkers() {
        mMap.clear();

        //Sets Users location
        LatLng ownPos = new LatLng(50.11121329678364, 8.681315734930514);
        mMap.addMarker(new MarkerOptions().position(ownPos).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Dein Standort"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ownPos, 17));

        //Tests if User is service to change between nearby and reported Station Markers
        if(ServiceMainActivity.isService()){
            //Adds reported Stations for Service User
            ArrayList<ChargingStation> reportedStations = StationManager.getInstance().get_Rep_Station_list();

            for (int i = 0; i < (reportedStations.size()); i++) {
                //set Marker for all NearbyStations
                LatLng newMarker = new LatLng(reportedStations.get(i).getLat(), reportedStations.get(i).getLon());
                Marker curMarker = mMap.addMarker(new MarkerOptions().position(newMarker).title(reportedStations.get(i).toString()));
                curMarker.setTag(reportedStations.get(i));
            }
        } else {
            //Adds nearby Stations for normal User
            ArrayList<ChargingStation> nearbyStations = StationManager.getInstance().getStationsbyRadius(ownPos.latitude, ownPos.longitude, radius);

            for (int i = 0; i < nearbyStations.size(); i++) {
                //set Marker for all NearbyStations
                LatLng newMarker = new LatLng(nearbyStations.get(i).getLat(), nearbyStations.get(i).getLon());
                Marker curMarker = mMap.addMarker(new MarkerOptions().position(newMarker).title(nearbyStations.get(i).toString()));
                curMarker.setTag(nearbyStations.get(i));
            }
        }
    }

        private void showBottomSheetDialog(Marker marker, View view2) {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
            bottomSheetDialog.setContentView(R.layout.bottom_sheet_info);

            ChargingStation curStation = (ChargingStation) marker.getTag();  //curStation = object we need for the Information Screen

            //Layout of the Screen, with Informations, report und favourite button
            Button warningbtn = bottomSheetDialog.findViewById(R.id.warningBtn);
            Button favouritebtn = bottomSheetDialog.findViewById(R.id.favoriteBtn);

            TextView operator = bottomSheetDialog.findViewById(R.id.text_view_location);
            TextView street = bottomSheetDialog.findViewById(R.id.text_view_street);
            TextView number = bottomSheetDialog.findViewById(R.id.text_view_number);

            /*mBottomSheetLayout = view2.findViewById(R.id.fragment_map);
            sheetBehavior = BottomSheetBehavior.from(mBottomSheetLayout);
            sheetBehavior.setDraggable(true);*/

            //Sets Information about the current Station to Sheet
            operator.setText(curStation.getOperator());
            street.setText(curStation.getStreet());
            number.setText(curStation.getNumber());

            warningbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view2) {
                    Toast t;
                    System.out.println(" warning button wurde geklickt");
                    //Tests if the user is a Serviceemployee to add or remove the reported Stations correctly
                    if(ServiceMainActivity.isService()){
                        StationManager.getInstance().removeStationsById(curStation.getId(), StationManager.getInstance().get_Rep_Station_list());
                    } else {
                        StationManager.getInstance().addReportedStation(curStation);
                        t = Toast.makeText(MapFragment.this.getContext(),
                                "Station gemeldet!", Toast.LENGTH_LONG);
                        t.show();
                    }
                }
            });

            favouritebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view2) {
                    Toast t;
                    System.out.println("fav button wurde geklickt");

                    //tests if the Current Station is already a Favourite, if not it removes the Stations from the list
                    if(StationManager.getInstance().findStationById(curStation.getId(), StationManager.getInstance().get_Fav_Station_list()) == null)
                    {
                        StationManager.getInstance().addStationToFavourite(curStation);

                        t = Toast.makeText(MapFragment.this.getContext(),
                                "Zu Favoriten hinzugefügt", Toast.LENGTH_LONG);
                        t.show();
                    } else if(StationManager.getInstance().findStationById(curStation.getId(), StationManager.getInstance().get_Fav_Station_list()).getId() == curStation.getId()){
                        StationManager.getInstance().removeStationsById(curStation.getId(), StationManager.getInstance().get_Fav_Station_list());

                        t = Toast.makeText(MapFragment.this.getContext(),
                                "von Favouriten entfernt", Toast.LENGTH_LONG);
                        t.show();
                    }
                }
            });

            bottomSheetDialog.show();
        }
}













