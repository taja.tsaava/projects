/*package com.example.e_mobility;
import android.app.AppComponentActivity;
import android.app.AppComponentFactory;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppComponentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);

        findViewById(R.id.imageMenu).setOnClickListener(new View.OnClickListener()){
            @Override
                public void onClick(View view){
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        }
    }
}*/
package com.example.e_mobility;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.backendless.Backendless;
import com.backendless.exceptions.BackendlessException;
import com.example.e_mobility.fragments.FavoritenFragment;
import com.example.e_mobility.fragments.HelpFragment;
import com.example.e_mobility.fragments.LastVisitedFragment;
import com.example.e_mobility.fragments.MapFragment;
import com.example.e_mobility.fragments.ProfilFragment;
import com.example.e_mobility.fragments.SettingsFragment;
import com.example.e_mobility.fragments.TankstelleFragment;
import com.google.android.material.navigation.NavigationView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Initialize Map fragment
        Fragment fragment = new MapFragment();
        //Open Fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

        startThread();
    }

    public void startThread() {
        Runnable downloadService = new DownloadService();
        Thread downloadThread = new Thread(downloadService);
        downloadThread.start();
        try {
            downloadThread.join();
        } catch (InterruptedException ex) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_more_menu, menu);

        return true;
    }

    /*************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.more:
                Toast.makeText(this, "More", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.profil:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfilFragment()).commit();
                return true;
            case R.id.logout:
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                try
                {
                    // now log out:
                    Backendless.UserService.logout();
                }
                catch( BackendlessException exception )
                {
                    // logout failed, to get the error code, call exception.getFault().getCode();
                    System.out.println(exception.getCode());
                }
                System.out.println("Successfully logged out from Backendless");
                startActivity(new Intent(MainActivity.this, Login.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_map:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MapFragment()).commit();
                break;
            case R.id.nav_fav:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FavoritenFragment()).commit();
                break;
            case R.id.nav_tankstellen:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TankstelleFragment()).commit();
                break;
            case R.id.nav_last_visited:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new LastVisitedFragment()).commit();
                break;

            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SettingsFragment()).commit();
                break;
            case R.id.nav_help:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HelpFragment()).commit();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}





