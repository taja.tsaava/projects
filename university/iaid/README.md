# h_da Navigator

## Project Description

The "h_da Navigator" project was developed by Anastasia Galeyev, Lukas Kebeiks, and **Anastasia Tsaava (me)**. It provides an innovative solution for campus navigation at Hochschule Darmstadt (h_da). 

The goal of the project is to offer optimized indoor and outdoor navigation, enabling both students and visitors to navigate the campus efficiently and barrier-free.

### Idea

The main idea of the project is to solve existing problems with campus navigation and integrate practical functions such as:

- Indoor and outdoor navigation
- Virtual campus tour
- Barrier-free navigation
- Direct navigation to events
- Providing room information (e.g., floor plans, feedback, capacities)

### Inspiration and Motivation

Inspiration for the h_da Navigator came from existing solutions like Google Maps and INFSOFT Indoor Navigation, as well as places like the Palmengarten Frankfurt. The motivation was to improve upon existing products and develop a more user-friendly and up-to-date solution for navigation on the h_da campus.

### Our Focus

The project's focus is on promoting barrier-free navigation, creating orientation aids, and increasing efficiency by linking to the timetable. The main functionalities of the h_da Navigator include:

- Finding rooms and their attributes
- Information on peak times
- Linking to the timetable

### Development

During development, various color palettes and screens were created, tested, and finalized. 
The h_da Navigator was designed in Figma, allowing detailed visual design and user guidance.

### Future Development Prospects

In the future, additional features could be added, such as:

- Real-time information on current events and facility usage
- Saving favorites
- Using the app for promotional purposes for "Hochschule Darmstadt"
- Rating functions for continuous improvement of the university

## Demo

A demo of the h_da Navigator is available in Figma:
./iaid/h_da Navigator.fig


