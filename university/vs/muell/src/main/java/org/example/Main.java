package org.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
        int port = 32768;
        URL url = new URL("http", "localhost:"+ port, "/matrixValues/");
        String hostname;
        String Data = "Test String";
        hostname = url.getHost();

        ArrayList<Float> databaseMatrix = new ArrayList<>();

        //###DUMMY DATA CREATION BEGINNING###
        int dataBaseColumns = 3;
        int databaseRows = 1000;
        Float value = 2286.0f;
        for (int i = 0; i < databaseRows; i++) {
            for (int j = 0; j < dataBaseColumns; j++) {
                databaseMatrix.add(value);
            }
        }
        //###DUMMY DATA CREATION END###
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Listening on port: " + port);

        // repeatedly wait for connections, and process
        while (true) {
            boolean getMatrix = false;
            boolean postMatrix = false;
            Socket clientSocket = serverSocket.accept();
            System.err.println("Connection with client established");

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            String clientRequestLine;
            String data = "";
            //TODO: postMatrix
            while ((clientRequestLine = in.readLine()) != null) {
                if(clientRequestLine.length() >= 3 && clientRequestLine.substring(0, 3).equals("GET")){
                    if(clientRequestLine.length() >= 17 && clientRequestLine.substring(4, 17).equals("/matrixValues")){
                        getMatrix = true;
                    }
                }
                else if (clientRequestLine.length() >= 4 && clientRequestLine.substring(0, 4).equals("POST")) {
                    if(clientRequestLine.length() >= 18 && clientRequestLine.substring(5, 18).equals("/matrixValues")){
                        postMatrix = true;
                    }
                }
                else if (clientRequestLine.length() >= 4 && clientRequestLine.substring(0, 4).equals("row:")){
                    data = clientRequestLine;
                }

                System.out.println(clientRequestLine);
                if (clientRequestLine.isEmpty()) {
                    break;
                }
            }
            if(postMatrix){
                String [] values = data.split("&");
                System.out.println("Row = " + values[0].substring(4));
                System.out.println("Col = " + values[1].substring(4));
                System.out.println("Result = " + values[2].substring(7));

                System.out.println("POSTFUNKTION");
                out.write("HTTP/1.0 200 OK\r\n");
                out.write("Content-Type: text/html\r\n");
                out.write("Content-Length: "+ 10 +"\r\n");
                out.write("<TITLE>Ack</TITLE>");
                out.write("<P>Progress was send successful</P>");
                out.write("\r\n");
            }

            if(getMatrix){
                System.out.println("GETFUNKTION");
                out.write("HTTP/1.0 200 OK\r\n");
                out.write("Content-Type: text/html\r\n");
                out.write("Content-Length: "+ databaseRows*dataBaseColumns+10 +"\r\n");
                out.write("\r\n");
                out.write("<TITLE>MatrixValues</TITLE>");

                for (int i = 0; i < databaseRows; i++) {
                    for (int j = 0; j < dataBaseColumns; j++) {
                        String output = "<P>Row: " + i + ", Column: " + j + ", Result:" + databaseMatrix.get(i+j) + "</P>";
                        out.write(output);
                    }
                }
            }
            else{
                System.out.println("ELSEFUNKTION");
                out.write("HTTP/1.0 200 OK\r\n");
                out.write("Content-Type: text/html\r\n");
                out.write("Content-Length: 69\r\n");
                out.write("<TITLE>Response</TITLE>");
                out.write("<P>In order to get the wanted data, you need to specify the correct file path in your GET-Request</P>");
                out.write("\r\n");
            }

            System.err.println("Connection terminated");
            out.close();
            in.close();
            clientSocket.close();
        }
    }
}