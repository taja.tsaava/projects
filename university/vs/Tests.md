# Tests

## Non-Functional
#### RTT - Healthcheck Packages (Based on solution of branch: 18_11_22)
- The controller class sends 100000 Healthcheck-Packages to the Worker(s) if the "NON_FUNC_TEST" is set "true"
- The threads of the controller save the values into an array of RTT values
    - This procedure is synchronized, due to the risk of overwriting each other's values 
- if the sample size was reached (in our example 100000), the threads will stop saving the values
    - the controller checks every 5 seconds if the sample size was reached:
        - In this case, the controller will return the Average RTT as well as the Variance and Standard deviation 

###### Results of our Test
Average RTT with 100000 samples: 150005 nano seconds\
Variance RTT with 100000 samples: 807124899162 nano seconds\
Standard deviation RTT with 100000 samples: 898401 nano seconds


#### RTT - TCP-Packages for Database (Based on solution of branch: 29_11_22_pra3_finished)
- The worker sends their packages to the HTTP-Server over the TCP-Socket:
    - if "NON_FUNC_TEST" is set "true", then the value for the time measurements will be passed along with the data package
- if the sample size was reached (in our example 10000), the worker will return Average RTT as well as the Variance and Standard deviation 

###### Results of our Test
Average RTT with 10000 samples: 1648466 nano seconds\
Variance RTT with 10000 samples: 5585616121668 nano seconds\
Standard deviation RTT with 10000 samples: 2363390 nano seconds

#### RTT - Matrix-Packages with RPC from Controller to Worker (Based on solution of branch: 15_12_2022_pra4_finished)
- The controller class sends 20000 Work-Packages to the Worker(s) if the "NON_FUNC_TEST_RPC_EDITION" is set "true"
- The threads of the controller save the values into an array of RTT values, if the worker successful callculated a package
    - This procedure is synchronized, due to the risk of overwriting each other's values 
- if the sample size was reached (in our example 20000), the threads will stop saving the values
    - the controller checks every 5 seconds if the sample size was reached:
        - In this case, the controller will return the Average RTT as well as the Variance and Standard deviation 

###### Results of our Test
> Tobias PC:

Average RTT with 20000 samples: 204178 nano seconds\
Variance RTT with 20000 samples: 181136743452 nano seconds\
Standard deviation RTT with 20000 samples: 425601 nano seconds

> Anastasias PC (1 Kern):

Average RTT with 20000 samples: 763015 nano seconds\
Variance RTT with 20000 samples: 10534741476026 nano seconds\
Standard deviation RTT with 20000 samples: 3245726 nano seconds

> Anastasias PC (4 Kerne):

Average RTT with 20000 samples: 319076 nano seconds\
Variance RTT with 20000 samples: 605599436873 nano seconds\
Standard deviation RTT with 20000 samples: 778202 nano seconds

#### RTT - From Request of Lamport-Algorithm to last needed Ack (Based on solution of branch: main)
- time measurement will be started, if worker publishes a request
- time measurement will be stopped, if the worker received all needed acknowledgements to his request

###### Results of our Test

> 1 Worker

Worker1:\
Average RTT with 1000 samples: 130799414 nano seconds\
Variance RTT with 1000 samples: 62444455257437 nano seconds\
Standard deviation RTT with 1000 samples: 7902180 nano seconds



> 2 Worker

Worker1:\
Average RTT with 1000 samples: 134251471 nano seconds\
Variance RTT with 1000 samples: 1052581503964586 nano seconds\
Standard deviation RTT with 1000 samples: 32443512 nano seconds

Worker2:\
Average RTT with 1000 samples: 133323060 nano seconds\
Variance RTT with 1000 samples: 987284732802377 nano seconds\
Standard deviation RTT with 1000 samples: 31421087 nano seconds



> 3 Worker

Worker1:\
Average RTT with 200 samples: 176397838 nano seconds\
Variance RTT with 200 samples: 3695414326807191 nano seconds\
Standard deviation RTT with 200 samples: 60789919 nano seconds

Worker2:\
Average RTT with 200 samples: 155971738 nano seconds\
Variance RTT with 200 samples: 2659334543209084 nano seconds\
Standard deviation RTT with 200 samples: 51568736 nano seconds

Worker3:\
Average RTT with 200 samples: 172176037 nano seconds\
Variance RTT with 200 samples: 3197246316664559 nano seconds\
Standard deviation RTT with 200 samples: 56544197 nano seconds


## Functional
#### Matrix Data Test (Based on solution of branch: 18_11_22)
- checks if worker really received right data from controller
- (notice: the cells of the matrices are filled with the values of the matrices of the controller (3.0))
- if "FUNC_TEST" is set "true":
    - worker will test if the result with the values of the received matrix from controller are the same as the result with the expected values
        - if they are, then the test was successful
 

#### HTTP Server Test (Based on solution of branch: 29_11_22_pra3_finished)
- if "FUNC_TEST" is set "true":
    - the HTTP Server will check if the total of the Database data equals the total amount of data that should have arrived 
 
```
if(FUNC_TEST) {
    ResultSet rsCount = stmt.executeQuery("select count(*) from matrix");
    rsCount.next();
    int count = rsCount.getInt(1);
        if (count == maxRows * maxColumns)
            System.out.println("Functional Test was a success: All of the expected data arrived");
        else
            System.out.println("Functional Test was a failed: Not all of the expected data arrived");
            notCloseServer = false;
}
```

#### RPC Test (Based on solution of branch: 15_12_2022_pra4_finished)
- checks if the results of the workers are correct
- if "FUNC_TEST_RPC_EDITION" is set "true":
    - the controller will check if the boolean "FUNC_TEST_RPC_EDITION_WORK_RESULT_IS_READY" is false and sleep if so, meaning the controller will simply wait until the worker sends him their results 
    - if "FUNC_TEST_RPC_EDITION_WORK_RESULT_IS_READY" is true  
        - the controller will check their own callculated results with the received results from the worker
        - if the results are the same the test was successful 

#### Lamport Algorithm Test (Based on solution of branch: main)
- if "FUNC_TEST_MOM" is set "true":
    - the worker will check individuelly themselves if they could execute the function "saveIntoDatabase" without another worker uploading data on the HTTP-Server at the same time
    - in order to do that, the worker will publish the message: "SENDING_AT_THE_MOMENT" to all the other workers, after he got all the acknowledgements needed to be allowed to upload his data into the Database
    - while sending data into Database: 
        - if there was a message received with a different ID and with a message containing "SENDING_AT_THE_MOMENT"
        - then the boolean "TEST_FAILED" will be set to "true"
        - the output will say: "FUNCTIONAL TEST FAILED: OTHER WORKER GOT ACCESS ON SERVER, WHILE I AM SENDING!"
        - the workerprogram will exit itself
