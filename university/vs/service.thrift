namespace cpp com.baeldung.thrift.impl
namespace java com.baeldung.thrift.impl

exception InvalidOperationException {
    1: i32 code,
    2: string description
}

struct Workload {
    1: i32 rowProgress,
    2: i32 colProgress,
    3: list <double> matrixRow,
    4: list <double> matrixColumn,
}

struct saveWork {
    1: i32 workerCount
}

service CrossPlatformService {

    bool calculateWorkload(1: i32 rowProgress, 2: i32 colProgress, 3: list <double> matrixRow, 4: list <double> matrixColumn) throws (1:InvalidOperationException e);

    bool saveIntoDatabase(1: i32 workerCount) throws (1:InvalidOperationException e);

    void workDone() throws (1:InvalidOperationException e);

    double calculateWorkloadFunctionalTest(1: i32 rowProgress, 2: i32 colProgress, 3: list <double> matrixRow, 4: list <double> matrixColumn) throws (1:InvalidOperationException e);
}
