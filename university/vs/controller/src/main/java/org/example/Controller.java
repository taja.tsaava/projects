package org.example;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.example.impl.CrossPlatformService;

import java.util.*;
import java.util.concurrent.CountDownLatch;

import static java.lang.Thread.sleep;

public class Controller implements Runnable {
    private float[][] matrix1;
    private float[][] matrix2;

    private ControllerSocket mySocket;

    private int idForWorker = 1;
    private int rowProgress = 0;
    private int columnProgress = 0;
    private HashMap<Integer, TTransport> workerIdToPortForWorkRequests;
    private HashMap<Integer, Integer> workerIdToPortForHealthCheckRequests;

    private TimeMeasure myTimeMeasure;

    private boolean workFinished = false;
    private final String WORKER_HANDLER = "workerHandler";
    private final String WORKERLOAD_HANDLER = "workloadHandler";
    private final String HEALTHCHECK_HANDLER = "healthcheckHandler";
    private int currentWorkerPort = 0;
    private CountDownLatch healthcheckHandlerLatch;


    private int currentWorkerID = 0;
    private CountDownLatch workloadHandlerLatch;
    private final int receiveBufferSize = 1024;

    private int healthCheckSleepInterval = 1000;


    private boolean NON_FUNC_TEST = false; //TODO: CHECK
    private Semaphore mutexForHealthcheck;
    private int measurementValue = 100000;
    long[] rttValues = new long[measurementValue];
    int rttValuesProgress = 0;


    private boolean NON_FUNC_TEST_RPC_EDITION = false; //TODO: CHECK
    private int measurementValueRpcEdition = 20000;


    private boolean FUNC_TEST_RPC_EDITION = false; //TODO: CHECK
    private float FUNC_TEST_RPC_EDITION_WORK_RESULT = 0;
    private boolean FUNC_TEST_RPC_EDITION_WORK_RESULT_IS_READY = false;

    private Semaphore mutexForWorkload;

    private Semaphore mutexForMissingWorkload;
    private Queue missingWorkload;

    public Controller(int matrix1RowCount, int matrix1ColumnCount, int matrix2RowCount, int matrix2ColumnCount) {

        if (matrix1ColumnCount != matrix2RowCount)
            throw new ArithmeticException("Row count of the first matrix must match the column count of the second matrix");

        matrix1 = new float[matrix1RowCount][matrix1ColumnCount];
        matrix2 = new float[matrix2RowCount][matrix2ColumnCount];
        missingWorkload = new LinkedList<WorkloadLocation>();

        matrixInit(matrix1, matrix1RowCount, matrix1ColumnCount);
        matrixInit(matrix2, matrix2RowCount, matrix2ColumnCount);

        workerIdToPortForWorkRequests = new HashMap<Integer, TTransport>();
        workerIdToPortForHealthCheckRequests = new HashMap<Integer, Integer>();

        mySocket = new ControllerSocket();
        mutexForHealthcheck = new Semaphore(1);
        mutexForWorkload = new Semaphore(1);
        mutexForMissingWorkload = new Semaphore(1);

        if (NON_FUNC_TEST) {
            myTimeMeasure = new TimeMeasure();
            runRoundTripTimeTests();
        } else if (NON_FUNC_TEST_RPC_EDITION) {
            myTimeMeasure = new TimeMeasure();
            runRoundTripTimeTestsRpcEdition();
        } else if (FUNC_TEST_RPC_EDITION) {
            runFuncTestRpcEdition();
        } else
            runSocket();
    }

    //All matrix cells are initialized as 3
    public void matrixInit(float[][] matrix, int matrixRowLength, int matrixColumnLength) {

        for (int i = 0; i < matrixRowLength; i++) {
            for (int j = 0; j < matrixColumnLength; j++) {
                matrix[i][j] = 3.0f;
            }
        }
    }

    private void runSocket() {

        mySocket.run();

        while (!workFinished) {
            workerHandler();
        }

        System.exit(0);
    }

    private static CrossPlatformService.Client prepareCommunicationWithWorker(TTransport transport) {
        try {
            transport.open();
        } catch (TTransportException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

        TProtocol protocol = new TBinaryProtocol(transport);
        CrossPlatformService.Client client = new CrossPlatformService.Client(protocol);
        return client;
    }

    private void removeWorker(int idToDelete) {
        workerIdToPortForWorkRequests.remove(idToDelete);
        workerIdToPortForHealthCheckRequests.remove(idToDelete);
    }

    private boolean adjustMatrix(TTransport transport) {
        if (columnProgress < matrix2[0].length - 1)
            columnProgress++;
        else if (columnProgress == (matrix2[0].length - 1) && rowProgress < (matrix1.length - 1)) {
            columnProgress = 0;
            rowProgress++;
        } else {
            System.out.println("Work finished!");
            workFinished = true;
            mySocket.socketTimeoutSetToTimeoutValue(); //Main-Program must not accept more worker, thus a time-out is required
            //If at least two workers are in the while-loop, duplicates could be sent. Thus, the for-loop should be left, if the work is done.
            return true;
        }
        return false;
    }


    public void run() {

        if (Thread.currentThread().getName().equals(WORKER_HANDLER))
            while (!workFinished) {
                workerHandler();
            }
        else if (Thread.currentThread().getName().equals(HEALTHCHECK_HANDLER))
            healthCheckHandler();
        else if (Thread.currentThread().getName().equals(WORKERLOAD_HANDLER))
            workloadHandler();
    }

    private void workerHandler() {
        boolean knownWorker = true;

        int workerPort = mySocket.waitForWorkerResponse();
        knownWorker = mySocket.isKnownWorker();

        if (workerPort != -1 && !knownWorker) {
            takeOnNewWorker(workerPort);
            System.out.println("NEW WORKER");
        } else if (workerPort == -1) {
            System.out.println("Error, probably?");
        }

    }

    private void takeOnNewWorker(int workerPort) {
        mySocket.socketTimeoutReset(); //needed, so timeout doesn't occur while no workers exist

        try {
            TTransport transport = new TSocket("localhost", workerPort);
            workerIdToPortForWorkRequests.put(idForWorker, transport);
        } catch (TTransportException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }


        workerIdToPortForHealthCheckRequests.put(idForWorker, mySocket.getWorkerPortHealthCheckRequest());

        System.out.println("###HEALTHPORT OF NEW WORKER###" + ", PORT: " + mySocket.getWorkerPortHealthCheckRequest());

        currentWorkerPort = workerPort; //FOR HEALTHCHECKHANDLER
        healthcheckHandlerLatch = new CountDownLatch(1);
        Thread threadHealth = new Thread(this, HEALTHCHECK_HANDLER);
        threadHealth.start();

        try {
            healthcheckHandlerLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        currentWorkerID = idForWorker; //FOR WORKLOADHANDLER
        workloadHandlerLatch = new CountDownLatch(1);
        Thread threadWorkload = new Thread(this, WORKERLOAD_HANDLER);
        threadWorkload.start();

        try {
            workloadHandlerLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        idForWorker++;
    }

    private void healthCheckHandler() { //TODO: wenn worker stirbt -> healthCheckHandler beenden

        ControllerHealthCheckSocket udpSocket = new ControllerHealthCheckSocket();

        int workerPort = currentWorkerPort;
        int workerPortHealthCheck = mySocket.getWorkerPortHealthCheckRequest();

        mySocket.sendID(idForWorker, workerPort, udpSocket.getLocalPort());
        healthcheckHandlerLatch.countDown();

        TimeMeasure myTimeMeasure = null;
        if (NON_FUNC_TEST)
            myTimeMeasure = new TimeMeasure();

        int countTimeout = 0;

        while (mySocket.isRunning()) {
            if (NON_FUNC_TEST)
                myTimeMeasure.setStart(System.nanoTime());

            udpSocket.healthCheck(workerPortHealthCheck);
            int healthCheckErrorCode = udpSocket.waitForHealthCheckResponse();

            if (NON_FUNC_TEST && healthCheckErrorCode == 0) {
                myTimeMeasure.setEnd(System.nanoTime());
                writeValue(myTimeMeasure.getRTT());
            }

            if (healthCheckErrorCode != 0) {
                System.out.println("Port: " + workerPortHealthCheck + ": " + "ERROR, COULDN'T GET HEALTH CHECK");
                countTimeout++;
            } else {
                countTimeout = 0;
                //System.out.println("Port: " + workerPortHealthCheck + ": " + "HEALTHCHECK: SUCCESS");
            }

            if (countTimeout == 3)
                break;

            try {
                sleep(healthCheckSleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
        System.out.println("Healthcheck-Handler says Bye Bye! :P");
    }


    int countTimeout = 0;

    private void workloadHandler() {
        int workerID = currentWorkerID;
        workloadHandlerLatch.countDown();

        TTransport transport = workerIdToPortForWorkRequests.get(workerID);
        CrossPlatformService.Client client = prepareCommunicationWithWorker(transport);

        while (!workFinished) {

            int rowProgress = 0;
            int columnProgress = 0;
            TimeMeasure myTimeMeasure = null;
            if (NON_FUNC_TEST_RPC_EDITION)
                myTimeMeasure = new TimeMeasure();

            try {

                //TODO: Checks if there are values in cache-array
                mutexForMissingWorkload.p();
                try {
                    WorkloadLocation myWorkloadLocation = (WorkloadLocation) missingWorkload.remove();
                    rowProgress = myWorkloadLocation.getRowLocation();
                    columnProgress = myWorkloadLocation.getColumnLocation();
                    System.out.println("Removing Missing Workload: row=" + rowProgress + ", col=" + columnProgress); //TODO: DEBUG
                } catch (NoSuchElementException e) {
                    //Critical Area beginning
                    mutexForWorkload.p();
                    rowProgress = this.rowProgress;
                    columnProgress = this.columnProgress;
                    adjustMatrix(transport);
                    mutexForWorkload.v();
                    //Critical Area end
                }
                mutexForMissingWorkload.v();

                if (NON_FUNC_TEST_RPC_EDITION)
                    myTimeMeasure.setStart(System.nanoTime());

                if (FUNC_TEST_RPC_EDITION) {
                    FUNC_TEST_RPC_EDITION_WORK_RESULT = (float) client.calculateWorkloadFunctionalTest((rowProgress + 1), (columnProgress + 1), prepareFirstMatrix(matrix1), prepareSecondMatrix(matrix2));
                    FUNC_TEST_RPC_EDITION_WORK_RESULT_IS_READY = true;
                }

                boolean resultWorkload = client.calculateWorkload((rowProgress + 1), (columnProgress + 1), prepareFirstMatrix(matrix1), prepareSecondMatrix(matrix2));


                if (NON_FUNC_TEST_RPC_EDITION && resultWorkload) {
                    myTimeMeasure.setEnd(System.nanoTime());
                    writeValue(myTimeMeasure.getRTT());
                }
                if (resultWorkload) {
                    boolean resultSaveValues = client.saveIntoDatabase(workerIdToPortForWorkRequests.size());
                    if (!resultSaveValues) {
                        System.out.println("Saving the values didn't work right");
                        saveMissingWorkloads(rowProgress, columnProgress);
                    }
                } else {
                    System.out.println("Multiplying matrices didn't work right");
                    saveMissingWorkloads(rowProgress, columnProgress);
                }

            } catch (TException e) {
                System.out.println(e.getMessage());
                saveMissingWorkloads(rowProgress, columnProgress);

                removeWorker(workerID);
                break;
            }
        }

        try {
            System.out.println("WTF");//TODO: DEBUG
            client.workDone();
        } catch (TException e) {
            System.out.println(e.getMessage());
        }

        if (transport.isOpen())
            transport.close();
    }

    private void saveMissingWorkloads(int rowProgress, int columnProgress) {
        //TODO: Writes values in cache-array
        WorkloadLocation myWorkloadLocation = new WorkloadLocation(rowProgress, columnProgress);
        mutexForMissingWorkload.p();
        missingWorkload.add(myWorkloadLocation);
        System.out.println("Adding to Missing Workload: row=" + rowProgress + ", col=" + columnProgress); //TODO: DEBUG
        mutexForMissingWorkload.v();
    }

    private List<Double> prepareFirstMatrix(float[][] firstMatrix) {
        List<Double> preparedFirstMatrix = new ArrayList<Double>();
        for (int i = 0; i < matrix1[0].length; i++) {
            preparedFirstMatrix.add((double) firstMatrix[rowProgress][i]);
        }
        return preparedFirstMatrix;
    }

    private List<Double> prepareSecondMatrix(float[][] secondMatrix) {
        List<Double> preparedSecondMatrix = new ArrayList<Double>();
        for (int i = 0; i < matrix2.length; i++) {
            preparedSecondMatrix.add((double) secondMatrix[i][columnProgress]);
        }
        return preparedSecondMatrix;
    }

    private void runRoundTripTimeTests() {
        mySocket.run();
        healthCheckSleepInterval = 0;
        int checkValueCountSleepInterval = 5000;

        Thread thread = new Thread(this, WORKER_HANDLER);
        thread.start();

        while (readValue() != measurementValue) {
            try {
                sleep(checkValueCountSleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("Current readValue(): " + readValue());
        }

        long result = 0;
        long variance = 0;

        for (int i = 0; i < measurementValue; i++) {
            result += rttValues[i];
        }
        result = result / measurementValue;


        for (int i = 0; i < measurementValue; i++) {
            variance += Math.pow(rttValues[i] - result, 2);
        }

        variance = variance / measurementValue;
        long standardDeviation = (int) Math.sqrt(variance);

        System.out.println("Average RTT with " + measurementValue + " samples: " + result + " nano seconds");
        System.out.println("Variance RTT with " + measurementValue + " samples: " + variance + " nano seconds");
        System.out.println("Standard deviation RTT with " + measurementValue + " samples: " + standardDeviation + " nano seconds");
        //System.out.println("Package Loss: " + 100*lostPackets/(float)measurementValue + "% with timeout value: "+ timeOutValue + " ms");

    }

    private void writeValue(long value) {
        mutexForHealthcheck.p();

        if (NON_FUNC_TEST) {
            if (rttValuesProgress < measurementValue) {
                rttValues[rttValuesProgress] = value;
                rttValuesProgress++;
            }
        } else if (NON_FUNC_TEST_RPC_EDITION) {
            if (rttValuesProgress < measurementValueRpcEdition) {
                rttValues[rttValuesProgress] = value;
                rttValuesProgress++;
            }
        }

        mutexForHealthcheck.v();
    }

    private int readValue() {
        mutexForHealthcheck.p();
        int returnValue = rttValuesProgress;
        mutexForHealthcheck.v();
        return returnValue;
    }

    private void runRoundTripTimeTestsRpcEdition() {
        int checkValueCountSleepInterval = 5000;
        mySocket.run();

        Thread thread = new Thread(this, WORKER_HANDLER);
        thread.start();

        while (readValue() != measurementValueRpcEdition) {
            try {
                sleep(checkValueCountSleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("Current readValue(): " + readValue());
        }

        long result = 0;
        long variance = 0;

        for (int i = 0; i < measurementValueRpcEdition; i++) {
            result += rttValues[i];
        }
        result = result / measurementValueRpcEdition;


        for (int i = 0; i < measurementValueRpcEdition; i++) {
            variance += Math.pow(rttValues[i] - result, 2);
        }

        variance = variance / measurementValueRpcEdition;
        long standardDeviation = (int) Math.sqrt(variance);

        System.out.println("Average RTT with " + measurementValueRpcEdition + " samples: " + result + " nano seconds");
        System.out.println("Variance RTT with " + measurementValueRpcEdition + " samples: " + variance + " nano seconds");
        System.out.println("Standard deviation RTT with " + measurementValueRpcEdition + " samples: " + standardDeviation + " nano seconds");
    }

    private void runFuncTestRpcEdition() {
        int checkResultRdySleepInterval = 5000;
        boolean success = false;

        mySocket.run();

        Thread thread = new Thread(this, WORKER_HANDLER);
        thread.start();

        while (!FUNC_TEST_RPC_EDITION_WORK_RESULT_IS_READY) {
            try {
                sleep(checkResultRdySleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.print("");


        float matrixResult = 0;
        List<?> testMatrix1 = prepareFirstMatrix(matrix1);
        List<?> testMatrix2 = prepareSecondMatrix(matrix2);

        for (int i = 0; i < testMatrix1.size(); i++) {
            matrixResult += (Double) testMatrix1.get(i) * (Double) testMatrix2.get(i);
        }

        System.out.println("Controller's result: " + FUNC_TEST_RPC_EDITION_WORK_RESULT);
        System.out.println("Worker's result: " + matrixResult);

        if (matrixResult == FUNC_TEST_RPC_EDITION_WORK_RESULT)
            success = true;
        else
            success = false;


        if (success)
            System.out.println("Test successful: The result of the worker is correct");
        else
            System.out.println("Test failed: The result of the worker is not correct");

    }
}



