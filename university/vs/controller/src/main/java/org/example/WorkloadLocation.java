package org.example;

public class WorkloadLocation {
    private int rowLocation;
    private int columnLocation;

    public WorkloadLocation(int parameterRowLocation, int parameterColumnLocation){
        this.rowLocation = parameterRowLocation;
        this.columnLocation = parameterColumnLocation;
    }

    public int getRowLocation() {
        return rowLocation;
    }

    public int getColumnLocation() {
        return columnLocation;
    }
}
