package org.example;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;


public class ControllerHealthCheckSocket {

    private DatagramSocket udpReceiveSocketHealthCheck;
    private byte[] receiveBufferHealthCheck;
    private final int receiveBufferSize = 1024;


    private DatagramSocket sendSocket;
    private byte[] sendBuffer;
    private final int sendBufferSize = 1024;

    private final String sendAddressName = "localhost";
    private InetAddress sendAddress;

    private int TIMEOUT = 50;

    public ControllerHealthCheckSocket(){
        receiveBufferHealthCheck = new byte[receiveBufferSize];
        sendBuffer = new byte[sendBufferSize];
        run();
    }

    private void run(){
        try{
            this.udpReceiveSocketHealthCheck = new DatagramSocket();
            System.out.println("Started the UDP HealthCheck socket controller at receivePort " + this.udpReceiveSocketHealthCheck.getLocalPort() + " with buffer size " + receiveBufferSize + ".");

            socketTimeoutSetToTimeoutValue();

            sendSocket = new DatagramSocket(0);
        }
        catch (SocketException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public int waitForHealthCheckResponse(){
        DatagramPacket udpPacket = new DatagramPacket(receiveBufferHealthCheck, receiveBufferSize);
        try {
            udpReceiveSocketHealthCheck.receive(udpPacket);

            byte[] content = Arrays.copyOfRange(udpPacket.getData(), 0, udpPacket.getLength());

            if(new String(content).equals("healthcheck status: ok")){
                return 0;
            }

        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
        }
        catch (IOException e) {
            System.out.println("Message could not be received.\n" + e);
        }

        return -1;
    }

    public void healthCheck(int port){
        sendBuffer = ("healthcheck").getBytes();
        send(port, sendBuffer);
    }

    private synchronized void send(int port, byte[] buf) {
        try{
            sendAddress = InetAddress.getByName(sendAddressName);
        } catch (UnknownHostException e){
            System.out.println("Can not parse send address.\n" + e.getMessage());
            System.exit(1);
        }

        DatagramPacket packet = new DatagramPacket(buf, buf.length, sendAddress, port);
        try {
            sendSocket.send(packet);
        } catch (IOException e) {
            System.out.println("Port: "+ packet.getPort() +", Message could not be send.\n" + e);
        }
    }

    public int getLocalPort(){
        return udpReceiveSocketHealthCheck.getLocalPort();
    }

    public void socketTimeoutSetToTimeoutValue(){
        try {
            udpReceiveSocketHealthCheck.setSoTimeout(TIMEOUT);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public void setTIMEOUT(int TIMEOUT) {
        this.TIMEOUT = TIMEOUT;
    }
}
