package org.example;
import java.net.Socket;
import java.net.UnknownHostException;

import static java.lang.Thread.sleep;

public class Worker implements Runnable{
    private int id;
    private float[][] theWantedArray;
    private float[] matrixRow;
    private float[] matrixCol;

    private boolean DEBUG = false;
    private boolean FUNC_TEST = false;
    private WorkerSocket socket;
    private TimeMeasure myTimeMeasure;
    //private double averageEST = 0; //average estimated time for RTT

    public Worker() throws UnknownHostException {

        myTimeMeasure = new TimeMeasure();
        socket = new WorkerSocket();
        runSocket();
    }

    public int statisticsRTT(){
        int numberOfPackages = 100;
        int[] rtts = new int[numberOfPackages];

        for(int i=0; i<numberOfPackages; i++){
            myTimeMeasure.setStart(System.nanoTime());
            socket.requestWork(getId());
            socket.getWorkload(myTimeMeasure);
            int time = (int)(myTimeMeasure.getEnd() - myTimeMeasure.getStart()) / 1000;
            rtts[i] = time;
        }

        int result = 0;

        for (int i = 0; i < numberOfPackages; i++) {
            result += rtts[i];
        }

        result = result/numberOfPackages;
        return result;
        //System.out.println("Average RTT with " + numberOfPackages + " samples: " + result + " micro seconds");
    }


    private void runSocket(){
        socket.createConnection();

        if(FUNC_TEST ==false) {
        Thread healthcheckResponder = new Thread(this);
        healthcheckResponder.start();

            id = socket.requestID();
            while (socket.isWorkNotDone()) {

                myTimeMeasure.setStart(System.nanoTime());

                socket.requestWork(id);
                theWantedArray = socket.getWorkload(myTimeMeasure);

                //myTimeMeasure.getRTT();


                matrixRow = fillMatrixRow(theWantedArray);
                matrixCol = fillMatrixCol(theWantedArray);

            /*System.out.println("Row worker: ");
            for (int i = 0; i < matrixRow.length; i++) {
                System.out.println(i + ": " + matrixRow[i]);
            }

            System.out.println("Col worker: ");
            for (int i = 0; i < matrixCol.length; i++) {
                System.out.println(i + ": " + matrixCol[i]);
            }*/

                if (socket.isWorkNotDone())
                    matrixMul(matrixCol, matrixRow); //UNBEDINGT BEHEBEN

                if (DEBUG == true) {
                    int estRTT = statisticsRTT();
                    System.out.println("Average estimated Round Trip Time: " + estRTT);
                }
            }
        }
        else {
            testRightValue();
        }

        System.out.println("goodbye world :,(");

    }

    /* Getter */
    public int getId() {
        return this.id;
    }

    /* Functions to fill theWantedArray into matrixCol and matrixRow */
    public float[] fillMatrixRow(float [][] theWantedArray){
        matrixRow = theWantedArray[0];
        return this.matrixRow;
    }

    public float[] fillMatrixCol(float [][] theWantedArray){
        matrixCol = theWantedArray[1];
        return this.matrixCol;
    }



    /* Multiplication function for Matrix */
    float matrixMul(float[] matrixCol, float[] matrixRow) {
        float matrixResult = 0;

        for (int i = 0; i < matrixRow.length; i++) {
            matrixResult += matrixCol[i] * matrixRow[i];
        }
        return matrixResult;
    }

    @Override
    public void run() {
        while (socket.isWorkNotDone())
            socket.waitForController();
    }

    private void testRightValue(){
        id = socket.requestID();
        socket.requestWork(id);


        theWantedArray = socket.getWorkload(myTimeMeasure);
        matrixRow = fillMatrixRow(theWantedArray);
        matrixCol = fillMatrixCol(theWantedArray);


        /*for (int i = 0; i < matrixRow.length; i++) {
            System.out.println("matrixRow: "+matrixRow[i]);
            System.out.println("matrixCol: "+matrixCol[i]);
        }*/

        float result1 = matrixMul(matrixCol, matrixRow);

        int matrSize = 254;
        float testMatrix1[] = new float[matrSize];
        float testMatrix2[]= new float[matrSize];

        for (int i = 0; i < matrSize; i++) {
            testMatrix1[i] = 3.0f;
            testMatrix2[i] = 3.0f;
        }

        float result2 = matrixMul(testMatrix1, testMatrix2);

        if(result1 == result2)
            System.out.println("Test erfolgreich");
    }

}