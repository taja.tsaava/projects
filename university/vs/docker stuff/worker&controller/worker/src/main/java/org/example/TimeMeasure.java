package org.example;

public class TimeMeasure {

    private long start;

    private long end;

    TimeMeasure(){
        start = 0;
        end = 0;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public void getRTT(){
        System.out.println("RTT: " + (end-start)/1000 + " micro seconds");
    }

}
