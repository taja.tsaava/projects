package org.example;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;


public class WorkerSocket {
    private int receivePort = 0;
    private final int sendPort = 6543;
    private int sendPortHealthCheck = 6544;
    private final int bufferSize = 1024;

    private byte[] receiveBuffer;
    private byte[] sendBuffer;
    private float [][] theWantedArray = new float[2][];

    private DatagramSocket wReceiveSocket; //UDP Socket
    private DatagramSocket hReceiveSocket; //UDP Socket
    private DatagramSocket sendSocket;
    String addressName = "localHost";
    InetAddress iaddress = InetAddress.getByName(addressName);

    boolean workNotDone = true;

    public boolean isWorkNotDone() {
        return workNotDone;
    }




    public WorkerSocket() throws UnknownHostException {
        receiveBuffer = new byte[bufferSize];
        sendBuffer = new byte[bufferSize];
    }

    public void createConnection(){
        try{
            this.wReceiveSocket = new DatagramSocket(receivePort);
            this.hReceiveSocket = new DatagramSocket(receivePort);
            System.out.println("Started the UDP work socket for worker with receivePort " + this.wReceiveSocket.getLocalPort() + " with buffer size " + bufferSize + ".");
            System.out.println("Started the UDP healthcheck socket for worker with receivePort " + this.hReceiveSocket.getLocalPort() + " with buffer size " + bufferSize + ".");
            int wTIMEOUT = 50;
            int hTIMEOUT = 2000;
            hReceiveSocket.setSoTimeout(hTIMEOUT);

            this.sendSocket = new DatagramSocket(0);
        }
        catch (SocketException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public int requestID(){
        byte[] buf = ("Port: "+ this.wReceiveSocket.getLocalPort() + "," + this.hReceiveSocket.getLocalPort()).getBytes();
        send(sendPort, buf);

        //send(sendPort, sendBuffer);

        DatagramPacket controllerPacket = new DatagramPacket(receiveBuffer, bufferSize);
        try {
            wReceiveSocket.receive(controllerPacket);
        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
            return requestID(); //Trying to get id again, if timeout occurs
        }catch (IOException e) {
            throw new RuntimeException(e);
        }

        byte[] content = Arrays.copyOfRange(controllerPacket.getData(), 0, controllerPacket.getLength());

        if(new String(content).length() > 3) {
            System.out.println(new String(content));
            if (new String(content).substring(0, 4).equals("Id: ")) {
                String[] values = new String(content).split(",");


                int id = Integer.valueOf(values[0].substring(4));
                sendPortHealthCheck = Integer.valueOf(values[1]);

                System.out.println("ID: " + id + ", " + "HCPORT: " + sendPortHealthCheck);
                return id;
            }
        }

        return -1;
    }

    public void requestWork(int id){
        //System.out.println("###REQUEST WORKLOAD###");
        byte[] buf = ("New workload required, " + id).getBytes();
        send(sendPort, buf);
    }

    public float[][] getWorkload(TimeMeasure myTM){
        getColOrRow(theWantedArray, myTM);
        getColOrRow(theWantedArray);
        return theWantedArray;
    }

    private void getColOrRow(float[][] theWantedArray) {
        DatagramPacket controllerPacket = new DatagramPacket(receiveBuffer, bufferSize);

        try {
            wReceiveSocket.receive(controllerPacket);
            evaluateContent(theWantedArray, controllerPacket);
        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void getColOrRow(float[][] theWantedArray, TimeMeasure myTM) {
        DatagramPacket controllerPacket = new DatagramPacket(receiveBuffer, bufferSize);

        try {
            wReceiveSocket.receive(controllerPacket);
            myTM.setEnd(System.nanoTime());
            evaluateContent(theWantedArray, controllerPacket);
        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void evaluateContent(float[][] theWantedArray, DatagramPacket controllerPacket) {
        byte[] content = Arrays.copyOfRange(controllerPacket.getData(), 0, controllerPacket.getLength());


        if (new String(content).substring(0, 4).equals("row:")) {
            theWantedArray[0] = byteToFloat(content);

             /*   System.out.println("ByteToFloatSuccessful");
                for (int i = 0; i < theWantedArray[0].length; i++) {
                    System.out.println(i + ": " + theWantedArray[0][i]);
                }*/

        }
        else if (new String(content).substring(0, 4).equals("col:")) {
            theWantedArray[1] = byteToFloat(content);

             /*   System.out.println("ByteToFloatSuccessful");
                for (int i = 0; i < theWantedArray[1].length; i++) {
                    System.out.println(i + ": " + theWantedArray[1][i]);
                }*/
        }
        else if(new String(content).equals("work done")){
            workNotDone = false;
        }
    }

    public void healthCheck(int port){
        byte[] buf = ("healthcheck status: ok").getBytes();
        send(port, buf);
    }

    public int waitForController(){
        byte[] buf = new byte[bufferSize];
        DatagramPacket controllerPacket = new DatagramPacket(buf, bufferSize);

        try {
            System.out.println("###WAITING FOR HEALTHCHECK###" + ", PORT: " + hReceiveSocket.getLocalPort());
            hReceiveSocket.receive(controllerPacket);
            byte[] content = Arrays.copyOfRange(controllerPacket.getData(), 0, controllerPacket.getLength());
            System.out.println("###GOT HEALTHCHECK PACKAGE###, content: " + new String(content));

            if(new String(content).equals("healthcheck")) {
                System.out.println("###SENDING HEALTHCHECKRESPONSE###");
                healthCheck(sendPortHealthCheck);
            }

        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        return 1;
    }

    private synchronized void send(int port, byte[] buf) {
        try{
            iaddress = InetAddress.getByName(addressName);
        } catch (UnknownHostException e){
            System.out.println("Can not parse send address.\n" + e.getMessage());
            System.exit(1);
        }

        DatagramPacket packet = new DatagramPacket(buf, buf.length, iaddress, port);
        try {
            sendSocket.send(packet);
        } catch (IOException e) {
            System.out.println("Port: "+ packet.getPort() +", Message could not be send.\n" + e);

            /*try {
                System.out.println("###Reached sleep statement###");
                int randomSleepTime = (int)(Math.random()*1000)+1000;
                sleep(randomSleepTime);
                System.out.println(randomSleepTime);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
            send(port, buf);*/

        }
    }

    private float[] byteToFloat(byte[] input) {

        //System.out.println("Progress: " + ByteBuffer.wrap(input, 4, 4).getInt());

        float[] ret = new float[(input.length-8)/*TEST input.length*/ / 4];
        for (int x = 8/*TEST x = 0*/; x < input.length; x += 4) {
            ret[(x-8)/*TEST x = 0*/ / 4] = ByteBuffer.wrap(input, x, 4).getFloat();
        }
        return ret;
    }

}