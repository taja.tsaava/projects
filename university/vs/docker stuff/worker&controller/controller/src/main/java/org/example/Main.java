package org.example;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static final String COMMA_DELIMITER = ",";
    private static final String CONFIG_PATH = "./src/config.csv";

    public static void main(String[] args)
    {
        try {
            mainProgram();
        }
        catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }
    }

    static public void mainProgram(){

        int matrix1RowCount= 0;
        int matrix1ColumnCount = 0;
        int matrix2RowCount = 0;
        int matrix2ColumnCount = 0;

        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(CONFIG_PATH))) {
            String line = br.readLine();
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Matrix creation beginning");
        matrix1RowCount = Integer.parseInt(records.get(0).get(0));
        matrix1ColumnCount = Integer.parseInt(records.get(0).get(1));
        matrix2RowCount = Integer.parseInt(records.get(0).get(2));
        matrix2ColumnCount = Integer.parseInt(records.get(0).get(3));
        System.out.println("Matrix values: " + matrix1RowCount + ", " + matrix1ColumnCount + ", " + matrix2RowCount + ", " + matrix2ColumnCount);
        System.out.println("Matrix creation end");

        Controller myController = new Controller(matrix1RowCount, matrix1ColumnCount, matrix2RowCount, matrix2ColumnCount);
    }
}
