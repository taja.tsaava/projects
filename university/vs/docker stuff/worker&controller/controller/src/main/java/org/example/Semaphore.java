package org.example;

public class Semaphore {
    private int value;

    public Semaphore(int init) {
        if(init < 0)
            init = 0;
        value = init;
    }

    public synchronized void p() { // Dijkstra's operation p=down
        while(value == 0) {
            try {
                wait();
            }
            catch(InterruptedException e) {}
        }
        value--;
    }

    public synchronized void v() { // Dijkstra's operation v=up
        value++;
        notify();
    }
}
