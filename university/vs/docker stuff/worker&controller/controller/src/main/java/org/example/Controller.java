package org.example;

import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

import static java.lang.Thread.sleep;

public class Controller implements Runnable{
    private float [][] matrix1;
    private float [][] matrix2;

    private ControllerSocket mySocket;

    private int idForWorker = 1;
    private int rowProgress = 0;
    private int columnProgress = 0;
    private HashMap<Integer, Integer> workerIdToPortForWorkRequests;
    private HashMap<Integer, Integer> workerIdToPortForHealthCheckRequests;

    private TimeMeasure myTimeMeasure;

    private boolean workFinished = false;
    private final String WORKLOAD_HANDLER = "workloadHandler";
    private final String HEALTHCHECK_HANDLER = "healthcheckHandler";
    private int currentWorkerPort = 0;
    private CountDownLatch latch;
    private final int receiveBufferSize = 1024;

    private int healthCheckSleepInterval = 1000;


    private boolean NON_FUNC_TEST = true;
    private Semaphore mutex;
    private int measurementValue = 100000;
    long[] rttValues = new long[measurementValue];
    int rttValuesProgress = 0;

    public Controller(int matrix1RowCount, int matrix1ColumnCount, int matrix2RowCount, int matrix2ColumnCount){

        if(matrix1ColumnCount != matrix2RowCount)
            throw new ArithmeticException("Row count of the first matrix must match the column count of the second matrix");

        matrix1 = new float[matrix1RowCount][matrix1ColumnCount];
        matrix2 = new float[matrix2RowCount][matrix2ColumnCount];

        matrixInit(matrix1, matrix1RowCount, matrix1ColumnCount);
        matrixInit(matrix2, matrix2RowCount, matrix2ColumnCount);

        workerIdToPortForWorkRequests = new HashMap<Integer, Integer>();
        workerIdToPortForHealthCheckRequests = new HashMap<Integer, Integer>();

        mySocket = new ControllerSocket();
        mutex = new Semaphore(1);

        if(NON_FUNC_TEST){
            myTimeMeasure = new TimeMeasure();
            runRoundTripTimeTests();
        }
        else
            runSocket();
    }

    //All matrix cells are initialized as 3
    public void matrixInit(float [][] matrix, int matrixRowLength, int matrixColumnLength){

        for (int i = 0; i < matrixRowLength; i++) {
            for (int j = 0; j < matrixColumnLength; j++) {
                matrix[i][j] = 3.0f;
            }
        }
    }

    private void runSocket(){

        mySocket.run();

        Thread thread = new Thread(this, WORKLOAD_HANDLER);
        thread.start();

    }

    public void run() {

        if(Thread.currentThread().getName().equals(WORKLOAD_HANDLER))
            while (mySocket.isRunning()) {
                workloadHandler();
            }
        else if(Thread.currentThread().getName().equals(HEALTHCHECK_HANDLER))
            while (mySocket.isRunning()) {
                healthCheckHandler();
            }
    }

    private void workloadHandler() {
        boolean knownWorker = true;

        int workerPort = mySocket.waitForWorkerResponse();
        knownWorker = mySocket.isKnownWorker();

        if (!workFinished) {
            if (workerPort != -1 && !knownWorker) {
                takeOnNewWorker(workerPort);
                sendWorkload(workerPort);
            } else if (workerPort != -1 && knownWorker) {
                sendWorkload(workerIdToPortForWorkRequests.get(workerPort)); //in this case workerPort is an id
            } else if (workerPort == -1) {
                System.out.println("Error, probably?");
            }
        } else if(workFinished){
            if (workerPort != -1 && !knownWorker) {
                takeOnNewWorker(workerPort);
                mySocket.sendWorkDone(workerPort);
            } else if (workerPort != -1 && knownWorker) {
                mySocket.sendWorkDone(workerIdToPortForWorkRequests.get(workerPort)); //in this case workerPort is an id //
            }else if (workerPort == -1) {
                    System.out.println("Error, probably?");
            }
        }
    }

    private void takeOnNewWorker(int workerPort) {
        mySocket.socketTimeoutSetToTimeoutValue(); //needed, so timeout doesn't occur while no workers exist
        workerIdToPortForWorkRequests.put(idForWorker, workerPort);
        workerIdToPortForHealthCheckRequests.put(idForWorker, mySocket.getWorkerPortHealthCheckRequest());

        System.out.println("###HEALTHPORT OF NEW WORKER###" + ", PORT: " + mySocket.getWorkerPortHealthCheckRequest());

        currentWorkerPort = workerPort;
        latch = new CountDownLatch(1);
        Thread thread = new Thread(this, HEALTHCHECK_HANDLER);
        thread.start();

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        idForWorker++;
    }

    private void healthCheckHandler(){
        ControllerHealthCheckSocket udpSocket = new ControllerHealthCheckSocket();

        int workerPort = currentWorkerPort;
        int workerPortHealthCheck = mySocket.getWorkerPortHealthCheckRequest();

        mySocket.sendID(idForWorker, workerPort, udpSocket.getLocalPort());
        latch.countDown();

        TimeMeasure myTimeMeasure = null;
        if(NON_FUNC_TEST)
            myTimeMeasure = new TimeMeasure();

        while(true){
            if(NON_FUNC_TEST)
                myTimeMeasure.setStart(System.nanoTime());

            udpSocket.healthCheck(workerPortHealthCheck);
            int healthCheckErrorCode = udpSocket.waitForHealthCheckResponse();

            if(NON_FUNC_TEST && healthCheckErrorCode == 0){
                myTimeMeasure.setEnd(System.nanoTime());
                writeValue(myTimeMeasure.getRTT());
            }

            if(healthCheckErrorCode != 0){
                System.out.println("Port: " + workerPortHealthCheck + ": " + "ERROR, COULDN'T GET HEALTH CHECK");
            }
            else {
                //System.out.println("Port: " + workerPortHealthCheck + ": " + "HEALTHCHECK: SUCCESS");
            }

            try {
                sleep(healthCheckSleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }

    private void sendWorkload(int workerPort) {

        mySocket.sendWorkLoad(workerPort, matrix1[rowProgress], prepareSecondMatrix(matrix2), (rowProgress+1), (columnProgress+1));

        if(columnProgress < matrix2[0].length-1)
            columnProgress++;
        else if(columnProgress == (matrix2[0].length-1) && rowProgress < (matrix1.length-1)){
            columnProgress = 0;
            rowProgress++;
        }
        else{
            System.out.println("Work finished!");
            workFinished = true;
            mySocket.socketTimeoutReset();
        }

    }

    private float[] prepareSecondMatrix(float [][] secondMatrix){
        float[] preparedSecondMatrix = new float[matrix2.length];
        for (int i = 0; i < matrix2.length; i++) {
            preparedSecondMatrix[i] = secondMatrix[i][columnProgress];
        }
        return preparedSecondMatrix;
    }




    
    private void runRoundTripTimeTests(){
        mySocket.run();
        healthCheckSleepInterval = 0;
        int checkValueCountSleepInterval = 5000;

        Thread thread = new Thread(this, WORKLOAD_HANDLER);
        thread.start();

        while(readValue() != measurementValue){
            try {
                sleep(checkValueCountSleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("Current readValue(): " + readValue());
        }

        long result = 0;
        long variance = 0;

        for (int i = 0; i < measurementValue; i++) {
            result += rttValues[i];
        }
        result = result/measurementValue;


        for (int i = 0; i < measurementValue; i++) {
            variance += Math.pow(rttValues[i]-result, 2);
        }

        variance = variance/measurementValue;
        long standardDeviation = (int) Math.sqrt(variance);

        System.out.println("Average RTT with " + measurementValue + " samples: " + result + " nano seconds");
        System.out.println("Variance RTT with " + measurementValue + " samples: " + variance + " nano seconds");
        System.out.println("Standard deviation RTT with " + measurementValue + " samples: " + standardDeviation + " nano seconds");
        //System.out.println("Package Loss: " + 100*lostPackets/(float)measurementValue + "% with timeout value: "+ timeOutValue + " ms");

    }

    private void writeValue(long value){
        mutex.p();

        if(rttValuesProgress < measurementValue){
            rttValues[rttValuesProgress] = value;
            rttValuesProgress++;
        }

        mutex.v();
    }

    private int readValue(){
        mutex.p();
        int returnValue = rttValuesProgress;
        mutex.v();
        return returnValue;
    }
}

