package org.example;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;


public class ControllerSocket {
    private final int receivePort = 6543;

    private final int receiveBufferSize = 1024;
    private byte[] receiveBuffer;
    private final int sendBufferSize = 1024;
    private byte[] sendBuffer;

    private final String sendAddressName = "localhost";
    private InetAddress sendAddress;

    private boolean running = true;

    private boolean knownWorker = false;

    private int workerPortHealthCheckRequest = 0;

    private DatagramSocket udpReceiveSocket;
    private DatagramSocket sendSocket;

    public void setTIMEOUT(int TIMEOUT) {
        this.TIMEOUT = TIMEOUT;
    }

    private int TIMEOUT = 50;



    public ControllerSocket(){
        receiveBuffer = new byte[receiveBufferSize];
        sendBuffer = new byte[sendBufferSize];
    }

    public void run(){
        try{
            this.udpReceiveSocket = new DatagramSocket(receivePort);
            System.out.println("Started the UDP socket for controller at receivePort " + this.udpReceiveSocket.getLocalPort() + " with buffer size " + receiveBufferSize + ".");

            sendSocket = new DatagramSocket(0);
        }
        catch (SocketException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop(){
        running = false;
    }

    public int waitForWorkerResponse(){

        DatagramPacket udpPacket = new DatagramPacket(receiveBuffer, receiveBufferSize);
        try {
            udpReceiveSocket.receive(udpPacket);

            byte[] content = Arrays.copyOfRange(udpPacket.getData(), 0, udpPacket.getLength());

            if(new String(content).length() > 4) {
                if (new String(content).substring(0, 5).equals("Port:")) {
                    String workerPorts = new String(content).substring(6);
                    int workerPortWorkRequest = Integer.parseInt(workerPorts.substring(0, workerPorts.indexOf(',')));
                    workerPortHealthCheckRequest = Integer.parseInt(workerPorts.substring(workerPorts.indexOf(',')+1));

                    knownWorker = false;
                    return workerPortWorkRequest;
                }
            }

            if(new String(content).substring(0, 21).equals("New workload required")){
                int workerId= Integer.parseInt(new String(content).substring(23));
                knownWorker = true;
                return workerId;
            }

            System.out.println("Received packet: Port: " + udpPacket.getPort() + ", length " + udpPacket.getLength() + ", content: " + new String(content));//SPÄTER ÄNDERN
        } catch (SocketTimeoutException e){
            System.out.println("SOCKET TIMEOUT OCCURRED");
        }
        catch (IOException e) {
            System.out.println("Message could not be received.\n" + e);
        }

        return -1;
    }

    public void sendWorkLoad(int port, float[] row, float[] column, int rowProgress,int columnProgress){

        //System.out.println("###SENDING WORK###");

        sendBuffer = floatToByte(row, 'r', rowProgress);
        send(port, sendBuffer);
        sendBuffer = floatToByte(column, 'c', columnProgress);
        send(port, sendBuffer);
    }

    public void sendWorkDone(int port){
        //System.out.println("###SENDING WORK DONE###");

        byte[] sendBuffer = ("work done").getBytes();
        send(port, sendBuffer);
    }

    public int sendID(int id, int port, int portOfHealthCheckThread){

        byte[] sendBuffer = ("Id: " + id +"," + portOfHealthCheckThread).getBytes();
        send(port, sendBuffer);

        return 0;
    }

    private void send(int port, byte[] buf) {
        try{
            sendAddress = InetAddress.getByName(sendAddressName);
        } catch (UnknownHostException e){
            System.out.println("Can not parse send address.\n" + e.getMessage());
            System.exit(1);
        }

        DatagramPacket packet = new DatagramPacket(buf, buf.length, sendAddress, port);
        try {
            sendSocket.send(packet);
        } catch (IOException e) {
            System.out.println("Port: "+ packet.getPort() +", Message could not be send.\n" + e);

            /*send(port, buf);*/
        }
    }

    public boolean isKnownWorker() {
        return knownWorker;
    }

    private byte[] floatToByte(float[] input, char option, int progress) {

        String indicator = "";

        if(option == 'r')
            indicator = "row:";
        else
            indicator = "col:";

        byte[] ret = new byte[input.length*4+8]/*TEST [input.length*4]*/;
        ByteBuffer.wrap(ret, 0,4).put(indicator.getBytes());//TEST
        ByteBuffer.wrap(ret, 4,4).putInt(progress);//TEST

        for (int x = 0; x < input.length; x++) {
            ByteBuffer.wrap(ret, ((x+2)/*TEST (x)*/*4), 4).putFloat(input[x]);
        }
        return ret;
    }

    public void socketTimeoutReset(){
        try {
            udpReceiveSocket.setSoTimeout(0);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public void socketTimeoutSetToTimeoutValue(){
        try {
            udpReceiveSocket.setSoTimeout(TIMEOUT);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public int getWorkerPortHealthCheckRequest() {
        return workerPortHealthCheckRequest;
    }
}