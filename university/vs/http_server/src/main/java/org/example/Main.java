package org.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.sql.*;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws Exception {
        boolean FUNC_TEST = false;
        int maxRows = 256;
        int maxColumns = 256;

        Class.forName("org.postgresql.Driver");
        String url = "jdbc:postgresql://localhost:5432/test_db";
        Properties props = new Properties();
        props.setProperty("user", "root123");
        props.setProperty("password", "root123");
        Connection con = DriverManager.getConnection(url, props);

        Statement stmt=con.createStatement();

        try{
            stmt.execute("Create table matrix(matrixRow int, matrixColumn int, matrixValue float, PRIMARY KEY (matrixRow, matrixColumn))");
        } catch (Exception e){
            System.out.println("Cannot create Table.\n" + e.getMessage());
            stmt.execute("Drop table matrix");
            stmt.execute("Create table matrix(matrixRow int, matrixColumn int, matrixValue float, PRIMARY KEY (matrixRow, matrixColumn))");
        }

        int port = 32768;
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Listening on port: " + port);
        boolean notCloseServer = true;


        while (notCloseServer) {
            boolean getMatrix = false;
            boolean postMatrix = false;
            boolean getWholeMatrix = false;

            try {
                Socket clientSocket = serverSocket.accept();

                System.err.println("Connection with client established");

                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                String clientRequestLine;
                long byteCount = 0;

                while ((clientRequestLine = in.readLine()) != null) {
                    if(clientRequestLine.length() >= 3 && clientRequestLine.substring(0, 3).equals("GET")){
                        if(clientRequestLine.length() >= 17 && clientRequestLine.substring(4, 17).equals("/matrixValues")){
                            getMatrix = true;
                        }
                        if(clientRequestLine.length() >= 17 && clientRequestLine.substring(4, 16).equals("/wholeMatrix")){
                            getWholeMatrix = true;
                        }
                        if(clientRequestLine.length() >= 17 && clientRequestLine.substring(4, 16).equals("/closeServer")){
                            notCloseServer = false;
                        }
                    }
                    else if (clientRequestLine.length() >= 4 && clientRequestLine.substring(0, 4).equals("POST")) {
                        if(clientRequestLine.length() >= 18 && clientRequestLine.substring(5, 18).equals("/matrixValues")){
                            postMatrix = true;
                        }
                    }
                    /*else if (clientRequestLine.length() >= 4 && clientRequestLine.substring(0, 4).equals("row:")){
                        data = clientRequestLine;
                    }*/
                    else if(clientRequestLine.length() >= 16 && clientRequestLine.substring(0,15).equals("Content-Length:"))
                        byteCount = Long.parseLong(clientRequestLine.substring(16));

                    System.out.println(clientRequestLine);
                    if (clientRequestLine.isEmpty()) {
                        break;
                    }
                }


                if(postMatrix){

                    String data = "";
                    for (int i = 0; i < byteCount; i++) {
                        data += (char)in.read();
                    }
                    if(data.startsWith("<html>")){
                        data = data.substring(6);
                        if(data.endsWith("</html>"));
                        data = data.substring(0, data.length()-7);
                    }


                    String [] values = data.split("&");

                    int row = Integer.parseInt(values[0].substring(4));
                    int col = Integer.parseInt(values[1].substring(4));
                    float result = Float.parseFloat(values[2].substring(7));

                    if(FUNC_TEST){
                        if(row == maxRows && col == maxColumns) {
                            serverSocket.setSoTimeout(10000);
                        }
                    }

                    System.out.println("Row = " + row);
                    System.out.println("Col = " + col);
                    System.out.println("Result = " + result);

                    PreparedStatement insertData = con.prepareStatement("Insert into matrix(matrixRow, matrixColumn, matrixValue) VALUES(?, ?, ?)");
                    insertData.setInt(1, row);
                    insertData.setInt(2, col);
                    insertData.setFloat(3, result);

                    try {
                        insertData.execute();
                    }catch (Exception e){ //TODO: Maybe change later, server ignores duplicate errors
                        if(e.getMessage().startsWith("ERROR: duplicate key value violates unique constraint \"matrix_pkey\""))
                            System.out.println(e.getMessage());
                        else
                            throw e;
                    }

                    System.out.println("POSTFUNKTION");
                    out.write("HTTP/1.0 200 OK\r\n");
                    out.write("Content-Type: text/html\r\n");
                    out.write("Content-Length: 59\r\n");
                    out.write("\r\n");
                    out.write("<TITLE>Ack</TITLE>");
                    out.write("<html>Progress was send successful</html>");
                    out.write("\r\n");
                }
                else if(getMatrix){

                    String body = "";

                    ResultSet rs=stmt.executeQuery("select * from matrix ORDER BY matrixRow DESC, matrixColumn DESC ");
                    while(rs.next()){
                        String output = "<P>Row: " + rs.getInt(1) + ", Column: " + rs.getInt(2) + ", Result:" + rs.getFloat(3) + "</P>";
                        body = body + output;
                    }

                    out.write("HTTP/1.0 200 OK\r\n");
                    out.write("Content-Type: text/html\r\n");
                    out.write("Content-Length: "+ body.length() + 27 +"\r\n");
                    out.write("\r\n");
                    out.write("<TITLE>MatrixValues</TITLE>");
                    out.write(body);
                }
                else if(getWholeMatrix){

                    ResultSet rs=stmt.executeQuery("select * from matrix ORDER BY matrixRow ASC , matrixColumn ASC ");

                    String body = "<table border=\"1\">";
                    int column = 1;
                    body += "<tr>";

                    while(rs.next()){
                        int checkColumn = rs.getInt(1);
                        if (checkColumn != column) {
                            body += "</tr>";
                            body += "<tr>";
                            column = checkColumn;
                        }
                        body += "<td>"+ rs.getFloat(3)+ "</td>";
                    }

                    body += "</tr>";
                    body += "</table>";

                    out.write("HTTP/1.0 200 OK\r\n");
                    out.write("Content-Type: text/html\r\n");
                    out.write("Content-Length: "+ body.length() + 27 +"\r\n");
                    out.write("\r\n");
                    out.write("<TITLE>MatrixValues</TITLE>");
                    out.write(body);
                }
                else if(!notCloseServer){
                    out.write("HTTP/1.0 200 OK\r\n");
                    out.write("Content-Type: text/html\r\n");
                    out.write("Content-Length: 88\r\n");
                    out.write("\r\n");
                    out.write("<TITLE>Response</TITLE>");
                    out.write("<P>Server will shutdown. Further Requests will be futile :,(.</P>");
                }
                else{
                    out.write("HTTP/1.0 200 OK\r\n");
                    out.write("Content-Type: text/html\r\n");
                    out.write("Content-Length: 124\r\n");
                    out.write("\r\n");
                    out.write("<TITLE>Response</TITLE>");
                    out.write("<P>In order to get the wanted data, you need to specify the correct file path in your GET-Request</P>");
                    out.write("\r\n");
                }

                System.err.println("Connection terminated");

                out.close();
                in.close();
                clientSocket.close();
            }catch (SocketTimeoutException e){

                System.out.println("TIMEOUT.\n" + e.getMessage());
                if(FUNC_TEST) {
                    ResultSet rsCount = stmt.executeQuery("select count(*) from matrix");
                    rsCount.next();
                    int count = rsCount.getInt(1);
                    if (count == maxRows * maxColumns)
                        System.out.println("Functional Test was a success: All of the expected data arrived");
                    else
                        System.out.println("Functional Test was a failed: Not all of the expected data arrived");
                    notCloseServer = false;
                }
            }
        }
        con.close();
    }
}

