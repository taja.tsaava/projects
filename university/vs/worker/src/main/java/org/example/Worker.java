package org.example;
import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.eclipse.paho.client.mqttv3.*;
import org.example.com.baeldung.thrift.impl.CrossPlatformService;
import org.example.com.baeldung.thrift.impl.InvalidOperationException;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Worker implements Runnable {
    private int id;
    private float[][] theWantedArray;
    private float[] matrixRow;
    private float[] matrixCol;

    private float currentResult = 0.0f;

    private boolean DEBUG = false;
    private boolean FUNC_TEST = false;
    private WorkerSocketUDP socketUDP;
    private WorkerSocketTCP socketTCP;
    private TimeMeasure myTimeMeasure;
    //private double averageEST = 0; //average estimated time for RTT
    private TServer server;



    private static final String MQTT_SERVER_URI = "tcp://localhost:1883";
    private static final String TOPIC = "savingWorkloadInServer";
    private MqttClient client;
    private long lamportTime = 1;
    private String clientId;
    private List requestList;
    private RequestInformation myInformation = null;

    private CountDownLatch waitingForAcknowledgements;
    private int W8_FOR_ACKS_TIMEOUT = 5000;
    private String MQTT_HANDLER = "mqttHandler";
    private Semaphore informationMutex;
    private int messageID = 1;


    private String MQTT_CALLER = "mqttCaller";
    private String MQTT_RESET = "mqttReset";
    private String MQTT_ACK_LIST= "mqttAckList";


    private final boolean NON_FUNC_TEST_MOM = false; //TODO: FOR PERFORMANCE TEST, SET TO TRUE
    private final int NON_FUNC_TEST_MOM_MEASUREMENT_VALUE = 1000;
    ArrayList <Long> NON_FUNC_TEST_MOM_RTT_VALUES;
    int NON_FUNC_TEST_MOM_RTT_VALUES_PROGRESS = 0;


    private final boolean FUNC_TEST_MOM = false; //TODO: FOR FUNCTIONAL TEST, SET TO TRUE
    private boolean FUNC_TEST_MOM_SENDING_AT_THE_MOMENT = false;
    private boolean TEST_FAILED = false;


    public Worker() throws UnknownHostException, MalformedURLException {

        String publisherId = UUID.randomUUID().toString();
        connectToMqtt(publisherId);
        this.clientId = client.getClientId();
        requestList = new ArrayList<RequestInformation>();
        informationMutex = new Semaphore(1);

        myTimeMeasure = new TimeMeasure();
        socketUDP = new WorkerSocketUDP();
        socketTCP = new WorkerSocketTCP();

        if(NON_FUNC_TEST_MOM)
            NON_FUNC_TEST_MOM_RTT_VALUES = new ArrayList<>();

        runSocket();
    }

    private void connectToMqtt(String publisherId) {
        try {
            client = new MqttClient(MQTT_SERVER_URI, publisherId, null);
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        try {
            client.connect(options);
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
    }

    public int statisticsRTT(){
        int numberOfPackages = 100;
        int[] rtts = new int[numberOfPackages];

        for(int i=0; i<numberOfPackages; i++){
            myTimeMeasure.setStart(System.nanoTime());
            socketUDP.requestWork(getId());
            socketUDP.getWorkload(myTimeMeasure);
            int time = (int)(myTimeMeasure.getEnd() - myTimeMeasure.getStart()) / 1000;
            rtts[i] = time;
        }

        int result = 0;

        for (int i = 0; i < numberOfPackages; i++) {
            result += rtts[i];
        }

        result = result/numberOfPackages;
        return result;
        //System.out.println("Average RTT with " + numberOfPackages + " samples: " + result + " micro seconds");
    }


    private void runSocket(){
        socketUDP.createConnection();

        //Instead of subscribing, the threads subscribes
        Thread mqttHandler = new Thread(this, MQTT_HANDLER);
        mqttHandler.start();

        try {
            TServerTransport serverTransport = new TServerSocket(socketUDP.getWorkReceivePort());
            server = new TSimpleServer(new TServer.Args(serverTransport).processor(new CrossPlatformService.Processor<>(new CrossPlatformService.Iface() {
                @Override
                public boolean calculateWorkload(int rowProgress, int colProgress, List<Double> matrixRow, List<Double> matrixColumn) throws InvalidOperationException, TException {

                    myTimeMeasure.setEnd(System.nanoTime());

                    try{
                        currentResult = matrixMul(matrixRow, matrixColumn);
                        myTimeMeasure.setRowProgress(rowProgress);
                        myTimeMeasure.setColProgress(colProgress);

                    }catch (RuntimeException e){
                        System.out.println(e.getMessage());
                        System.out.println("VERSAGT");
                        return false;
                    }

                    System.out.println("ERFOLGREICH");

                    return true;
                }

                @Override
                public boolean saveIntoDatabase(int workerCount) throws InvalidOperationException, TException {

                    waitingForAcknowledgements = new CountDownLatch((workerCount));
                    boolean gotAllAcknowledgements;

                    callThread();

                    try {
                        gotAllAcknowledgements = waitingForAcknowledgements.await(W8_FOR_ACKS_TIMEOUT, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        return false;
                    }

                    if (!gotAllAcknowledgements) {
                        //reset own request ambitions
                        reset();
                        return false;
                    }

                    //For performance test
                    if (NON_FUNC_TEST_MOM){
                        myTimeMeasure.setEnd(System.nanoTime());
                        System.out.println( "###TEST PROGRESS: "+NON_FUNC_TEST_MOM_RTT_VALUES_PROGRESS + " ###"); //TODO: Debug
                        if(NON_FUNC_TEST_MOM_RTT_VALUES_PROGRESS < NON_FUNC_TEST_MOM_MEASUREMENT_VALUE){
                            NON_FUNC_TEST_MOM_RTT_VALUES.add(myTimeMeasure.getRTT());
                        }
                        else if(NON_FUNC_TEST_MOM_RTT_VALUES_PROGRESS == NON_FUNC_TEST_MOM_MEASUREMENT_VALUE){
                            testEvaluation();
                        }
                        NON_FUNC_TEST_MOM_RTT_VALUES_PROGRESS++;
                    }

                    if(FUNC_TEST_MOM){
                        FUNC_TEST_MOM_SENDING_AT_THE_MOMENT = true;
                        try {
                            callForFunctionalTest();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }

                    try{
                        socketTCP.sendData(myTimeMeasure.getRowProgress(), myTimeMeasure.getColProgress(), currentResult);
                    }catch (RuntimeException e){
                        System.out.println(e.getMessage());
                        return false;
                    }

                    if(FUNC_TEST_MOM && TEST_FAILED){
                        System.out.println("FUNCTIONAL TEST FAILED: OTHER WORKER GOT ACCESS ON SERVER, WHILE I AM SENDING!");
                        System.exit(1);
                    }

                    if(FUNC_TEST_MOM){
                        FUNC_TEST_MOM_SENDING_AT_THE_MOMENT = false;
                    }

                    ackListThread();
                    reset();

                    currentResult = 0.0f;
                    myTimeMeasure.setRowProgress(0);
                    myTimeMeasure.setColProgress(0);

                    return true;
                }

                @Override
                public void workDone() throws InvalidOperationException, TException {
                    socketUDP.setWorkNotDone(false);
                    if (server != null && server.isServing()) {
                        System.out.println("Stopping the worker... ");

                        server.stop();

                        System.out.println("goodbye world :,(");
                        System.exit(0);
                    }
                }

                @Override
                public double calculateWorkloadFunctionalTest(int rowProgress, int colProgress, List<Double> matrixRow, List<Double> matrixColumn) throws InvalidOperationException, TException {

                    myTimeMeasure.setEnd(System.nanoTime());

                    try{
                        currentResult = matrixMul(matrixRow, matrixColumn);
                        myTimeMeasure.setRowProgress(rowProgress);
                        myTimeMeasure.setColProgress(colProgress);

                    }catch (RuntimeException e){
                        System.out.println(e.getMessage());
                        System.out.println("VERSAGT");
                        return 0.0f;
                    }

                    System.out.println("ERFOLGREICH");

                    return currentResult;
                }
            })));
        } catch (TTransportException e) {
            throw new RuntimeException(e);
        }

        if(FUNC_TEST == false) {
            Thread healthcheckResponder = new Thread(this);
            healthcheckResponder.start();

            id = socketUDP.requestID();

            server.serve();
        }

    }

    private void testEvaluation() {
        long result = 0;
        long variance = 0;

        for (int i = 0; i < NON_FUNC_TEST_MOM_MEASUREMENT_VALUE; i++) {
            result += NON_FUNC_TEST_MOM_RTT_VALUES.get(i);
        }
        result = result / NON_FUNC_TEST_MOM_MEASUREMENT_VALUE;


        for (int i = 0; i < NON_FUNC_TEST_MOM_MEASUREMENT_VALUE; i++) {
            variance += Math.pow(NON_FUNC_TEST_MOM_RTT_VALUES.get(i) - result, 2);
        }

        variance = variance / NON_FUNC_TEST_MOM_MEASUREMENT_VALUE;
        long standardDeviation = (int) Math.sqrt(variance);

        System.out.println("Average RTT with " + NON_FUNC_TEST_MOM_MEASUREMENT_VALUE + " samples: " + result + " nano seconds");
        System.out.println("Variance RTT with " + NON_FUNC_TEST_MOM_MEASUREMENT_VALUE + " samples: " + variance + " nano seconds");
        System.out.println("Standard deviation RTT with " + NON_FUNC_TEST_MOM_MEASUREMENT_VALUE + " samples: " + standardDeviation + " nano seconds");
    }

    public Void call() throws Exception {
        if ( !client.isConnected()) {
            return null;
        }
        informationMutex.p();

        if(NON_FUNC_TEST_MOM) //For performance test
            myTimeMeasure.setStart(System.nanoTime());

        byte[] payload = String.format("LT: "+ lamportTime + ",ID: " + clientId + ",MSG: REQUEST,MSG_ID: " + messageID)//4 characters, 2 decimal points
                .getBytes();

        myInformation = new RequestInformation(lamportTime, clientId, String.valueOf(messageID));

        client.publish(TOPIC,payload, 2, true);
        lamportTime++;
        messageID++;

        informationMutex.v();
        return null;
    }

    public Void callForFunctionalTest() throws Exception {
        if ( !client.isConnected()) {
            return null;
        }
        informationMutex.p();

        if(NON_FUNC_TEST_MOM) //For performance test
            myTimeMeasure.setStart(System.nanoTime());

        byte[] payload = String.format("LT: "+ lamportTime + ",ID: " + clientId + ",MSG: SENDING_AT_THE_MOMENT,MSG_ID: " + messageID)//4 characters, 2 decimal points
                .getBytes();

        myInformation = new RequestInformation(lamportTime, clientId, String.valueOf(messageID));

        client.publish(TOPIC,payload, 2, true);
        lamportTime++;
        messageID++;

        informationMutex.v();
        return null;
    }

    public void callThread(){
        Thread myThread = new Thread(this, MQTT_CALLER);
        myThread.start();
    }

    public void reset(){
        informationMutex.p();
        myInformation = null;
        informationMutex.v();
    }

    public void resetThread(){
        Thread myThread = new Thread(this, MQTT_RESET);
        myThread.start();
    }

    public void ackList(){
        informationMutex.p();
        //Acknowledge other's requests
        for (int i = requestList.size()-1; i >= 0; i--) {
            RequestInformation foreignerInformation = (RequestInformation) requestList.get(i);
            sendAck(foreignerInformation.getId(), foreignerInformation.getMsgId());
            requestList.remove(i);
        }myInformation = null;
        informationMutex.v();
    }

    public void ackListThread(){
        Thread myThread = new Thread(this, MQTT_ACK_LIST);
        myThread.start();
    }

    public void subscribe(){
        try {
            client.subscribe(TOPIC);
            MqttCallback myCallBack = (new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {
                    System.out.println("LOST CONNECTION TO MQTT-BROKER");
                }

                @Override
                public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                    byte[] payload = mqttMessage.getPayload();
                    // ... payload handling omitted
                    String s = new String(payload, StandardCharsets.US_ASCII);
                    System.out.println("Output : " + s);

                    String[] information = s.split(",");

                    long foreignerLamportTime = Integer.parseInt(information[0].substring(4));
                    String foreignerID = information[1].substring(4);
                    String content = information[2].substring(5);
                    String msgID = information[3].substring(8);

                    informationMutex.p();
                    System.out.println("LOCKED"); //TODO: DEBUG

                    System.out.println("ABSCHNITT 1"); //TODO: DEBUG
                    if(foreignerLamportTime >= lamportTime)
                        lamportTime = foreignerLamportTime+1;

                    System.out.println("ABSCHNITT 2"); //TODO: DEBUG
                    if(myInformation != null && content.equals("REQUEST") && foreignerLamportTime!=1){ //Doesn't ack lamport time of 1, because then there could be 2 workers, sending at the same time
                        System.out.println("ABSCHNITT 2.1"); //TODO: DEBUG
                        RequestInformation foreignerInformation = new RequestInformation(foreignerLamportTime, foreignerID, msgID);

                        if((myInformation.getLamportTime() > foreignerInformation.getLamportTime()) || myInformation.getLamportTime() == 1) { //ack other's while own lamport time is 1, because current request won't be acked
                            System.out.println("CURRENT LAMPORT TIME WHILE ACKING: " + myInformation.getLamportTime()); //TODO: DEBUG
                            sendAck(foreignerID, msgID);
                        }
                        else if(myInformation.getLamportTime() == foreignerInformation.getLamportTime()){
                            int idComparison = myInformation.getId().compareTo(foreignerInformation.getId());
                            if(idComparison >= 0) {
                                sendAck(foreignerID, msgID);
                            }
                            else if(idComparison < 0) {
                                requestList.add(foreignerInformation);
                            }

                        }
                        else {
                            System.out.println("CURRENT LAMPORT TIME: " + myInformation.getLamportTime()); //TODO: DEBUG
                            requestList.add(foreignerInformation);
                        }

                    }
                    else if(content.equals("REQUEST") && ((foreignerLamportTime!=1) || (!foreignerID.equals(myInformation.getId())))){ //Doesn't ack lamport time of 1, because then there could be 2 workers, sending at the same time
                        System.out.println("ABSCHNITT 2.2"); //TODO: DEBUG
                        sendAck(foreignerID, msgID);
                    }
                    else if(content.equals("ACK") && myInformation != null){
                        System.out.println("ABSCHNITT 2.3"); //TODO: DEBUG
                        if(foreignerID.equals(myInformation.getId()) && msgID.equals(myInformation.getMsgId())) {
                            waitingForAcknowledgements.countDown();
                        }
                    }
                    else if(FUNC_TEST_MOM && content.equals("SENDING_AT_THE_MOMENT") && FUNC_TEST_MOM_SENDING_AT_THE_MOMENT && (!foreignerID.equals(myInformation.getId()))){
                        TEST_FAILED = true;
                    }

                    System.out.println("ABSCHNITT 3"); //TODO: DEBUG
                    informationMutex.v();
                    System.out.println("UNLOCKED"); //TODO: DEBUG
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                    System.out.println("Delivery Complete");//TODO DEBUG
                }
            });

            client.setCallback(myCallBack);
            Semaphore mySemaphore = new Semaphore(0);
            mySemaphore.p();

            } catch (MqttException e) {
            e.getMessage();
            throw new RuntimeException(e);
        }
    }

    private void sendAck(String foreignerId, String msgId){
        byte[] payloadAck = String.format("LT: "+ lamportTime + ",ID: " + foreignerId + ",MSG: ACK,MSG_ID: " + msgId)//4 characters, 2 decimal points
                .getBytes();
        try {
            client.publish(TOPIC,payloadAck, 2, true);
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
    }

    /* Getter */
    public int getId() {
        return this.id;
    }

    /* Functions to fill theWantedArray into matrixCol and matrixRow */
    public float[] fillMatrixRow(float [][] theWantedArray){
        matrixRow = theWantedArray[0];
        return this.matrixRow;
    }

    public float[] fillMatrixCol(float [][] theWantedArray){
        matrixCol = theWantedArray[1];
        return this.matrixCol;
    }



    /* Multiplication function for Matrix */
    float matrixMul(float[] matrixCol, float[] matrixRow) {
        float matrixResult = 0;

        for (int i = 0; i < matrixRow.length; i++) {
            matrixResult += matrixCol[i] * matrixRow[i];
        }
        return matrixResult;
    }

    float matrixMul(List<Double> matrixCol, List<Double> matrixRow) {
        float matrixResult = 0;

        for (int i = 0; i < matrixRow.size(); i++) {
            matrixResult += matrixCol.get(i) * matrixRow.get(i);
        }
        return matrixResult;
    }

    @Override
    public void run() {

        if(Thread.currentThread().getName().equals(MQTT_HANDLER)){
            subscribe();
            System.out.println("THE END");// TODO: Debug
        }
        else if(Thread.currentThread().getName().equals(MQTT_CALLER)){
            try {
                call();
            } catch (Exception e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
            try {
                Thread.currentThread().join();
            } catch (InterruptedException e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
        }
        else if(Thread.currentThread().getName().equals(MQTT_RESET)){
            try {
                reset();
            } catch (Exception e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
            try {
                Thread.currentThread().join();
            } catch (InterruptedException e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
        }
        else if(Thread.currentThread().getName().equals(MQTT_ACK_LIST)){
            try {
                ackList();
            } catch (Exception e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
            try {
                Thread.currentThread().join();
            } catch (InterruptedException e) {
                e.getMessage();
                throw new RuntimeException(e);
            }
        }
        else {
            while (socketUDP.isWorkNotDone())
                socketUDP.waitForController();
        }
    }

    private void testRightValue(){//TODO: Doesn't work at the moment, i think
        id = socketUDP.requestID();
        socketUDP.requestWork(id);


        theWantedArray = socketUDP.getWorkload(myTimeMeasure);
        matrixRow = fillMatrixRow(theWantedArray);
        matrixCol = fillMatrixCol(theWantedArray);


        /*for (int i = 0; i < matrixRow.length; i++) {
            System.out.println("matrixRow: "+matrixRow[i]);
            System.out.println("matrixCol: "+matrixCol[i]);
        }*/

        float result1 = matrixMul(matrixCol, matrixRow);

        int matrSize = 254;
        float testMatrix1[] = new float[matrSize];
        float testMatrix2[]= new float[matrSize];

        for (int i = 0; i < matrSize; i++) {
            testMatrix1[i] = 3.0f;
            testMatrix2[i] = 3.0f;
        }

        float result2 = matrixMul(testMatrix1, testMatrix2);

        if(result1 == result2)
            System.out.println("Test erfolgreich");
    }


}