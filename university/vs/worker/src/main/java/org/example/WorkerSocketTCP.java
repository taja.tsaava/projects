package org.example;

import java.io.*;
import java.net.*;
import java.net.Socket;
import java.util.ArrayList;

public class WorkerSocketTCP {
    boolean NON_FUNC_TEST = false;
    int measurementValue = 10000;
    ArrayList<Long> rttValues;

    private TimeMeasure myTimeMeasure;
    private int sendPort = 32768;
    private Socket sendSocket;
    private String hostname = "localhost";

    public WorkerSocketTCP() throws MalformedURLException {
        if(NON_FUNC_TEST) {
            myTimeMeasure = new TimeMeasure();
            rttValues = new ArrayList<Long>();
        }
    }

    public void sendData(int rowProgress, int colProgress, float result){
        try {
            sendSocket = new Socket(hostname, sendPort);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String rowString = Integer.toString(rowProgress);
        String colString = Integer.toString(colProgress);
        String resultString = Float.toString(result);

        String Data = "row:"+rowString+"&col:"+colString+"&result:"+resultString+"\r\n";
        sendDataPackage(Data);
    }
    public void sendDataPackage(String Data) {
        OutputStream output = null;
        try {
            output = sendSocket.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        PrintWriter writer = new PrintWriter(output, true);

        if(NON_FUNC_TEST)
            myTimeMeasure.setStart(System.nanoTime());

        writer.println("POST /matrixValues HTTP/1.1");
        writer.println("Content-Type: text/html");
        writer.println("Content-Length: " + (Data.length()+13));
        writer.println("");
        writer.write("<html>" + Data + "</html>");
        writer.println("");

        //TODO: Receive Acknowledgement from Server
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(sendSocket.getInputStream()));
            String fromServer;
            long byteCount = 0;
            while((fromServer = in.readLine())!= null){

                if(NON_FUNC_TEST && fromServer.equals("HTTP/1.0 200 OK")){
                    myTimeMeasure.setEnd(System.nanoTime());
                    rttValues.add(myTimeMeasure.getRTT());
                    System.out.println(rttValues.size());
                }

                if(fromServer.length() >= 16 && fromServer.startsWith("Content-Length:"))
                    byteCount = Long.parseLong(fromServer.substring(16));

                System.out.println(fromServer);
                if(fromServer.isEmpty())break;
            }

            fromServer="";
            for (int i = 0; i < byteCount; i++) {
                fromServer += (char)in.read();
            }
            System.out.println(fromServer);

            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        writer.close();

        if(NON_FUNC_TEST && (rttValues.size() == measurementValue)){
            long result = 0;
            long variance = 0;

            for (int i = 0; i < measurementValue; i++) {
                result += rttValues.get(i);
            }
            result = result/measurementValue;


            for (int i = 0; i < measurementValue; i++) {
                variance += Math.pow(rttValues.get(i)-result, 2);
            }

            variance = variance/measurementValue;
            long standardDeviation = (int) Math.sqrt(variance);

            System.out.println("Average RTT with " + measurementValue + " samples: " + result + " nano seconds");
            System.out.println("Variance RTT with " + measurementValue + " samples: " + variance + " nano seconds");
            System.out.println("Standard deviation RTT with " + measurementValue + " samples: " + standardDeviation + " nano seconds");

            System.exit(0);
        }
    }
}
