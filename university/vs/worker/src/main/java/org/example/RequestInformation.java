package org.example;

public class RequestInformation {

    public String getMsgId() {
        return msgId;
    }

    public long getLamportTime() {
        return lamportTime;
    }

    public String getId() {
        return id;
    }

    private long lamportTime;

    private String id;

    private String msgId;

    public RequestInformation(long parameterLamportTime, String parameterId, String parameterMsgId){
        this.lamportTime = parameterLamportTime;
        this.id = parameterId;
        this.msgId = parameterMsgId;
    }

}
