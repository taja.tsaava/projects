package org.example;

public class TimeMeasure {

    private long start;

    private long end;

    public int getRowProgress() {
        return rowProgress;
    }

    public void setRowProgress(int rowProgress) {
        this.rowProgress = rowProgress;
    }

    private int rowProgress = -1;

    public int getColProgress() {
        return colProgress;
    }

    public void setColProgress(int colProgress) {
        this.colProgress = colProgress;
    }

    private int colProgress = -1;

    TimeMeasure(){
        start = 0;
        end = 0;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public long getRTT(){return ((end-start));}

}
