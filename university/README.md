# University Projects Showcase

Welcome to my Git repository, where I showcase all my university projects. This repository contains a collection of projects from various courses I have taken during my studies. Some of the projects were completed in collaboration with partners or groups. This repository serves as a collection of knowledge and experiences I have gathered throughout my academic journey.

## Projects

### 1. [h_da Navigator]
- **Folder:** `iaid`
- **Course:** [Interaction and Interface Design]
- **Focus Areas:**
  - Fundamentals of User-Centered Design
  - Interface Design Principles
  - Cognition and its effects on interface design
  - Conceptualization of interactive applications
  - Design prototyping
  - Characteristics of interfaces for different platforms, devices, and input/output modalities (e.g., sound-based, gesture-based, visual)

- **Topics of project:** This project focused on creating a concept design for a campus navigation app aimed at enhancing accessibility and efficiency for students and visitors. Key features include providing orientation aids such as room finding assistance, details on room characteristics, and guidance during peak hours. The app also integrates with class schedules to optimize navigation based on academic activities, thereby improving overall efficiency and user experience on campus.

The concept design incorporates usability principles and user-centric features to create a seamless navigation experience tailored specifically for educational environments.

### 2. [Multi-Touch- and Multi-User-Interface]
- **Folder:** `mtmm`
- **Course:** [Multi-Touch- and Multi-User-Interfaces]
- **Focus Areas:**
  - Different types of Interfaces
  - Multi-User, Multi-Touch
    - Generic examples
    - Multi-Touch-Table h_da
    - Multi-Touch interfaces

- **Topics of project:** 
    - blob detection with openCV
    - videostreams with multi-touch-camera
    - object tracking and calculation
    - TUIO framework
        - normalization of blob coordinates
        - transmission of tracked blobs
        - TuioDump and TuioDemo
    - interaction with NUI (natural user interface)
    - gesture recognition
        - "1$ Gesture Recognizer"

### 3. [Autonomous Parking]
- **Folder:** `psd`
- **Course:** [Project System Development]
- **Focus Areas:**
  - Autonomous vehicle technology
  - Sensor integration
  - Mobile app development

- **Description:** This project was a collaborative effort by all students in the course.

The objective of this project was to develop and build a model vehicle capable of autonomously detecting parking spaces and parking in them with sensor support. The remote control of the vehicle is achieved through a responsive app. Additionally, the team took the initiative to implement an extra feature: controller support. This allows the user to control the vehicle using a physical controller via the app.

### 4. [E-Mobility App]
- **Folder:** `nzse`
- **Course:** [Nutzerzentrierte Softwareentwicklung]
- **Focus Areas:**
  - UseCase, UserStory, UserMapping
  - UX/UI
  - App Development with Android Studio
 
- **Description:** This project involves developing an app to help users find electric vehicle charging stations. Users can apply filters such as proximity, price, operator, and available facilities to locate charging stations. The app also allows users to quickly find previously visited stations through "Favorites" or "Recently Visited" options. 

The primary user scenario involves a user at home looking to charge their electric vehicle. The user can open the app to view nearby charging stations on a map, select a station, view its details, and use the app's navigation feature to drive to the chosen station. Additional features include live battery status and location tracking, accessible through the "Car Information" option in the app menu.

### 5. [Containerized Matrix Multiplication]
- **Folder:** `vs`
- **Course:** [Verteile Systeme]
- **Focus Areas:**
  - Distributed Computing / High Performance Computing
  - Sockets
  - RPC (Remote Procedure Calls)
  - MOM (Message-Oriented-Middleware)
- **Description:** The Distributed Systems Lab at Hochschule Darmstadt focuses on developing a distributed computing application. 

The project involves:

Technologies: Using Sockets, RPCs, and Message-Oriented Middleware.
Development: Incrementally building a system for distributed matrix multiplication, with components like health checks, simple database storage via REST, and coordinated database access using Lamport’s algorithm.
Deployment: Testing and deploying solutions using Docker on the Pi-Lab cluster.
The lab emphasizes inter-component communication, software design, testing, and real-world deployment.

### 6. [OS exercises]
- **Folder:** `bs`
- **Course:** [Betriebssysteme]
- **Focus Areas:**
  - OS (operating systems) Architecture
  - Procedures
  - Coordination and communication between processes
  - Scheduling
  - Virtual Box -> Kali Linux

- **Description:** This project is a collection of 5 exercises. 

### 7. [Human Machine Interface]
- **Folder:** `hmi`
- **Course:** [Human Machine Interfaces]
- **Focus Areas:**
  - Qt Creator
    - QML
    - Model-View-Framework
    - Qt Quick Controls
  - RaspberryPi
    - WiringPi
  - Sensordata

- **Description:**  This project involves developing a QML-based application to visualize sensor data acquired through GPIO using the wiringPi library. The application utilizes SwipeView for intuitive navigation between different screens and displays sensor readings using animated bars for enhanced visual feedback. Data is presented in a table format using the Model-View architecture, allowing users to interactively explore and analyze sensor measurements.

Additional features include settings management for customization of display preferences such as colors and themes. The project also encompasses hardware configuration to ensure seamless integration and reliable data acquisition from sensors connected via GPIO.


## DISCLAIMER:

Some of the projects listed here were completed in partnership or group work. This repository contains accumulated knowledge and experiences from my time at university. 

All projects and their contents are for demonstration purposes ONLY and serve as a reference for my skills and knowledge.

Please note that some projects may not have been fully completed (yet).
---

Thank you for your interest in my projects! 

If you have any questions or would like to learn more about a specific project, feel free to contact me.

